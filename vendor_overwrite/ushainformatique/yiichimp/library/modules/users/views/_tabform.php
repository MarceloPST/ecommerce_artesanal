<?php
use usni\UsniAdaptor;
use usni\library\bootstrap\TabbedActiveForm;
use usni\library\bootstrap\FormButtons;
use usni\library\widgets\Tabs;
use usni\library\widgets\TabbedActiveFormAlert;

/* @var $formDTO \usni\library\modules\users\dto\UserFormDTO */
/* @var $form \usni\library\bootstrap\TabbedActiveForm */
/* @var $this \usni\library\web\AdminView */

$user       = $formDTO->getModel();
$person     = $formDTO->getPerson();
$address    = $formDTO->getAddress();
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<?php
if($formDTO->getScenario() == 'create')
{
    $caption = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('users', ($language=='en-US')?'User':'Usuario');
}
else
{
    $caption = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('users', ($language=='en-US')?'User':'Usuario');
}
echo TabbedActiveFormAlert::widget(['model' => $formDTO->getModel()]);
$form = TabbedActiveForm::begin([
                                    'id'          => 'usereditview', 
                                    'layout'      => 'horizontal',
                                    'options'     => ['enctype' => 'multipart/form-data'],
                                    'caption'     => $caption
                               ]); 
?>
<?php
            $items[] = [
                'options' => ['id' => 'tabuser'],
                'label' => UsniAdaptor::t('application', ($language=='en-US')?'General':'General'),
                'class' => 'active',
                'content' => $this->render('/_useredit', ['form' => $form, 'formDTO' => $formDTO])
            ];
            $items[] = [
                'options' => ['id' => 'tabperson'],
                'label' => UsniAdaptor::t('users', ($language=='en-US')?'Person':'Persona'),
                'content' => $this->render('/_personedit', ['form' => $form, 'formDTO' => $formDTO, 'deleteUrl' => UsniAdaptor::createUrl('users/default/delete-image')])
            ];
            $items[] = [
                'options' => ['id' => 'tabaddress'],
                'label' => UsniAdaptor::t('users', ($language=='en-US')?'Address':'Dirección'),
                'content' => $this->render('/_addressedit', ['formDTO' => $formDTO, 'form' => $form])
            ];
            echo Tabs::widget(['items' => $items]);
    ?>
<?= FormButtons::widget(['cancelUrl' => UsniAdaptor::createUrl('users/default/index')]);?>
<?php TabbedActiveForm::end();