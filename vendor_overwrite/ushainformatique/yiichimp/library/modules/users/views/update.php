<?php
/**
 * @copyright Copyright (c) 2017 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://github.com/ushainformatique/yiichimp/blob/master/LICENSE.md
 */

/* @var $form \usni\library\bootstrap\TabbedActiveForm */
/* @var $this \usni\library\web\AdminView */
/* @var $userFormDTO \usni\library\modules\users\dto\UserFormDTO */
/* @var $formDTO \usni\library\modules\users\dto\UserFormDTO */

use usni\UsniAdaptor;
use usni\library\modules\users\widgets\BrowseDropdown;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$user = $formDTO->getModel();
$this->params['breadcrumbs'] = [
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Actualizar') . ' ' . UsniAdaptor::t('users', ($language=='en-US')?'Users':'Usuarios'),
                                        'url'   => ['/users/default/index']
                                    ],
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' #' . $user['id']
                                    ]
                               ];
$browseParams   = ['permission' => 'user.updateother',
                   'data' => $formDTO->getBrowseModels(),
                   'model' => $user,
                   'textAttribute' => 'username'];
echo BrowseDropdown::widget($browseParams);
$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('users', ($language=='en-US')?'User':'Usuario');
echo $this->render('/_tabform', ['formDTO' => $formDTO]);