<?php
/**
 * @copyright Copyright (c) 2017 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://github.com/ushainformatique/yiichimp/blob/master/LICENSE.md
 */
use usni\UsniAdaptor;
use usni\library\modules\users\widgets\UserNameDataColumn;
use usni\library\utils\TimezoneUtil;
use usni\library\modules\users\grid\UserActionColumn;
use yii\grid\CheckboxColumn;
use usni\library\grid\GridView;
use usni\library\modules\users\utils\UserUtil;
use usni\library\modules\users\widgets\ActionToolbar;
use usni\library\modules\users\models\User;
use usni\library\grid\StatusDataColumn;

/* @var $gridViewDTO \usni\library\modules\users\dto\UserGridViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$title          = UsniAdaptor::t('users', ($language=='en-US')?'Manage Users':'Administrar usuarios');
$this->title    = $this->params['breadcrumbs'][] = $title;

$toolbarParams  = [
                        'createUrl'         => 'create',
                        'bulkEditFormView'  => '/_bulkedit.php',
                        'showBulkEdit'      => true,
                        'showBulkDelete'    => false,
                        'gridId'            => 'usergridview',
                        'pjaxId'            => 'usergridview-pjax',
                        'bulkEditFormTitle' => UsniAdaptor::t('users', ($language=='en-US')?'User':'Usuario') . ' ' . UsniAdaptor::t('application', ($language=='en-US')?'Bulk Edit':'Editar seleccionado'),
                        'bulkEditActionUrl' => 'bulk-edit',
                        'bulkEditFormId'    => 'userbulkeditview',
                        'groupList'         => $gridViewDTO->getGroupList(),
                        'permissionPrefix'  => 'user'
                  ];
$widgetParams  = [
                        'id'            => 'usergridview',
                        'dataProvider'  => $gridViewDTO->getDataProvider(),
                        'filterModel'   => $gridViewDTO->getSearchModel(),
                        'caption'       => $title,
                        'columns' => [
                                        ['class' => CheckboxColumn::className()],   
                                        [
                                            'label' => ($language=='en-US')? 'Username':'Usuario',
                                            'attribute' => 'username',
                                            'class'     => UserNameDataColumn::className()
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'Email':'E-mail'),
                                            'attribute' => 'email'
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'First Name':'Nombre'),
                                            'attribute' => 'firstname'
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'Last Name':'Apellido'),
                                            'attribute' => 'lastname',
                                        ],
                                        [
                                            'label' => ($language=='en-US')? 'Timezone':'Zona horaria',
                                            'attribute' => 'timezone',
                                            'filter'    => TimezoneUtil::getTimezoneSelectOptions()
                                        ],
                                        [
                                            'label' => ($language=='en-US')?'Address1':'Dirección1',
                                            'attribute' => 'address1'
                                        ],
                                        [
                                            'label' => ($language=='en-US')?'Status':'Estado',
                                            'attribute' => 'status',
                                            'class'     => StatusDataColumn::className(),
                                            'filter'    => UserUtil::getStatusDropdown()
                                        ],
                                        [
                                            'class'             => UserActionColumn::className(),
                                            'template'          => '{view} {update} {delete} {changepassword} {changestatus}',
                                            'modelClassName'    => User::className()
                                        ]
                                     ],
                 ];
echo ActionToolbar::widget($toolbarParams);
echo GridView::widget($widgetParams);