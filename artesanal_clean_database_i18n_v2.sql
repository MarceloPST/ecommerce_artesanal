DROP TABLE IF EXISTS `tbl_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `country` varchar(10) DEFAULT NULL,
  `postal_code` varchar(16) DEFAULT NULL,
  `relatedmodel` varchar(32) DEFAULT NULL,
  `relatedmodel_id` int(11) DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_address`
--

LOCK TABLES `tbl_address` WRITE;
/*!40000 ALTER TABLE `tbl_address` DISABLE KEYS */;
INSERT INTO `tbl_address` (`id`, `address1`, `address2`, `city`, `state`, `country`, `postal_code`, `relatedmodel`, `relatedmodel_id`, `type`, `status`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'PUCE Sede Esmeraldas','','Esmeraldas','Esmeraldas','EC','080101','Person',1,1,1,1,1,'2018-08-13 02:29:33','2018-08-13 02:29:33'),(2,'PUCE Sede Esmeraldas','','Esmeraldas','Esmeraldas','EC','080101','Store',1,2,1,1,1,'2018-08-13 02:29:33','2018-08-13 02:29:33'),(3,'PUCE Sede Esmeraldas','','Esmeraldas','Esmeraldas','EC','080101','Store',1,3,1,1,1,'2018-08-13 03:18:35','2018-08-13 03:18:35');
/*!40000 ALTER TABLE `tbl_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_auth_assignment`
--

DROP TABLE IF EXISTS `tbl_auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_auth_assignment` (
  `identity_name` varchar(32) NOT NULL,
  `identity_type` varchar(16) NOT NULL,
  `permission` varchar(64) NOT NULL,
  `resource` varchar(32) NOT NULL,
  `module` varchar(32) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_auth_assignment`
--

LOCK TABLES `tbl_auth_assignment` WRITE;
/*!40000 ALTER TABLE `tbl_auth_assignment` DISABLE KEYS */;
INSERT INTO `tbl_auth_assignment` (`identity_name`, `identity_type`, `permission`, `resource`, `module`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES ('Artesano','group','access.catalog','CatalogModule','catalog',1,0,'2018-11-14 01:33:52',NULL),('Artesano','group','product.view','Product','catalog',1,0,'2018-11-14 01:33:53',NULL),('Artesano','group','product.update','Product','catalog',1,0,'2018-11-14 01:33:53',NULL),('Artesano','group','product.create','Product','catalog',1,0,'2018-11-14 01:33:53',NULL),('Artesano','group','product.manage','Product','catalog',1,0,'2018-11-14 01:33:54',NULL),('Artesano','group','product.delete','Product','catalog',1,0,'2018-11-14 01:33:54',NULL),('Artesano','group','productcategory.view','ProductCategory','catalog',1,0,'2018-11-14 01:33:54',NULL),('Artesano','group','productcategory.viewother','ProductCategory','catalog',1,0,'2018-11-14 01:33:54',NULL),('Artesano','group','productcategory.updateother','ProductCategory','catalog',1,0,'2018-11-14 01:33:55',NULL),('Artesano','group','productcategory.bulk-delete','ProductCategory','catalog',1,0,'2018-11-14 01:33:55',NULL),('Artesano','group','productreview.delete','ProductReview','catalog',1,0,'2018-11-14 01:33:55',NULL),('Artesano','group','productreview.manage','ProductReview','catalog',1,0,'2018-11-14 01:33:56',NULL),('Artesano','group','productreview.approve','ProductReview','catalog',1,0,'2018-11-14 01:33:56',NULL),('Artesano','group','productreview.spam','ProductReview','catalog',1,0,'2018-11-14 01:33:56',NULL),('Artesano','group','order.delete','Order','order',1,0,'2018-11-14 01:33:56',NULL),('Artesano','group','order.manage','Order','order',1,0,'2018-11-14 01:33:57',NULL),('Artesano','group','order.view','Order','order',1,0,'2018-11-14 01:33:57',NULL),('Artesano','group','order.update','Order','order',1,0,'2018-11-14 01:33:57',NULL),('Artesano','group','access.order','OrderModule','order',1,0,'2018-11-14 01:33:58',NULL);
/*!40000 ALTER TABLE `tbl_auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_auth_permission`
--

DROP TABLE IF EXISTS `tbl_auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `alias` varchar(64) NOT NULL,
  `resource` varchar(32) NOT NULL,
  `module` varchar(32) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=292 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_auth_permission`
--

LOCK TABLES `tbl_auth_permission` WRITE;
/*!40000 ALTER TABLE `tbl_auth_permission` DISABLE KEYS */;
INSERT INTO `tbl_auth_permission` (`id`, `name`, `alias`, `resource`, `module`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'access.auth','Access Tab','AuthModule','auth',1,0,'2018-08-13 03:37:55',NULL),(2,'auth.managepermissions','Manage Permissions','AuthModule','auth',1,0,'2018-08-13 03:37:55',NULL),(3,'group.create','Create Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(4,'group.view','View Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(5,'group.viewother','View Others Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(6,'group.update','Update Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(7,'group.updateother','Update Others Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(8,'group.delete','Delete Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(9,'group.deleteother','Delete Others Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(10,'group.manage','Manage Groups','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(11,'group.bulk-edit','Bulk Edit Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(12,'group.bulk-delete','Bulk Delete Group','Group','auth',1,0,'2018-08-13 03:37:55',NULL),(13,'access.home','Access Tab','HomeModule','home',1,0,'2018-08-13 03:37:55',NULL),(14,'access.language','Access Tab','LanguageModule','language',1,0,'2018-08-13 03:37:55',NULL),(15,'language.create','Create Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(16,'language.view','View Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(17,'language.viewother','View Others Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(18,'language.update','Update Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(19,'language.updateother','Update Others Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(20,'language.delete','Delete Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(21,'language.deleteother','Delete Others Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(22,'language.manage','Manage Languages','Language','language',1,0,'2018-08-13 03:37:55',NULL),(23,'language.bulk-edit','Bulk Edit Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(24,'language.bulk-delete','Bulk Delete Language','Language','language',1,0,'2018-08-13 03:37:55',NULL),(25,'access.notification','Access Tab','NotificationModule','notification',1,0,'2018-08-13 03:37:55',NULL),(26,'notification.delete','Delete Notification','Notification','notification',1,0,'2018-08-13 03:37:55',NULL),(27,'notification.manage','Manage Notifications','Notification','notification',1,0,'2018-08-13 03:37:55',NULL),(28,'notificationtemplate.create','Create Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(29,'notificationtemplate.view','View Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(30,'notificationtemplate.viewother','View Others Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(31,'notificationtemplate.update','Update Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(32,'notificationtemplate.updateother','Update Others Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(33,'notificationtemplate.delete','Delete Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(34,'notificationtemplate.deleteother','Delete Others Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(35,'notificationtemplate.manage','Manage Templates','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(36,'notificationtemplate.bulk-edit','Bulk Edit Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(37,'notificationtemplate.bulk-delete','Bulk Delete Template','NotificationTemplate','notification',1,0,'2018-08-13 03:37:55',NULL),(38,'notificationlayout.create','Create Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(39,'notificationlayout.view','View Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(40,'notificationlayout.viewother','View Others Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(41,'notificationlayout.update','Update Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(42,'notificationlayout.updateother','Update Others Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(43,'notificationlayout.delete','Delete Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(44,'notificationlayout.deleteother','Delete Others Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(45,'notificationlayout.manage','Manage Layouts','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(46,'notificationlayout.bulk-edit','Bulk Edit Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(47,'notificationlayout.bulk-delete','Bulk Delete Layout','NotificationLayout','notification',1,0,'2018-08-13 03:37:55',NULL),(48,'access.service','Access Tab','ServiceModule','service',1,0,'2018-08-13 03:37:55',NULL),(49,'service.checksystem','System Configuration','ServiceModule','service',1,0,'2018-08-13 03:37:55',NULL),(50,'service.rebuildpermissions','Rebuild Permissions','ServiceModule','service',1,0,'2018-08-13 03:37:55',NULL),(51,'service.rebuildmodulemetadata','Rebuild module metadata','ServiceModule','service',1,0,'2018-08-13 03:37:55',NULL),(52,'access.settings','Access Tab','SettingsModule','settings',1,0,'2018-08-13 03:37:55',NULL),(53,'settings.email','Email Settings','SettingsModule','settings',1,0,'2018-08-13 03:37:55',NULL),(54,'settings.site','Site Settings','SettingsModule','settings',1,0,'2018-08-13 03:37:55',NULL),(55,'settings.database','Database Settings','SettingsModule','settings',1,0,'2018-08-13 03:37:55',NULL),(56,'access.users','Access Tab','UsersModule','users',1,0,'2018-08-13 03:37:55',NULL),(57,'user.create','Create User','User','users',1,0,'2018-08-13 03:37:55',NULL),(58,'user.view','View User','User','users',1,0,'2018-08-13 03:37:55',NULL),(59,'user.viewother','View Others User','User','users',1,0,'2018-08-13 03:37:55',NULL),(60,'user.update','Update User','User','users',1,0,'2018-08-13 03:37:55',NULL),(61,'user.updateother','Update Others User','User','users',1,0,'2018-08-13 03:37:55',NULL),(62,'user.delete','Delete User','User','users',1,0,'2018-08-13 03:37:55',NULL),(63,'user.deleteother','Delete Others User','User','users',1,0,'2018-08-13 03:37:55',NULL),(64,'user.manage','Manage Users','User','users',1,0,'2018-08-13 03:37:55',NULL),(65,'user.bulk-edit','Bulk Edit User','User','users',1,0,'2018-08-13 03:37:55',NULL),(66,'user.bulk-delete','Bulk Delete User','User','users',1,0,'2018-08-13 03:37:55',NULL),(67,'user.change-password','Change Password','User','users',1,0,'2018-08-13 03:37:55',NULL),(68,'user.change-status','Change Status','User','users',1,0,'2018-08-13 03:37:55',NULL),(69,'user.settings','Settings','User','users',1,0,'2018-08-13 03:37:55',NULL),(70,'user.change-passwordother','Change Others Password','User','users',1,0,'2018-08-13 03:37:55',NULL),(71,'access.catalog','Access Tab','CatalogModule','catalog',1,0,'2018-08-13 03:37:55',NULL),(72,'productcategory.create','Create Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(73,'productcategory.view','View Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(74,'productcategory.viewother','View Others Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(75,'productcategory.update','Update Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(76,'productcategory.updateother','Update Others Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(77,'productcategory.delete','Delete Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(78,'productcategory.deleteother','Delete Others Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(79,'productcategory.manage','Manage Product Categories','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(80,'productcategory.bulk-edit','Bulk Edit Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(81,'productcategory.bulk-delete','Bulk Delete Product Category','ProductCategory','catalog',1,0,'2018-08-13 03:37:55',NULL),(82,'product.create','Create Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(83,'product.view','View Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(84,'product.viewother','View Others Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(85,'product.update','Update Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(86,'product.updateother','Update Others Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(87,'product.delete','Delete Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(88,'product.deleteother','Delete Others Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(89,'product.manage','Manage Products','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(90,'product.bulk-edit','Bulk Edit Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(91,'product.bulk-delete','Bulk Delete Product','Product','catalog',1,0,'2018-08-13 03:37:55',NULL),(92,'productreview.delete','Delete Review','ProductReview','catalog',1,0,'2018-08-13 03:37:55',NULL),(93,'productreview.manage','Manage Reviews','ProductReview','catalog',1,0,'2018-08-13 03:37:55',NULL),(94,'productreview.approve','Approve','ProductReview','catalog',1,0,'2018-08-13 03:37:55',NULL),(95,'productreview.spam','Spam','ProductReview','catalog',1,0,'2018-08-13 03:37:55',NULL),(96,'access.cms','Access Tab','CmsModule','cms',1,0,'2018-08-13 03:37:55',NULL),(97,'page.create','Create Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(98,'page.view','View Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(99,'page.viewother','View Others Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(100,'page.update','Update Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(101,'page.updateother','Update Others Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(102,'page.delete','Delete Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(103,'page.deleteother','Delete Others Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(104,'page.manage','Manage Pages','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(105,'page.bulk-edit','Bulk Edit Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(106,'page.bulk-delete','Bulk Delete Page','Page','cms',1,0,'2018-08-13 03:37:55',NULL),(107,'access.customer','Access Tab','CustomerModule','customer',1,0,'2018-08-13 03:37:55',NULL),(108,'customer.create','Create Customer','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(109,'customer.view','View Customer','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(110,'customer.viewother','View Others Customer','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(111,'customer.update','Update Customer','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(112,'customer.delete','Delete Customer','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(113,'customer.deleteother','Delete Others Customer','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(114,'customer.manage','Manage Customers','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(115,'customer.bulk-edit','Bulk Edit Customer','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(116,'customer.bulk-delete','Bulk Delete Customer','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(117,'customer.change-password','Change Password','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(118,'customer.change-status','Change Status','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(119,'customer.change-passwordother','Change Others Password','Customer','customer',1,0,'2018-08-13 03:37:55',NULL),(120,'access.dataCategories','Access Tab','DataCategoriesModule','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(121,'datacategory.create','Create Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(122,'datacategory.view','View Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(123,'datacategory.viewother','View Others Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(124,'datacategory.update','Update Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(125,'datacategory.updateother','Update Others Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(126,'datacategory.delete','Delete Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(127,'datacategory.deleteother','Delete Others Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(128,'datacategory.manage','Manage Data Categories','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(129,'datacategory.bulk-edit','Bulk Edit Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(130,'datacategory.bulk-delete','Bulk Delete Data Category','DataCategory','dataCategories',1,0,'2018-08-13 03:37:55',NULL),(131,'access.enhancement','Access Tab','EnhancementModule','enhancement',1,0,'2018-08-13 03:37:55',NULL),(132,'access.extension','Access Tab','ExtensionModule','extension',1,0,'2018-08-13 03:37:55',NULL),(133,'extension.update','Update Extension','Extension','extension',1,0,'2018-08-13 03:37:55',NULL),(134,'extension.updateother','Update Others Extension','Extension','extension',1,0,'2018-08-13 03:37:55',NULL),(135,'extension.delete','Delete Extension','Extension','extension',1,0,'2018-08-13 03:37:55',NULL),(136,'extension.deleteother','Delete Others Extension','Extension','extension',1,0,'2018-08-13 03:37:55',NULL),(137,'extension.manage','Manage Extensions','Extension','extension',1,0,'2018-08-13 03:37:55',NULL),(138,'extension.manageother','Manager Others Extension','Extension','extension',1,0,'2018-08-13 03:37:55',NULL),(139,'access.localization','Access Tab','LocalizationModule','localization',1,0,'2018-08-13 03:37:55',NULL),(140,'city.create','Create City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(141,'city.view','View City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(142,'city.viewother','View Others City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(143,'city.update','Update City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(144,'city.updateother','Update Others City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(145,'city.delete','Delete City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(146,'city.deleteother','Delete Others City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(147,'city.manage','Manage Cities','City','localization',1,0,'2018-08-13 03:37:55',NULL),(148,'city.bulk-edit','Bulk Edit City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(149,'city.bulk-delete','Bulk Delete City','City','localization',1,0,'2018-08-13 03:37:55',NULL),(150,'country.create','Create Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(151,'country.view','View Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(152,'country.viewother','View Others Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(153,'country.update','Update Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(154,'country.updateother','Update Others Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(155,'country.delete','Delete Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(156,'country.deleteother','Delete Others Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(157,'country.manage','Manage Countries','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(158,'country.bulk-edit','Bulk Edit Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(159,'country.bulk-delete','Bulk Delete Country','Country','localization',1,0,'2018-08-13 03:37:55',NULL),(160,'currency.create','Create Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(161,'currency.view','View Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(162,'currency.viewother','View Others Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(163,'currency.update','Update Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(164,'currency.updateother','Update Others Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(165,'currency.delete','Delete Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(166,'currency.deleteother','Delete Others Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(167,'currency.manage','Manage Currencies','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(168,'currency.bulk-edit','Bulk Edit Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(169,'currency.bulk-delete','Bulk Delete Currency','Currency','localization',1,0,'2018-08-13 03:37:55',NULL),(170,'state.create','Create State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(171,'state.view','View State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(172,'state.viewother','View Others State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(173,'state.update','Update State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(174,'state.updateother','Update Others State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(175,'state.delete','Delete State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(176,'state.deleteother','Delete Others State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(177,'state.manage','Manage States','State','localization',1,0,'2018-08-13 03:37:55',NULL),(178,'state.bulk-edit','Bulk Edit State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(179,'state.bulk-delete','Bulk Delete State','State','localization',1,0,'2018-08-13 03:37:55',NULL),(180,'lengthclass.create','Create Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(181,'lengthclass.view','View Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(182,'lengthclass.viewother','View Others Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(183,'lengthclass.update','Update Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(184,'lengthclass.updateother','Update Others Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(185,'lengthclass.delete','Delete Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(186,'lengthclass.deleteother','Delete Others Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(187,'lengthclass.manage','Manage Length Classes','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(188,'lengthclass.bulk-edit','Bulk Edit Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(189,'lengthclass.bulk-delete','Bulk Delete Length Class','LengthClass','localization',1,0,'2018-08-13 03:37:55',NULL),(190,'weightclass.create','Create Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(191,'weightclass.view','View Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(192,'weightclass.viewother','View Others Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(193,'weightclass.update','Update Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(194,'weightclass.updateother','Update Others Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(195,'weightclass.delete','Delete Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(196,'weightclass.deleteother','Delete Others Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(197,'weightclass.manage','Manage Weight Classes','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(198,'weightclass.bulk-edit','Bulk Edit Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(199,'weightclass.bulk-delete','Bulk Delete Weight Class','WeightClass','localization',1,0,'2018-08-13 03:37:55',NULL),(200,'stockstatus.create','Create Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(201,'stockstatus.view','View Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(202,'stockstatus.viewother','View Others Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(203,'stockstatus.update','Update Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(204,'stockstatus.updateother','Update Others Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(205,'stockstatus.delete','Delete Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(206,'stockstatus.deleteother','Delete Others Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(207,'stockstatus.manage','Manage Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(208,'stockstatus.bulk-edit','Bulk Edit Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(209,'stockstatus.bulk-delete','Bulk Delete Stock Status','StockStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(210,'orderstatus.create','Create Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(211,'orderstatus.view','View Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(212,'orderstatus.viewother','View Others Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(213,'orderstatus.update','Update Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(214,'orderstatus.updateother','Update Others Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(215,'orderstatus.delete','Delete Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(216,'orderstatus.deleteother','Delete Others Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(217,'orderstatus.manage','Manage Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(218,'orderstatus.bulk-edit','Bulk Edit Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(219,'orderstatus.bulk-delete','Bulk Delete Order Status','OrderStatus','localization',1,0,'2018-08-13 03:37:55',NULL),(220,'producttaxclass.create','Create Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(221,'producttaxclass.view','View Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(222,'producttaxclass.viewother','View Others Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(223,'producttaxclass.update','Update Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(224,'producttaxclass.updateother','Update Others Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(225,'producttaxclass.delete','Delete Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(226,'producttaxclass.deleteother','Delete Others Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(227,'producttaxclass.manage','Manage Product Tax Classes','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(228,'producttaxclass.bulk-edit','Bulk Edit Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(229,'producttaxclass.bulk-delete','Bulk Delete Product Tax Class','ProductTaxClass','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(230,'taxrule.create','Create Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(231,'taxrule.view','View Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(232,'taxrule.viewother','View Others Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(233,'taxrule.update','Update Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(234,'taxrule.updateother','Update Others Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(235,'taxrule.delete','Delete Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(236,'taxrule.deleteother','Delete Others Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(237,'taxrule.manage','Manage Tax Rules','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(238,'taxrule.bulk-edit','Bulk Edit Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(239,'taxrule.bulk-delete','Bulk Delete Tax Rule','TaxRule','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(240,'zone.create','Create Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(241,'zone.view','View Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(242,'zone.viewother','View Others Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(243,'zone.update','Update Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(244,'zone.updateother','Update Others Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(245,'zone.delete','Delete Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(246,'zone.deleteother','Delete Others Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(247,'zone.manage','Manage Zones','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(248,'zone.bulk-edit','Bulk Edit Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(249,'zone.bulk-delete','Bulk Delete Zone','Zone','localization/tax',1,0,'2018-08-13 03:37:55',NULL),(250,'access.manufacturer','Access Tab','ManufacturerModule','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(251,'manufacturer.create','Create Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(252,'manufacturer.view','View Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(253,'manufacturer.viewother','View Others Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(254,'manufacturer.update','Update Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(255,'manufacturer.updateother','Update Others Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(256,'manufacturer.delete','Delete Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(257,'manufacturer.deleteother','Delete Others Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(258,'manufacturer.manage','Manage Manufacturers','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(259,'manufacturer.bulk-edit','Bulk Edit Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(260,'manufacturer.bulk-delete','Bulk Delete Manufacturer','Manufacturer','manufacturer',1,0,'2018-08-13 03:37:55',NULL),(261,'access.marketing','Access Tab','MarketingModule','marketing',1,0,'2018-08-13 03:37:55',NULL),(262,'marketing.mail','Marketing Mails','MarketingModule','marketing',1,0,'2018-08-13 03:37:55',NULL),(263,'newsletter.create','Create Newsletter','Newsletter','marketing/newsletter',1,0,'2018-08-13 03:37:55',NULL),(264,'newsletter.view','View Newsletter','Newsletter','marketing/newsletter',1,0,'2018-08-13 03:37:55',NULL),(265,'newsletter.viewother','View Others Newsletter','Newsletter','marketing/newsletter',1,0,'2018-08-13 03:37:55',NULL),(266,'newsletter.update','Update Newsletter','Newsletter','marketing/newsletter',1,0,'2018-08-13 03:37:55',NULL),(267,'newsletter.updateother','Update Others Newsletter','Newsletter','marketing/newsletter',1,0,'2018-08-13 03:37:55',NULL),(268,'newsletter.delete','Delete Newsletter','Newsletter','marketing/newsletter',1,0,'2018-08-13 03:37:55',NULL),(269,'newsletter.deleteother','Delete Others Newsletter','Newsletter','marketing/newsletter',1,0,'2018-08-13 03:37:55',NULL),(270,'newsletter.manage','Manage Newsletters','Newsletter','marketing/newsletter',1,0,'2018-08-13 03:37:55',NULL),(271,'access.order','Access Tab','OrderModule','order',1,0,'2018-08-13 03:37:55',NULL),(272,'order.create','Create Order','Order','order',1,0,'2018-08-13 03:37:55',NULL),(273,'order.view','View Order','Order','order',1,0,'2018-08-13 03:37:55',NULL),(274,'order.viewother','View Others Order','Order','order',1,0,'2018-08-13 03:37:55',NULL),(275,'order.update','Update Order','Order','order',1,0,'2018-08-13 03:37:55',NULL),(276,'order.updateother','Update Others Order','Order','order',1,0,'2018-08-13 03:37:55',NULL),(277,'order.delete','Delete Order','Order','order',1,0,'2018-08-13 03:37:55',NULL),(278,'order.deleteother','Delete Others Order','Order','order',1,0,'2018-08-13 03:37:55',NULL),(279,'order.manage','Manage Orders','Order','order',1,0,'2018-08-13 03:37:55',NULL),(280,'access.payment','Access Tab','PaymentModule','payment',1,0,'2018-08-13 03:37:55',NULL),(281,'access.shipping','Access Tab','ShippingModule','shipping',1,0,'2018-08-13 03:37:55',NULL),(282,'access.stores','Access Tab','StoresModule','stores',1,0,'2018-08-13 03:37:55',NULL),(283,'store.create','Create Store','Store','stores',1,0,'2018-08-13 03:37:55',NULL),(284,'store.view','View Store','Store','stores',1,0,'2018-08-13 03:37:55',NULL),(285,'store.viewother','View Others Store','Store','stores',1,0,'2018-08-13 03:37:55',NULL),(286,'store.update','Update Store','Store','stores',1,0,'2018-08-13 03:37:55',NULL),(287,'store.updateother','Update Others Store','Store','stores',1,0,'2018-08-13 03:37:55',NULL),(288,'store.delete','Delete Store','Store','stores',1,0,'2018-08-13 03:37:55',NULL),(289,'store.deleteother','Delete Others Store','Store','stores',1,0,'2018-08-13 03:37:55',NULL),(290,'store.manage','Manage Stores','Store','stores',1,0,'2018-08-13 03:37:55',NULL),(291,'store.bulk-edit','Bulk Edit Store','Store','stores',1,0,'2018-08-13 03:37:55',NULL);
/*!40000 ALTER TABLE `tbl_auth_permission` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `tbl_city`
--

DROP TABLE IF EXISTS `tbl_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_city`
--

LOCK TABLES `tbl_city` WRITE;
/*!40000 ALTER TABLE `tbl_city` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_city_translated`
--

DROP TABLE IF EXISTS `tbl_city_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_city_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_city_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_city_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_city_translated`
--

LOCK TABLES `tbl_city_translated` WRITE;
/*!40000 ALTER TABLE `tbl_city_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_city_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_configuration`
--

DROP TABLE IF EXISTS `tbl_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(32) NOT NULL,
  `key` varchar(32) NOT NULL,
  `value` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_configuration`
--

LOCK TABLES `tbl_configuration` WRITE;
/*!40000 ALTER TABLE `tbl_configuration` DISABLE KEYS */;
INSERT INTO `tbl_configuration` (`id`, `module`, `key`, `value`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'application','dbAdminUsername','',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(2,'application','dbAdminPassword','',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(3,'application','siteName','Ecommerce Artesanal',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(4,'application','siteDescription','Ecommerce Artesanal',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(5,'application','superUsername','superadmin',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(6,'application','superEmail','m.sancheztarira@gmail.com',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(7,'application','superPassword','Soloyoentro2018',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(8,'application','dbHost','localhost',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(9,'application','dbPort','3306',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(10,'application','dbName','artesanal',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(11,'application','dbUsername','artesanal',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(12,'application','dbPassword','artesanal_pass',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(13,'application','environment','dev',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(14,'application','demoData','1',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(15,'application','timezone','America/Guayaquil',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(16,'application','logo','',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(17,'application','uploadInstance',NULL,1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(18,'application','enableSchemaCache','1',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(19,'application','schemaCachingDuration','3600',1,1,'2018-06-29 03:47:14','2018-06-29 03:47:14'),(20,'application','appRebuild','',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(21,'users','passwordTokenExpiry','3600',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(22,'application','installTime','2018-06-29 03:49:20',1,1,'2018-06-29 03:49:20','2018-06-29 03:49:20'),(23,'settings','emailSettings','a:10:{s:8:\"fromName\";s:13:\"Administrador\";s:11:\"fromAddress\";s:22:\"pucese.comex@gmail.com\";s:14:\"replyToAddress\";s:17:\"noreply@email.com\";s:13:\"sendingMethod\";s:4:\"smtp\";s:8:\"smtpHost\";s:14:\"smtp.gmail.com\";s:8:\"smtpPort\";s:3:\"587\";s:12:\"smtpUsername\";s:22:\"pucese.comex@gmail.com\";s:12:\"smtpPassword\";s:20:\"ComercioExterior2018\";s:8:\"smtpAuth\";i:0;s:8:\"testMode\";s:1:\"0\";}',1,1,'2018-08-05 11:59:04','2018-08-05 12:02:42');
/*!40000 ALTER TABLE `tbl_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_country`
--

DROP TABLE IF EXISTS `tbl_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postcode_required` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `iso_code_2` varchar(2) DEFAULT NULL,
  `iso_code_3` varchar(3) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_country`
--

LOCK TABLES `tbl_country` WRITE;
/*!40000 ALTER TABLE `tbl_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_country_translated`
--

DROP TABLE IF EXISTS `tbl_country_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address_format` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_country_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_country_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_country_translated`
--

LOCK TABLES `tbl_country_translated` WRITE;
/*!40000 ALTER TABLE `tbl_country_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_country_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_currency`
--

DROP TABLE IF EXISTS `tbl_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` smallint(6) DEFAULT NULL,
  `value` decimal(10,2) NOT NULL,
  `code` varchar(10) NOT NULL,
  `symbol_left` varchar(10) DEFAULT NULL,
  `symbol_right` varchar(10) DEFAULT NULL,
  `decimal_place` varchar(3) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_currency`
--

LOCK TABLES `tbl_currency` WRITE;
/*!40000 ALTER TABLE `tbl_currency` DISABLE KEYS */;
INSERT INTO `tbl_currency` (`id`, `status`, `value`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,1.00,'USD','$','','2',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15');
/*!40000 ALTER TABLE `tbl_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_currency_translated`
--

DROP TABLE IF EXISTS `tbl_currency_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_currency_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_currency_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_currency_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_currency_translated`
--

LOCK TABLES `tbl_currency_translated` WRITE;
/*!40000 ALTER TABLE `tbl_currency_translated` DISABLE KEYS */;
INSERT INTO `tbl_currency_translated` (`id`, `owner_id`, `language`, `name`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','US Dollars',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,1,'es-EC','Dólares Americanos',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15');
/*!40000 ALTER TABLE `tbl_currency_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_customer`
--

DROP TABLE IF EXISTS `tbl_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `unique_id` int(11) NOT NULL,
  `password_reset_token` varchar(128) DEFAULT NULL,
  `password_hash` varchar(128) DEFAULT NULL,
  `auth_key` varchar(128) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `login_ip` varchar(20) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `timezone` varchar(32) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_customer`
--

LOCK TABLES `tbl_customer` WRITE;
/*!40000 ALTER TABLE `tbl_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_customer_activity`
--

DROP TABLE IF EXISTS `tbl_customer_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_customer_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `key` varchar(128) NOT NULL,
  `data` longtext NOT NULL,
  `ip` varchar(164) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_customer_activity`
--

LOCK TABLES `tbl_customer_activity` WRITE;
/*!40000 ALTER TABLE `tbl_customer_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_customer_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_customer_download_mapping`
--

DROP TABLE IF EXISTS `tbl_customer_download_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_customer_download_mapping` (
  `customer_id` int(11) DEFAULT NULL,
  `download_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_customer_download_mapping`
--

LOCK TABLES `tbl_customer_download_mapping` WRITE;
/*!40000 ALTER TABLE `tbl_customer_download_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_customer_download_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_customer_metadata`
--

DROP TABLE IF EXISTS `tbl_customer_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_customer_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `cart` longtext DEFAULT NULL,
  `wishlist` longtext DEFAULT NULL,
  `compareproducts` longtext DEFAULT NULL,
  `currency` varchar(128) DEFAULT NULL,
  `language` varchar(128) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_customer_metadata`
--

LOCK TABLES `tbl_customer_metadata` WRITE;
/*!40000 ALTER TABLE `tbl_customer_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_customer_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_customer_online`
--

DROP TABLE IF EXISTS `tbl_customer_online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_customer_online` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(64) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `url` varchar(164) DEFAULT NULL,
  `referer` varchar(164) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_customer_online`
--

LOCK TABLES `tbl_customer_online` WRITE;
/*!40000 ALTER TABLE `tbl_customer_online` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_customer_online` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_data_category`
--

DROP TABLE IF EXISTS `tbl_data_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_data_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` smallint(6) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_data_category`
--

LOCK TABLES `tbl_data_category` WRITE;
/*!40000 ALTER TABLE `tbl_data_category` DISABLE KEYS */;
INSERT INTO `tbl_data_category` (`id`, `status`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16');
/*!40000 ALTER TABLE `tbl_data_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_data_category_translated`
--

DROP TABLE IF EXISTS `tbl_data_category_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_data_category_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_data_category_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_data_category_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_data_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_data_category_translated`
--

LOCK TABLES `tbl_data_category_translated` WRITE;
/*!40000 ALTER TABLE `tbl_data_category_translated` DISABLE KEYS */;
INSERT INTO `tbl_data_category_translated` (`id`, `owner_id`, `language`, `name`, `description`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','Root Category','This is root data category for the application under which all the data would reside',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(2,1,'es-EC','Categoría Principal','Categoría principal para la aplicación donde residirán todos los datos',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16');
/*!40000 ALTER TABLE `tbl_data_category_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_extension`
--

DROP TABLE IF EXISTS `tbl_extension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_extension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(16) NOT NULL,
  `author` varchar(128) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `product_version` longtext DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `code` varchar(32) DEFAULT NULL,
  `data` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_extension`
--

LOCK TABLES `tbl_extension` WRITE;
/*!40000 ALTER TABLE `tbl_extension` DISABLE KEYS */;
INSERT INTO `tbl_extension` (`id`, `category`, `author`, `version`, `product_version`, `status`, `code`, `data`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'payment','WhatACart','1.0','2.0.0',1,'cashondelivery',NULL,1,1,'2018-06-29 03:49:17','2018-06-29 15:57:57'),(2,'payment','WhatACart','1.0','2.0.0',1,'paypal_standard',NULL,1,1,'2018-06-29 03:49:17','2018-06-29 15:57:59'),(3,'shipping','WhatACart','1.0','2.0.0',1,'flat',NULL,1,1,'2018-06-29 03:49:17','2018-06-29 15:58:03'),(4,'shipping','WhatACart','1.0','2.0.0',1,'free',NULL,1,1,'2018-06-29 03:49:17','2018-06-29 15:58:04');
/*!40000 ALTER TABLE `tbl_extension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_extension_translated`
--

DROP TABLE IF EXISTS `tbl_extension_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_extension_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_extension_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_extension_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_extension` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_extension_translated`
--

LOCK TABLES `tbl_extension_translated` WRITE;
/*!40000 ALTER TABLE `tbl_extension_translated` DISABLE KEYS */;
INSERT INTO `tbl_extension_translated` (`id`, `owner_id`, `language`, `name`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','Cash On Delivery',1,1,'2018-06-29 03:49:17','2018-06-29 03:49:17'),(2,2,'en-US','Paypal Standard',1,1,'2018-06-29 03:49:17','2018-06-29 03:49:17'),(3,3,'en-US','Flat Rate',1,1,'2018-06-29 03:49:17','2018-06-29 03:49:17'),(4,4,'en-US','Free Shipping',1,1,'2018-06-29 03:49:17','2018-06-29 03:49:17'),(5,1,'es-EC','Efectivo',1,1,'2018-06-29 03:49:17','2018-06-29 03:49:17'),(6,2,'es-EC','Paypal',1,1,'2018-06-29 03:49:17','2018-06-29 03:49:17'),(7,3,'es-EC','Tarifa Única',1,1,'2018-06-29 03:49:17','2018-06-29 03:49:17'),(8,4,'es-EC','Envío Gratuito',1,1,'2018-06-29 03:49:17','2018-06-29 03:49:17');
/*!40000 ALTER TABLE `tbl_extension_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_group`
--

DROP TABLE IF EXISTS `tbl_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `category` varchar(16) NOT NULL DEFAULT 'system',
  `path` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_group`
--

LOCK TABLES `tbl_group` WRITE;
/*!40000 ALTER TABLE `tbl_group` DISABLE KEYS */;
INSERT INTO `tbl_group` (`id`, `name`, `parent_id`, `level`, `status`, `category`, `path`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'Cliente',0,0,1,'customer','7',1,1,'2018-08-13 02:24:25','2018-08-13 02:24:25'),(2,'Artesano',0,0,1,'system','2',1,1,'2018-11-14 01:27:57','2018-11-14 01:27:57');
/*!40000 ALTER TABLE `tbl_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_group_member`
--

DROP TABLE IF EXISTS `tbl_group_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_group_member` (
  `group_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `member_type` varchar(16) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_group_member`
--

LOCK TABLES `tbl_group_member` WRITE;
/*!40000 ALTER TABLE `tbl_group_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_group_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_language`
--

DROP TABLE IF EXISTS `tbl_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `locale` varchar(10) NOT NULL,
  `image` varchar(64) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `code` varchar(10) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_language`
--

LOCK TABLES `tbl_language` WRITE;
/*!40000 ALTER TABLE `tbl_language` DISABLE KEYS */;
INSERT INTO `tbl_language` (`id`, `name`, `locale`, `image`, `sort_order`, `status`, `code`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'English','en-US','',1,1,'en-US',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,'Español','es-EC','',1,1,'es-EC',1,1,'2018-07-30 13:36:08','2018-08-13 02:08:56');
/*!40000 ALTER TABLE `tbl_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_length_class`
--

DROP TABLE IF EXISTS `tbl_length_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_length_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(10) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_length_class`
--

LOCK TABLES `tbl_length_class` WRITE;
/*!40000 ALTER TABLE `tbl_length_class` DISABLE KEYS */;
INSERT INTO `tbl_length_class` (`id`, `unit`, `value`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'m',1.00,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,'cm',100.00,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(3,'in',39.37,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(4,'mm',1000.00,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15');
/*!40000 ALTER TABLE `tbl_length_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_length_class_translated`
--

DROP TABLE IF EXISTS `tbl_length_class_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_length_class_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_length_class_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_length_class_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_length_class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_length_class_translated`
--

LOCK TABLES `tbl_length_class_translated` WRITE;
/*!40000 ALTER TABLE `tbl_length_class_translated` DISABLE KEYS */;
INSERT INTO `tbl_length_class_translated` (`id`, `owner_id`, `language`, `name`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','Meter',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,2,'en-US','Centimeter',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(3,3,'en-US','Inch',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(4,4,'en-US','Millimeter',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(5,1,'es-EC','Metro',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(6,2,'es-EC','Centímetro',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(7,3,'es-EC','Pulgada',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(8,4,'es-EC','Milímetro',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15');
/*!40000 ALTER TABLE `tbl_length_class_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_manufacturer`
--

DROP TABLE IF EXISTS `tbl_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `image` varchar(64) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_manufacturer`
--

LOCK TABLES `tbl_manufacturer` WRITE;
/*!40000 ALTER TABLE `tbl_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_manufacturer_description`
--

DROP TABLE IF EXISTS `tbl_manufacturer_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_manufacturer_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `description` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_manufacturer_description_manufacturer_id` (`owner_id`),
  CONSTRAINT `fk_tbl_manufacturer_description_manufacturer_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_manufacturer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_manufacturer_description`
--

LOCK TABLES `tbl_manufacturer_description` WRITE;
/*!40000 ALTER TABLE `tbl_manufacturer_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_manufacturer_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_newsletter`
--

DROP TABLE IF EXISTS `tbl_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `subject` varchar(164) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_newsletter`
--

LOCK TABLES `tbl_newsletter` WRITE;
/*!40000 ALTER TABLE `tbl_newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_newsletter_customers`
--

DROP TABLE IF EXISTS `tbl_newsletter_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_newsletter_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `email` varchar(164) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_newsletter_customers`
--

LOCK TABLES `tbl_newsletter_customers` WRITE;
/*!40000 ALTER TABLE `tbl_newsletter_customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_newsletter_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_newsletter_translated`
--

DROP TABLE IF EXISTS `tbl_newsletter_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_newsletter_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `content` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_newsletter_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_newsletter_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_newsletter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_newsletter_translated`
--

LOCK TABLES `tbl_newsletter_translated` WRITE;
/*!40000 ALTER TABLE `tbl_newsletter_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_newsletter_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_notification`
--

DROP TABLE IF EXISTS `tbl_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modulename` varchar(16) NOT NULL,
  `type` varchar(16) NOT NULL,
  `data` longblob NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `priority` smallint(6) NOT NULL DEFAULT 1,
  `senddatetime` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_notification`
--

LOCK TABLES `tbl_notification` WRITE;
/*!40000 ALTER TABLE `tbl_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_notification_layout`
--

DROP TABLE IF EXISTS `tbl_notification_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification_layout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_notification_layout`
--

LOCK TABLES `tbl_notification_layout` WRITE;
/*!40000 ALTER TABLE `tbl_notification_layout` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_notification_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_notification_layout_translated`
--

DROP TABLE IF EXISTS `tbl_notification_layout_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification_layout_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `content` longblob NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_notification_layout_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_notification_layout_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_notification_layout` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_notification_layout_translated`
--

LOCK TABLES `tbl_notification_layout_translated` WRITE;
/*!40000 ALTER TABLE `tbl_notification_layout_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_notification_layout_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_notification_template`
--

DROP TABLE IF EXISTS `tbl_notification_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL,
  `notifykey` varchar(32) NOT NULL,
  `layout_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_notification_template`
--

LOCK TABLES `tbl_notification_template` WRITE;
/*!40000 ALTER TABLE `tbl_notification_template` DISABLE KEYS */;
INSERT INTO `tbl_notification_template` (`id`, `type`, `notifykey`, `layout_id`, `status`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'email','createUser',NULL,1,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,'email','changepassword',NULL,1,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(3,'email','forgotpassword',NULL,1,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(4,'email','productReview',NULL,1,1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),(5,'email','orderCompletion',NULL,1,1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),(6,'email','orderReceived',NULL,1,1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),(7,'email','orderUpdate',NULL,1,1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),(8,'email','sendMail',NULL,1,1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),(9,'email','sendNewsletter',NULL,1,1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),(10,'email','createCustomer',NULL,1,1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18');
/*!40000 ALTER TABLE `tbl_notification_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_notification_template_translated`
--

DROP TABLE IF EXISTS `tbl_notification_template_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification_template_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `subject` varchar(128) NOT NULL,
  `content` longblob NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_notification_template_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_notification_template_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_notification_template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_notification_template_translated`
--

LOCK TABLES `tbl_notification_template_translated` WRITE;
/*!40000 ALTER TABLE `tbl_notification_template_translated` DISABLE KEYS */;
INSERT INTO `tbl_notification_template_translated` (`id`, `owner_id`, `language`, `subject`, `content`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`)
VALUES
(1,1,'en-US','New User Registration','<p>Welcome {{fullName}}. Your account has been created successfully at {{appname}}</p>\n\n<p>Your login details are as below<br /><br/>\n    <strong>Username:</strong> {{username}}<br />\n    <strong>Password</strong>: {{password}}</p>\n\n{{confirmemailLabel}}\n{{confirmemail}}\n\nThanks,<br />\nSystem Admin',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),
(2,2,'en-US','You have changed your password','<p>Dear {{fullName}}, <br/><br/>Your password has been changed to {{password}}.\n<br/><br/>\nThanks<br/>\nSystem Admin</p>',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),
(3,3,'en-US','Forgot Password Request','<p>Dear {{fullName}},<br/>\nYour login details are as below<br>\n<strong>Username:</strong> {{username}}<br>\n<strong>Password</strong>: {{password}}\n<br/><br/>\nThanks<br>\nSystem Admin\n</p>',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),
(4,4,'en-US','Product Review | {{productName}}','<p>\n    Hello,<br/>\n    {{customername}} has posted a new review on {{productname}}.\n</p>\n<p>\n    The review is:<br/>\n    {{review}}<br/><br/>\n    Thanks,<br />\n    System Admin\n</p>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(5,5,'en-US','Order Completion','<p>Dear, {{customername}}</p>\r\n<p>\r\n    Your order #{{ordernumber}} processing is completed on {{orderdate}}.\r\n</p>\r\n{{orderLink}}\r\n<p>\r\n    Thank You, <br/> \r\n    System Admin\r\n</p>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(6,6,'en-US','Received Order','<div style=\"width: 680px;\">\r\n{{orderLink}}\r\n  <table style=\"border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;\">\r\n    <thead>\r\n      <tr>\r\n        <td style=\"font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;\" colspan=\"2\">\r\n            Order Details\r\n        </td>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr>\r\n        <td style=\"font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;\">\r\n          <b>Order ID:</b> {{orderId}}<br />\r\n          <b>Date of Order:</b> {{dateAdded}}<br />\r\n          </td>\r\n        <td style=\"font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;\">\r\n          <b>Email:</b> {{email}}<br />\r\n          <b>Telephone:</b> {{telephone}}<br />\r\n          <b>Status:</b> {{orderStatus}}<br />\r\n        </td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n  <table style=\"border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;\">\r\n    <thead>\r\n      <tr>\r\n        <td style=\"font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;\">\r\n            Billing Address\r\n        </td>\r\n        {{shippingAddressTitle}}\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr>\r\n        <td style=\"font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;\">\r\n            {{paymentAddress}}\r\n        </td>\r\n        {{shippingAddress}}\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n  {{orderProducts}}\r\n  <br/>\r\n{{orderDescription}}\r\n<br/>\r\n<p>\r\n      Thanks,<br/>\r\n      System Admin\r\n  </p>\r\n</div>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(7,7,'en-US','Update Order | {{ordernumber}}','<p>Dear {{customername}},</p>\n<p>\n    Your order #{{ordernumber}} status ordered on {{orderdate}} has been updated to {{orderstatus}}.\n</p>\n{{orderLink}}\n<p>\n    The comments for the order are:<br/>\n    {{ordercomments}}\n</p>\n<p>\n    Thank You, <br/> \n    System Admin\n</p>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(8,8,'en-US','Send Mail','<h1>{{appname}}</h1>\n<p>\n    <strong>From:</strong> {{storename}}<br />\n    <strong>Subject:</strong>: {{subject}}<br />\n    <strong>Message:</strong>: {{message}}\n</p>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(9,9,'en-US','Newsletter','<h1>{{appname}}</h1>\n<p>\n    <strong>From:</strong> {{storename}}<br />\n    <strong>Subject:</strong>: {{subject}}<br />\n    <strong>Message:</strong>: {{message}}\n</p>\n{{unsubscribe}}',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(10,10,'en-US','New Customer Registration','<p>Welcome {{fullName}}. Your account has been created successfully at {{appname}}</p>\n\n<p>Your login details are as below<br /><br/>\n    <strong>Username:</strong> {{username}}<br />\n    <strong>Password</strong>: {{password}}</p>\n\n{{confirmemailLabel}}\n{{confirmemail}}\n\nThanks,<br />\nSystem Admin\n\n',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(11,1,'es-EC','Nuevo Registro de Usuario','<p>Bienvenido {{fullName}}. Tu cuenta ha sido creada satisfactoriamente en {{appname}}</p>\r\n\r\n<p>A continuaciónn los detalles de tu cuenta<br /><br/>\r\n    <strong>Usuario:</strong> {{username}}<br />\r\n    <strong>Contraseña</strong>: {{password}}</p>\r\n\r\n{{confirmemailLabel}}\r\n{{confirmemail}}\r\n\r\nGracias,<br />\r\nAdministrador',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),
(12,2,'es-EC','Has Cambiado la Contraseña','<p>Estimado {{fullName}}, <br/><br/>Su contraseña ha sido cambiada a la siguiente {{password}}.\r\n<br/><br/>\r\nGracias<br/>\r\nAdministrado</p>',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(13,3,'es-EC','Solicitud de Cambio de Contraseña','<p>Estimado {{fullName}},<br/>\r\nSus credenciales son los siguientes<br>\r\n<strong>Usuario:</strong> {{username}}<br>\r\n<strong>Contraseña</strong>: {{password}}\r\n<br/><br/>\r\nGracias<br>\r\nAdministrador\r\n</p>',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(14,4,'es-EC','Revisión de Producto | {{productName}}','<p>\r\n    Holla,<br/>\r\n    {{customername}} ha realizado una reseña del producto {{productname}}.\r\n</p>\r\n<p>\r\n    La reseña es la siguiente:<br/>\r\n    {{review}}<br/><br/>\r\n    Gracias,<br />\r\n    Administrador\r\n</p>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(15,5,'es-EC','Terminación de Pedido','<p>Estimado, {{customername}}</p>\r\n<p>\r\n    Su orden #{{ordernumber}} fue completada el {{orderdate}}.\r\n</p>\r\n{{orderLink}}\r\n<p>\r\n    Gracias, <br/> \r\n    Administrador\r\n</p>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(16,6,'es-EC','Pedido Recibido','<div style=\"width: 680px;\">\r\n{{orderLink}}\r\n  <table style=\"border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;\">\r\n    <thead>\r\n      <tr>\r\n        <td style=\"font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;\" colspan=\"2\">\r\n            Detalle del Pedido\r\n        </td>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr>\r\n        <td style=\"font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;\">\r\n          <b>ID del Pedido:</b> {{orderId}}<br />\r\n          <b>Decah del Pedido:</b> {{dateAdded}}<br />\r\n          </td>\r\n        <td style=\"font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;\">\r\n          <b>Email:</b> {{email}}<br />\r\n          <b>Contacto:</b> {{telephone}}<br />\r\n          <b>Estado:</b> {{orderStatus}}<br />\r\n        </td>\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n  <table style=\"border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;\">\r\n    <thead>\r\n      <tr>\r\n        <td style=\"font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;\">\r\n            Billing Address\r\n        </td>\r\n        {{shippingAddressTitle}}\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr>\r\n        <td style=\"font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;\">\r\n            {{paymentAddress}}\r\n        </td>\r\n        {{shippingAddress}}\r\n      </tr>\r\n    </tbody>\r\n  </table>\r\n  {{orderProducts}}\r\n<br/>\r\n{{orderDescription}}\r\n<br/>\r\n  <p>\r\n      Gracias,<br/>\r\n      Administrador\r\n  </p>\r\n</div>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(17,7,'es-EC','Actualizar Pedido | {{ordernumber}}','<p>Estimado {{customername}},</p>\r\n<p>\r\n    El estado de su pedido #{{ordernumber}} con fecha {{orderdate}} ha sido actualizado a {{orderstatus}}.\r\n</p>\r\n{{orderLink}}\r\n<p>\r\n    Comentarios recibidos:<br/>\r\n    {{ordercomments}}\r\n</p>\r\n<p>\r\n    Gracias, <br/> \r\n    Administrador\r\n</p>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(18,8,'es-EC','Enviar Correo','<h1>{{appname}}</h1>\n<p>\n    <strong>From:</strong> {{storename}}<br />\n    <strong>Subject:</strong>: {{subject}}<br />\n    <strong>Message:</strong>: {{message}}\n</p>',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(19,9,'es-EC','Newsletter','<h1>{{appname}}</h1>\n<p>\n    <strong>From:</strong> {{storename}}<br />\n    <strong>Subject:</strong>: {{subject}}<br />\n    <strong>Message:</strong>: {{message}}\n</p>\n{{unsubscribe}}',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18'),
(20,10,'es-EC','Nuevo Registro de Cliente','<p>Bienvenido {{fullName}}. Su cuenta ha sido creada satisfactoriamente en {{appname}}</p>\r\n\r\n<p>Sus credenciales son los siguientes<br /><br/>\r\n    <strong>Usuario:</strong> {{username}}<br />\r\n    <strong>Contraseña</strong>: {{password}}</p>\r\n\r\n{{confirmemailLabel}}\r\n{{confirmemail}}\r\n\r\nGracias,<br />\r\nAdministrador',1,1,'2018-06-29 03:49:18','2018-06-29 03:49:18');
/*!40000 ALTER TABLE `tbl_notification_template_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order`
--

DROP TABLE IF EXISTS `tbl_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `shipping` varchar(64) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `shipping_fee` decimal(10,2) DEFAULT 0.00,
  `unique_id` int(11) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `currency_conversion_value` double NOT NULL DEFAULT 1,
  `interface` varchar(6) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order`
--

LOCK TABLES `tbl_order` WRITE;
/*!40000 ALTER TABLE `tbl_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_address_details`
--

DROP TABLE IF EXISTS `tbl_order_address_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_address_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `firstname` varchar(32) DEFAULT NULL,
  `lastname` varchar(32) DEFAULT NULL,
  `mobilephone` varchar(16) DEFAULT NULL,
  `officephone` varchar(16) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `country` varchar(10) DEFAULT NULL,
  `postal_code` varchar(16) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_address_details_order_id` (`order_id`),
  CONSTRAINT `fk_tbl_order_address_details_order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_address_details`
--

LOCK TABLES `tbl_order_address_details` WRITE;
/*!40000 ALTER TABLE `tbl_order_address_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_address_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_history`
--

DROP TABLE IF EXISTS `tbl_order_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `notify_customer` smallint(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_history_order_id` (`order_id`),
  CONSTRAINT `fk_tbl_order_history_order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_history`
--

LOCK TABLES `tbl_order_history` WRITE;
/*!40000 ALTER TABLE `tbl_order_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_history_translated`
--

DROP TABLE IF EXISTS `tbl_order_history_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_history_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_history_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_order_history_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_order_history` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_history_translated`
--

LOCK TABLES `tbl_order_history_translated` WRITE;
/*!40000 ALTER TABLE `tbl_order_history_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_history_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_payment_details`
--

DROP TABLE IF EXISTS `tbl_order_payment_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_payment_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `payment_method` varchar(164) NOT NULL,
  `payment_type` varchar(64) DEFAULT NULL,
  `total_including_tax` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `shipping_fee` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_payment_details_order_id` (`order_id`),
  CONSTRAINT `fk_tbl_order_payment_details_order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_payment_details`
--

LOCK TABLES `tbl_order_payment_details` WRITE;
/*!40000 ALTER TABLE `tbl_order_payment_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_payment_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_payment_details_translated`
--

DROP TABLE IF EXISTS `tbl_order_payment_details_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_payment_details_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_payment_details_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_order_payment_details_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_order_payment_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_payment_details_translated`
--

LOCK TABLES `tbl_order_payment_details_translated` WRITE;
/*!40000 ALTER TABLE `tbl_order_payment_details_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_payment_details_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_payment_transaction_map`
--

DROP TABLE IF EXISTS `tbl_order_payment_transaction_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_payment_transaction_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `payment_method` varchar(20) NOT NULL,
  `transaction_record_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_payment_transaction_map_order_id` (`order_id`),
  CONSTRAINT `fk_tbl_order_payment_transaction_map_order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_payment_transaction_map`
--

LOCK TABLES `tbl_order_payment_transaction_map` WRITE;
/*!40000 ALTER TABLE `tbl_order_payment_transaction_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_payment_transaction_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_product`
--

DROP TABLE IF EXISTS `tbl_order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `options` longtext DEFAULT NULL,
  `displayed_options` longtext DEFAULT NULL,
  `item_code` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `model` varchar(128) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT 0.00,
  `options_price` decimal(10,2) DEFAULT 0.00,
  `total` decimal(10,2) DEFAULT 0.00,
  `tax` decimal(10,2) DEFAULT 0.00,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_product_order_id` (`order_id`),
  CONSTRAINT `fk_tbl_order_product_order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_product`
--

LOCK TABLES `tbl_order_product` WRITE;
/*!40000 ALTER TABLE `tbl_order_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_status`
--

DROP TABLE IF EXISTS `tbl_order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_status`
--

LOCK TABLES `tbl_order_status` WRITE;
/*!40000 ALTER TABLE `tbl_order_status` DISABLE KEYS */;
INSERT INTO `tbl_order_status` (`id`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(3,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(4,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(5,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(6,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(7,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(8,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(9,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(10,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(11,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(12,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(13,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(14,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16');
/*!40000 ALTER TABLE `tbl_order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_status_translated`
--

DROP TABLE IF EXISTS `tbl_order_status_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_status_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_status_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_order_status_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_order_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_status_translated`
--

LOCK TABLES `tbl_order_status_translated` WRITE;
/*!40000 ALTER TABLE `tbl_order_status_translated` DISABLE KEYS */;
INSERT INTO `tbl_order_status_translated` (`id`, `owner_id`, `language`, `name`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','Cancelled',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,2,'en-US','Canceled_Reversal',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(3,3,'en-US','Chargeback',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(4,4,'en-US','Completed',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(5,5,'en-US','Denied',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(6,6,'en-US','Expired',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(7,7,'en-US','Failed',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(8,8,'en-US','Pending',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(9,9,'en-US','Processed',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(10,10,'en-US','Processing',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(11,11,'en-US','Refunded',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(12,12,'en-US','Reversed',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(13,13,'en-US','Shipped',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(14,14,'en-US','Voided',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(15,1,'es-EC','Cancelado',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(16,2,'es-EC','Revertir_Cancelado',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(17,3,'es-EC','Contracargo',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(18,4,'es-EC','Completado',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(19,5,'es-EC','Denegado',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(20,6,'es-EC','Expirado',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(21,7,'es-EC','Fallido',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(22,8,'es-EC','Pendiente',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(23,9,'es-EC','Procesado',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(24,10,'es-EC','Procesando',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(25,11,'es-EC','Reembolsado',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(26,12,'es-EC','Revertido',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(27,13,'es-EC','Enviado',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(28,14,'es-EC','Anulado',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16');
/*!40000 ALTER TABLE `tbl_order_status_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order_translated`
--

DROP TABLE IF EXISTS `tbl_order_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `shipping_comments` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_order_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_order_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order_translated`
--

LOCK TABLES `tbl_order_translated` WRITE;
/*!40000 ALTER TABLE `tbl_order_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_order_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_page`
--

DROP TABLE IF EXISTS `tbl_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` smallint(6) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `custom_url` varchar(64) DEFAULT NULL,
  `level` smallint(6) NOT NULL,
  `path` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_page`
--

LOCK TABLES `tbl_page` WRITE;
/*!40000 ALTER TABLE `tbl_page` DISABLE KEYS */;
INSERT INTO `tbl_page` (`id`, `status`, `parent_id`, `custom_url`, `level`, `path`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,0,NULL,0,'1',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(2,1,0,NULL,0,'2',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(3,1,0,NULL,0,'3',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(4,1,0,NULL,0,'4',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16');
/*!40000 ALTER TABLE `tbl_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_page_translated`
--

DROP TABLE IF EXISTS `tbl_page_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_page_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `menuitem` varchar(128) NOT NULL,
  `content` longtext DEFAULT NULL,
  `summary` longtext DEFAULT NULL,
  `metakeywords` longtext DEFAULT NULL,
  `metadescription` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_page_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_page_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_page_translated`
--

LOCK TABLES `tbl_page_translated` WRITE;
/*!40000 ALTER TABLE `tbl_page_translated` DISABLE KEYS */;
INSERT INTO `tbl_page_translated` (`id`, `owner_id`, `language`, `name`, `alias`, `menuitem`, `content`, `summary`, `metakeywords`, `metadescription`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','About Us','about-us','About Us','<p>\n    <strong class=\"first-paragraph\">A</strong>t Usha Informatique, Web Development Company in India, we are driven by SPEED and EFFICIENCY to achieve superior quality and cost-competitiveness so as to enable our customer&rsquo;s stay at the forefront of their industry.</p><p>At Usha Informatique, you can find a right combination of Technical excellence, outstanding design, effective strategy and the results are pretty impressive, to serve clients acroos the globe. We utilizes both continued technical and intellectual education to enhance each project that is brought to Usha Informatique that stands our clients into the world of technology with class.</p><p>Our knowledge and experience in Software and Web solutions have greatly boosted our clients in business augmentation. We specialize in delivering cost-effective software/web solutions by implementing an offshore development model. We have a dedicated team of software professionals to bring quality products to the clients.\n</p>\n\n','About us summary','about us','about us description',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(2,2,'en-US','Delivery Information','delivery-info','Delivery Information','<p>This is delivery information</p>','Delivery information summary','delivery information','deliverr information description',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(3,3,'en-US','Privacy Policy','privacy-policy','Privacy Policy','<p>This is privacy policy</p>','Privacy policy summary','privacy policy','privacy policy description',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(4,4,'en-US','Terms & Conditions','terms','Terms & Conditions','<p>These are terms and conditions</p>','Terms & condition summary','terms & condition','terms & condition description',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(5,1,'es-EC','Acerca de Nosotros','about-us','Acerca de Nosotros','<p>\n    <strong class=\"first-paragraph\">A</strong>t Usha Informatique, Web Development Company in India, we are driven by SPEED and EFFICIENCY to achieve superior quality and cost-competitiveness so as to enable our customer&rsquo;s stay at the forefront of their industry.</p><p>At Usha Informatique, you can find a right combination of Technical excellence, outstanding design, effective strategy and the results are pretty impressive, to serve clients acroos the globe. We utilizes both continued technical and intellectual education to enhance each project that is brought to Usha Informatique that stands our clients into the world of technology with class.</p><p>Our knowledge and experience in Software and Web solutions have greatly boosted our clients in business augmentation. We specialize in delivering cost-effective software/web solutions by implementing an offshore development model. We have a dedicated team of software professionals to bring quality products to the clients.\n</p>\n\n','About us summary','about us','about us description',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(6,2,'es-EC','Información de Entrega','delivery-info','Delivery Information','<p>This is delivery information</p>','Delivery information summary','delivery information','deliverr information description',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(7,3,'es-EC','Políticas de Privacidad','privacy-policy','Privacy Policy','<p>This is privacy policy</p>','Privacy policy summary','privacy policy','privacy policy description',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(8,4,'es-EC','Términos y Condiciones','terms','Terms & Conditions','<p>These are terms and conditions</p>','Terms & condition summary','terms & condition','terms & condition description',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16');
/*!40000 ALTER TABLE `tbl_page_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_paypal_standard_transaction`
--

DROP TABLE IF EXISTS `tbl_paypal_standard_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_paypal_standard_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `payment_status` varchar(32) NOT NULL,
  `received_date` date NOT NULL,
  `transaction_id` varchar(32) NOT NULL,
  `transaction_fee` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_paypal_standard_transaction_order_id` (`order_id`),
  CONSTRAINT `fk_tbl_paypal_standard_transaction_order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_paypal_standard_transaction`
--

LOCK TABLES `tbl_paypal_standard_transaction` WRITE;
/*!40000 ALTER TABLE `tbl_paypal_standard_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_paypal_standard_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_person`
--

DROP TABLE IF EXISTS `tbl_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) DEFAULT NULL,
  `lastname` varchar(32) DEFAULT NULL,
  `mobilephone` varchar(16) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `avatar` varchar(128) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_person`
--

LOCK TABLES `tbl_person` WRITE;
/*!40000 ALTER TABLE `tbl_person` DISABLE KEYS */;
INSERT INTO `tbl_person` (`id`, `firstname`, `lastname`, `mobilephone`, `email`, `avatar`, `profile_image`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'Super','Admin','','pucese.comex@gmail.com',NULL,'NGRiNDdlODcomex.png',1,1,'2018-06-29 03:47:15','2018-07-29 20:51:01');
/*!40000 ALTER TABLE `tbl_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product`
--

DROP TABLE IF EXISTS `tbl_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) DEFAULT 1,
  `model` varchar(64) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT 0.00,
  `buy_price` decimal(10,2) DEFAULT 0.00,
  `image` varchar(64) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `sku` varchar(16) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `initial_quantity` int(11) DEFAULT NULL,
  `tax_class_id` int(11) DEFAULT NULL,
  `minimum_quantity` int(11) DEFAULT NULL,
  `subtract_stock` varchar(5) DEFAULT NULL,
  `stock_status` smallint(6) DEFAULT NULL,
  `requires_shipping` smallint(6) DEFAULT NULL,
  `available_date` date DEFAULT NULL,
  `manufacturer` int(11) DEFAULT NULL,
  `is_featured` smallint(6) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `date_available` date DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  `length_class` int(11) DEFAULT NULL,
  `weight_class` int(11) DEFAULT NULL,
  `hits` int(11) NOT NULL DEFAULT 0,
  `upc` varchar(12) DEFAULT NULL,
  `ean` varchar(14) DEFAULT NULL,
  `jan` varchar(13) DEFAULT NULL,
  `isbn` varchar(17) DEFAULT NULL,
  `mpn` varchar(64) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product`
--

LOCK TABLES `tbl_product` WRITE;
/*!40000 ALTER TABLE `tbl_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_attribute`
--

DROP TABLE IF EXISTS `tbl_product_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `attribute_group` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_attribute`
--

LOCK TABLES `tbl_product_attribute` WRITE;
/*!40000 ALTER TABLE `tbl_product_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_attribute_group`
--

DROP TABLE IF EXISTS `tbl_product_attribute_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_attribute_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_attribute_group`
--

LOCK TABLES `tbl_product_attribute_group` WRITE;
/*!40000 ALTER TABLE `tbl_product_attribute_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_attribute_group_translated`
--

DROP TABLE IF EXISTS `tbl_product_attribute_group_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_attribute_group_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_attribute_group_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_attribute_group_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_attribute_group_translated`
--

LOCK TABLES `tbl_product_attribute_group_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_attribute_group_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_attribute_group_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_attribute_mapping`
--

DROP TABLE IF EXISTS `tbl_product_attribute_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_attribute_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `attribute_value` varchar(32) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_attribute_mapping_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_attribute_mapping_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_attribute_mapping`
--

LOCK TABLES `tbl_product_attribute_mapping` WRITE;
/*!40000 ALTER TABLE `tbl_product_attribute_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_attribute_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_attribute_translated`
--

DROP TABLE IF EXISTS `tbl_product_attribute_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_attribute_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_attribute_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_attribute_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_attribute_translated`
--

LOCK TABLES `tbl_product_attribute_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_attribute_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_attribute_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_category`
--

DROP TABLE IF EXISTS `tbl_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(64) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `displayintopmenu` smallint(6) DEFAULT NULL,
  `data_category_id` int(11) NOT NULL,
  `code` varchar(164) NOT NULL,
  `path` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_category`
--

LOCK TABLES `tbl_product_category` WRITE;
/*!40000 ALTER TABLE `tbl_product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_category_mapping`
--

DROP TABLE IF EXISTS `tbl_product_category_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_category_mapping` (
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `data_category_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  KEY `fk_tbl_product_category_mapping_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_category_mapping_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_category_mapping`
--

LOCK TABLES `tbl_product_category_mapping` WRITE;
/*!40000 ALTER TABLE `tbl_product_category_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_category_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_category_translated`
--

DROP TABLE IF EXISTS `tbl_product_category_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_category_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `metakeywords` varchar(128) DEFAULT NULL,
  `metadescription` varchar(128) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_category_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_category_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_category_translated`
--

LOCK TABLES `tbl_product_category_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_category_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_category_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_discount`
--

DROP TABLE IF EXISTS `tbl_product_discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_discount_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_discount_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_discount`
--

LOCK TABLES `tbl_product_discount` WRITE;
/*!40000 ALTER TABLE `tbl_product_discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_download`
--

DROP TABLE IF EXISTS `tbl_product_download`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(128) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `allowed_downloads` int(11) DEFAULT 0,
  `number_of_days` int(11) DEFAULT 0,
  `size` double DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_download`
--

LOCK TABLES `tbl_product_download` WRITE;
/*!40000 ALTER TABLE `tbl_product_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_download` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_download_mapping`
--

DROP TABLE IF EXISTS `tbl_product_download_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_download_mapping` (
  `product_id` int(11) DEFAULT NULL,
  `download_id` int(11) DEFAULT NULL,
  `download_option` varchar(28) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  KEY `fk_tbl_product_download_mapping_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_download_mapping_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_download_mapping`
--

LOCK TABLES `tbl_product_download_mapping` WRITE;
/*!40000 ALTER TABLE `tbl_product_download_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_download_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_download_translated`
--

DROP TABLE IF EXISTS `tbl_product_download_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_download_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_download_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_download_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_download` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_download_translated`
--

LOCK TABLES `tbl_product_download_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_download_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_download_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_image`
--

DROP TABLE IF EXISTS `tbl_product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(64) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_image`
--

LOCK TABLES `tbl_product_image` WRITE;
/*!40000 ALTER TABLE `tbl_product_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_image_translated`
--

DROP TABLE IF EXISTS `tbl_product_image_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_image_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `caption` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_image_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_image_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_image` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_image_translated`
--

LOCK TABLES `tbl_product_image_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_image_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_image_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_option`
--

DROP TABLE IF EXISTS `tbl_product_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_option`
--

LOCK TABLES `tbl_product_option` WRITE;
/*!40000 ALTER TABLE `tbl_product_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_option_mapping`
--

DROP TABLE IF EXISTS `tbl_product_option_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_option_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `required` smallint(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_option_mapping_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_option_mapping_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_option_mapping`
--

LOCK TABLES `tbl_product_option_mapping` WRITE;
/*!40000 ALTER TABLE `tbl_product_option_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_option_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_option_mapping_details`
--

DROP TABLE IF EXISTS `tbl_product_option_mapping_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_option_mapping_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mapping_id` int(11) NOT NULL,
  `option_value_id` varchar(32) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `subtract_stock` smallint(6) NOT NULL,
  `price_prefix` varchar(1) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `weight_prefix` varchar(1) DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_option_mapping_details`
--

LOCK TABLES `tbl_product_option_mapping_details` WRITE;
/*!40000 ALTER TABLE `tbl_product_option_mapping_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_option_mapping_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_option_translated`
--

DROP TABLE IF EXISTS `tbl_product_option_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_option_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `display_name` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_option_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_option_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_option` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_option_translated`
--

LOCK TABLES `tbl_product_option_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_option_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_option_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_option_value`
--

DROP TABLE IF EXISTS `tbl_product_option_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_option_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_option_value`
--

LOCK TABLES `tbl_product_option_value` WRITE;
/*!40000 ALTER TABLE `tbl_product_option_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_option_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_option_value_translated`
--

DROP TABLE IF EXISTS `tbl_product_option_value_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_option_value_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `value` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_option_value_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_option_value_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_option_value` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_option_value_translated`
--

LOCK TABLES `tbl_product_option_value_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_option_value_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_option_value_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_rating`
--

DROP TABLE IF EXISTS `tbl_product_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` decimal(10,2) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_rating_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_rating_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_rating`
--

LOCK TABLES `tbl_product_rating` WRITE;
/*!40000 ALTER TABLE `tbl_product_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_related_product_mapping`
--

DROP TABLE IF EXISTS `tbl_product_related_product_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_related_product_mapping` (
  `product_id` int(11) DEFAULT NULL,
  `related_product_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  KEY `fk_tbl_product_related_product_mapping_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_related_product_mapping_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_related_product_mapping`
--

LOCK TABLES `tbl_product_related_product_mapping` WRITE;
/*!40000 ALTER TABLE `tbl_product_related_product_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_related_product_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_review`
--

DROP TABLE IF EXISTS `tbl_product_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `status` smallint(6) NOT NULL,
  `product_id` int(11) NOT NULL,
  `email` varchar(64) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_review_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_review_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_review`
--

LOCK TABLES `tbl_product_review` WRITE;
/*!40000 ALTER TABLE `tbl_product_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_review_translated`
--

DROP TABLE IF EXISTS `tbl_product_review_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_review_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `review` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_review_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_review_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_review_translated`
--

LOCK TABLES `tbl_product_review_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_review_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_review_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_special`
--

DROP TABLE IF EXISTS `tbl_product_special`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_special` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_special_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_special_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_special`
--

LOCK TABLES `tbl_product_special` WRITE;
/*!40000 ALTER TABLE `tbl_product_special` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_special` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_tag_mapping`
--

DROP TABLE IF EXISTS `tbl_product_tag_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_tag_mapping` (
  `product_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  KEY `fk_tbl_product_tag_mapping_product_id` (`product_id`),
  CONSTRAINT `fk_tbl_product_tag_mapping_product_id` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_tag_mapping`
--

LOCK TABLES `tbl_product_tag_mapping` WRITE;
/*!40000 ALTER TABLE `tbl_product_tag_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_tag_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_tax_class`
--

DROP TABLE IF EXISTS `tbl_product_tax_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_tax_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_tax_class`
--

LOCK TABLES `tbl_product_tax_class` WRITE;
/*!40000 ALTER TABLE `tbl_product_tax_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_tax_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_tax_class_translated`
--

DROP TABLE IF EXISTS `tbl_product_tax_class_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_tax_class_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(164) NOT NULL,
  `description` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_tax_class_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_tax_class_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product_tax_class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_tax_class_translated`
--

LOCK TABLES `tbl_product_tax_class_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_tax_class_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_tax_class_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product_translated`
--

DROP TABLE IF EXISTS `tbl_product_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `metakeywords` varchar(128) DEFAULT NULL,
  `metadescription` varchar(128) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_product_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_product_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product_translated`
--

LOCK TABLES `tbl_product_translated` WRITE;
/*!40000 ALTER TABLE `tbl_product_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_product_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sequence`
--

DROP TABLE IF EXISTS `tbl_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sequence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_sequence_no` varchar(11) NOT NULL,
  `customer_sequence_no` varchar(11) NOT NULL,
  `order_sequence_no` varchar(11) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sequence`
--

LOCK TABLES `tbl_sequence` WRITE;
/*!40000 ALTER TABLE `tbl_sequence` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_session`
--

DROP TABLE IF EXISTS `tbl_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_session` (
  `id` varchar(40) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_session`
--

LOCK TABLES `tbl_session` WRITE;
/*!40000 ALTER TABLE `tbl_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_state`
--

DROP TABLE IF EXISTS `tbl_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_state`
--

LOCK TABLES `tbl_state` WRITE;
/*!40000 ALTER TABLE `tbl_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_state_translated`
--

DROP TABLE IF EXISTS `tbl_state_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_state_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_state_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_state_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_state` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_state_translated`
--

LOCK TABLES `tbl_state_translated` WRITE;
/*!40000 ALTER TABLE `tbl_state_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_state_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stock_status`
--

DROP TABLE IF EXISTS `tbl_stock_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stock_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stock_status`
--

LOCK TABLES `tbl_stock_status` WRITE;
/*!40000 ALTER TABLE `tbl_stock_status` DISABLE KEYS */;
INSERT INTO `tbl_stock_status` (`id`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(2,1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16');
/*!40000 ALTER TABLE `tbl_stock_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stock_status_translated`
--

DROP TABLE IF EXISTS `tbl_stock_status_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stock_status_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_stock_status_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_stock_status_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_stock_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stock_status_translated`
--

LOCK TABLES `tbl_stock_status_translated` WRITE;
/*!40000 ALTER TABLE `tbl_stock_status_translated` DISABLE KEYS */;
INSERT INTO `tbl_stock_status_translated` (`id`, `owner_id`, `language`, `name`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','In Stock',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(2,2,'en-US','Out Of Stock',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(3,1,'es-EC','En Stock',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16'),(4,2,'es-EC','Fuera de Stock',1,1,'2018-06-29 03:47:16','2018-06-29 03:47:16');
/*!40000 ALTER TABLE `tbl_stock_status_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_store`
--

DROP TABLE IF EXISTS `tbl_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(64) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `data_category_id` int(11) NOT NULL,
  `is_default` smallint(6) NOT NULL DEFAULT 0,
  `owner_id` int(11) NOT NULL,
  `theme` varchar(16) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_store`
--

LOCK TABLES `tbl_store` WRITE;
/*!40000 ALTER TABLE `tbl_store` DISABLE KEYS */;
INSERT INTO `tbl_store` (`id`, `url`, `status`, `data_category_id`, `is_default`, `owner_id`, `theme`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'http://teststore.org',1,1,0,1,'',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33');
/*!40000 ALTER TABLE `tbl_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_store_configuration`
--

DROP TABLE IF EXISTS `tbl_store_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `category` varchar(32) NOT NULL,
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` longtext NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_store_configuration`
--

LOCK TABLES `tbl_store_configuration` WRITE;
/*!40000 ALTER TABLE `tbl_store_configuration` DISABLE KEYS */;
INSERT INTO `tbl_store_configuration` (`id`, `store_id`, `category`, `code`, `key`, `value`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'storeconfig','storesettings','invoice_prefix','#',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(2,1,'storeconfig','storesettings','catalog_items_per_page','8',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(3,1,'storeconfig','storesettings','list_description_limit','100',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(4,1,'storeconfig','storesettings','display_price_with_tax','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(5,1,'storeconfig','storesettings','tax_calculation_based_on','billing',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(6,1,'storeconfig','storesettings','guest_checkout','0',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(7,1,'storeconfig','storesettings','order_status','8',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(8,1,'storeconfig','storesettings','display_stock','0',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(9,1,'storeconfig','storesettings','customer_online','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(10,1,'storeconfig','storesettings','default_customer_group','',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(11,1,'storeconfig','storesettings','allow_reviews','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(12,1,'storeconfig','storesettings','allow_guest_reviews','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(13,1,'storeconfig','storesettings','show_out_of_stock_warning','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(14,1,'storeconfig','storesettings','allow_out_of_stock_checkout','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(15,1,'storeconfig','storesettings','allow_wishlist','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(16,1,'storeconfig','storesettings','allow_compare_products','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(17,1,'storeconfig','storesettings','customer_prefix','#',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(18,1,'storeconfig','storesettings','order_prefix','#',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(19,1,'storeconfig','storesettings','display_weight','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(20,1,'storeconfig','storesettings','display_dimensions','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(21,1,'storeconfig','storelocal','country','EC',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(22,1,'storeconfig','storelocal','timezone','America/Guyana',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(23,1,'storeconfig','storelocal','state','Esmeraldas',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(24,1,'storeconfig','storelocal','currency','USD',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(25,1,'storeconfig','storelocal','length_class','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(26,1,'storeconfig','storelocal','weight_class','1',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(27,1,'storeconfig','storelocal','language','en-US',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(28,1,'storeconfig','storeimage','store_logo','',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(29,1,'storeconfig','storeimage','icon','',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(30,1,'storeconfig','storeimage','category_image_width','90',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(31,1,'storeconfig','storeimage','category_image_height','90',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(32,1,'storeconfig','storeimage','product_list_image_width','150',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(33,1,'storeconfig','storeimage','product_list_image_height','150',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(34,1,'storeconfig','storeimage','related_product_image_width','80',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(35,1,'storeconfig','storeimage','related_product_image_height','80',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(36,1,'storeconfig','storeimage','compare_image_width','90',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(37,1,'storeconfig','storeimage','compare_image_height','90',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(38,1,'storeconfig','storeimage','wishlist_image_width','47',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(39,1,'storeconfig','storeimage','wishlist_image_height','47',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(40,1,'storeconfig','storeimage','cart_image_width','47',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(41,1,'storeconfig','storeimage','cart_image_height','47',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(42,1,'storeconfig','storeimage','store_image_width','47',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(43,1,'storeconfig','storeimage','store_image_height','47',1,1,'2018-06-29 03:49:17','2018-08-13 02:29:33'),(44,1,'payment','cashondelivery','order_status','8',1,1,'2018-06-29 15:57:57','2018-06-29 15:57:57'),(45,1,'payment','paypal_standard_orderstatus_map','canceled_reversal_status','2',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(46,1,'payment','paypal_standard_orderstatus_map','completed_status','4',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(47,1,'payment','paypal_standard_orderstatus_map','denied_status','5',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(48,1,'payment','paypal_standard_orderstatus_map','expired_status','6',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(49,1,'payment','paypal_standard_orderstatus_map','failed_status','7',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(50,1,'payment','paypal_standard_orderstatus_map','pending_status','8',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(51,1,'payment','paypal_standard_orderstatus_map','processed_status','9',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(52,1,'payment','paypal_standard_orderstatus_map','refunded_status','11',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(53,1,'payment','paypal_standard_orderstatus_map','reversed_status','12',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(54,1,'payment','paypal_standard_orderstatus_map','voided_status','14',1,1,'2018-06-29 15:57:59','2018-06-29 15:57:59'),(55,1,'shipping','flat','method_name','fixed',1,1,'2018-06-29 15:58:03','2018-06-29 15:58:03'),(56,1,'shipping','flat','calculateHandlingFee','fixed',1,1,'2018-06-29 15:58:03','2018-06-29 15:58:03'),(57,1,'shipping','flat','handlingFee','0',1,1,'2018-06-29 15:58:03','2018-06-29 15:58:03'),(58,1,'shipping','flat','type','perItem',1,1,'2018-06-29 15:58:03','2018-06-29 15:58:03'),(59,1,'shipping','flat','applicableZones','1',1,1,'2018-06-29 15:58:03','2018-06-29 15:58:03'),(60,1,'shipping','flat','specificZones','a:0:{}',1,1,'2018-06-29 15:58:03','2018-06-29 15:58:03'),(61,1,'shipping','flat','price','5',1,1,'2018-06-29 15:58:03','2018-06-29 15:58:03');
/*!40000 ALTER TABLE `tbl_store_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_store_translated`
--

DROP TABLE IF EXISTS `tbl_store_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` longtext DEFAULT NULL,
  `metakeywords` longtext DEFAULT NULL,
  `metadescription` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_store_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_store_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_store` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_store_translated`
--

LOCK TABLES `tbl_store_translated` WRITE;
/*!40000 ALTER TABLE `tbl_store_translated` DISABLE KEYS */;
INSERT INTO `tbl_store_translated` (`id`, `owner_id`, `language`, `name`, `description`, `metakeywords`, `metadescription`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','Default','This is test store set up with the application','','',1,1,'2018-06-29 03:49:17','2018-08-13 01:57:28'),(2,1,'es-EC','Default','Esta es la tienda por defecto de la aplicación','','',1,1,'2018-06-29 03:49:17','2018-08-13 01:57:28');
/*!40000 ALTER TABLE `tbl_store_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tag`
--

DROP TABLE IF EXISTS `tbl_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `frequency` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tag`
--

LOCK TABLES `tbl_tag` WRITE;
/*!40000 ALTER TABLE `tbl_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tag_translated`
--

DROP TABLE IF EXISTS `tbl_tag_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tag_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_tag_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_tag_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tag_translated`
--

LOCK TABLES `tbl_tag_translated` WRITE;
/*!40000 ALTER TABLE `tbl_tag_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tag_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tax_rule`
--

DROP TABLE IF EXISTS `tbl_tax_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tax_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `based_on` varchar(16) NOT NULL,
  `type` varchar(64) NOT NULL,
  `value` varchar(64) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tax_rule`
--

LOCK TABLES `tbl_tax_rule` WRITE;
/*!40000 ALTER TABLE `tbl_tax_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tax_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tax_rule_details`
--

DROP TABLE IF EXISTS `tbl_tax_rule_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tax_rule_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_rule_id` int(11) NOT NULL,
  `product_tax_class_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `tax_zone_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tax_rule_details`
--

LOCK TABLES `tbl_tax_rule_details` WRITE;
/*!40000 ALTER TABLE `tbl_tax_rule_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tax_rule_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tax_rule_translated`
--

DROP TABLE IF EXISTS `tbl_tax_rule_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tax_rule_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_tax_rule_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_tax_rule_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_tax_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tax_rule_translated`
--

LOCK TABLES `tbl_tax_rule_translated` WRITE;
/*!40000 ALTER TABLE `tbl_tax_rule_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tax_rule_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password_reset_token` varchar(128) DEFAULT NULL,
  `password_hash` varchar(128) NOT NULL,
  `auth_key` varchar(128) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `person_id` int(11) DEFAULT NULL,
  `login_ip` varchar(20) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `timezone` varchar(32) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`id`, `username`, `password_reset_token`, `password_hash`, `auth_key`, `status`, `person_id`, `login_ip`, `last_login`, `timezone`, `type`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'superadmin',NULL,'$2y$13$7doXqm4dmHrUfGc4sTmq/u6aE9Myo6T1yTvM50ZVBqGGMtehJVHbS','vU5bx--w-yE8CcYRtpkajPbmZSCY6-X1',1,1,'127.0.0.1','2018-08-13 02:59:32','America/Guayaquil','system',1,1,'2018-06-29 03:47:15','2018-07-29 20:51:01');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_weight_class`
--

DROP TABLE IF EXISTS `tbl_weight_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_weight_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(10) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_weight_class`
--

LOCK TABLES `tbl_weight_class` WRITE;
/*!40000 ALTER TABLE `tbl_weight_class` DISABLE KEYS */;
INSERT INTO `tbl_weight_class` (`id`, `unit`, `value`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,'kg',1.00,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,'g',1000.00,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(3,'oz',35.27,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(4,'lb',2.20,1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15');
/*!40000 ALTER TABLE `tbl_weight_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_weight_class_translated`
--

DROP TABLE IF EXISTS `tbl_weight_class_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_weight_class_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_weight_class_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_weight_class_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_weight_class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_weight_class_translated`
--

LOCK TABLES `tbl_weight_class_translated` WRITE;
/*!40000 ALTER TABLE `tbl_weight_class_translated` DISABLE KEYS */;
INSERT INTO `tbl_weight_class_translated` (`id`, `owner_id`, `language`, `name`, `created_by`, `modified_by`, `created_datetime`, `modified_datetime`) VALUES (1,1,'en-US','Kilogram',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(2,2,'en-US','Gram',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(3,3,'en-US','Ounce',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(4,4,'en-US','Pound',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(5,1,'es-EC','Kilogramo',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(6,2,'es-EC','Gramo',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(7,3,'es-EC','Onza',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15'),(8,4,'es-EC','Libra',1,1,'2018-06-29 03:47:15','2018-06-29 03:47:15');
/*!40000 ALTER TABLE `tbl_weight_class_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_zone`
--

DROP TABLE IF EXISTS `tbl_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `zip` varchar(16) DEFAULT NULL,
  `is_zip_range` smallint(6) DEFAULT NULL,
  `from_zip` varchar(16) DEFAULT NULL,
  `to_zip` varchar(16) DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_zone`
--

LOCK TABLES `tbl_zone` WRITE;
/*!40000 ALTER TABLE `tbl_zone` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_zone_translated`
--

DROP TABLE IF EXISTS `tbl_zone_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_zone_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_zone_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_zone_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_zone` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_zone_translated`
--

LOCK TABLES `tbl_zone_translated` WRITE;
/*!40000 ALTER TABLE `tbl_zone_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_zone_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_cash_on_delivery_transaction`
--

DROP TABLE IF EXISTS `tbl_cash_on_delivery_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cash_on_delivery_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `payment_status` varchar(16) NOT NULL,
  `received_date` date NOT NULL,
  `transaction_id` varchar(32) NOT NULL,
  `transaction_fee` decimal(10,2) NOT NULL DEFAULT 0.00,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_cash_on_delivery_transaction_order_id` (`order_id`),
  CONSTRAINT `fk_tbl_cash_on_delivery_transaction_order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_cash_on_delivery_transaction`
--

LOCK TABLES `tbl_cash_on_delivery_transaction` WRITE;
/*!40000 ALTER TABLE `tbl_cash_on_delivery_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_cash_on_delivery_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_invoice`
--

DROP TABLE IF EXISTS `tbl_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `price_excluding_tax` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL,
  `shipping_fee` decimal(10,2) NOT NULL,
  `total_items` int(11) NOT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_invoice_order_id` (`order_id`),
  CONSTRAINT `fk_tbl_invoice_order_id` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invoice`
--

LOCK TABLES `tbl_invoice` WRITE;
/*!40000 ALTER TABLE `tbl_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_invoice_translated`
--

DROP TABLE IF EXISTS `tbl_invoice_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_invoice_translated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `language` varchar(10) NOT NULL,
  `terms` longtext DEFAULT NULL,
  `created_by` int(11) DEFAULT 0,
  `modified_by` int(11) DEFAULT 0,
  `created_datetime` datetime DEFAULT NULL,
  `modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_invoice_translated_owner_id` (`owner_id`),
  CONSTRAINT `fk_tbl_invoice_translated_owner_id` FOREIGN KEY (`owner_id`) REFERENCES `tbl_invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_invoice_translated`
--

LOCK TABLES `tbl_invoice_translated` WRITE;
/*!40000 ALTER TABLE `tbl_invoice_translated` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_invoice_translated` ENABLE KEYS */;
UNLOCK TABLES;
