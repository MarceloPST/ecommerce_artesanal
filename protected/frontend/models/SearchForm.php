<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace frontend\models;

use usni\UsniAdaptor;
/**
 * SearchForm class file.
 *
 * @package frontend\models
 */
class SearchForm extends \yii\base\Model
{
    /**
     * Store keyword during search.
     * @var string
     */
    public $keyword;
    
    /**
     * Category under which search has to be performed
     * @var mixed 
     */
    public $categoryId;
    
    /**
     * Manufacturer under which search has to be performed
     * @var mixed 
     */
    public $manufacturerId;
    
    /**
     * Tag under which search has to be performed
     * @var mixed 
     */
    public $tag;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                    [['keyword', 'categoryId', 'manufacturerId', 'tag'], 'safe'],
                    [['keyword', 'categoryId', 'manufacturerId', 'tag'], 'filter', 'filter' => 'usni\library\utils\Html::encode']
               ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'keyword'       => UsniAdaptor::t('application', ($language=='en-US')?'Keyword':'Palabra clave'),
                    'categoryId'    => UsniAdaptor::t('productCategories', ($language=='en-US')?'Category':'Categoría'),
                    'manufacturerId'=> UsniAdaptor::t('manufacturer', ($language=='en-US')?'Manufacturer':'Fabricante'),
                    'tag'           => UsniAdaptor::t('products', ($language=='en-US')?'Tag':'Etiqueta'),
               ];
    }
}