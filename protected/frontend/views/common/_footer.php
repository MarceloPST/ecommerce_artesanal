<?php
use usni\UsniAdaptor;
use usni\library\utils\Html;
use common\modules\stores\models\Store;
use newsletter\models\NewsletterCustomers;

/* @var $this \frontend\web\View */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<!-- begin:footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h5><?php echo UsniAdaptor::t('cms', ($language=='en-US')?'Information':'Información'); ?></h5>
                <ul class="list-unstyled">
                    <li>
                        <?php echo Html::a(UsniAdaptor::t('cms', ($language=='en-US')?'About Us':'Conócenos'), UsniAdaptor::createUrl('cms/site/page', ['alias' => UsniAdaptor::t('cms', 'about-us')])); ?>
                    </li>
                    <li>
                        <?php echo Html::a(UsniAdaptor::t('cms', ($language=='en-US')?'Delivery Information':'Información de entrega'), UsniAdaptor::createUrl('cms/site/page', ['alias' => UsniAdaptor::t('cms', 'delivery-info')])); ?>
                    </li>
                    <li>
                        <?php echo Html::a(UsniAdaptor::t('cms', ($language=='en-US')?'Privacy Policy':'Política de privacidad'), UsniAdaptor::createUrl('cms/site/page', ['alias' => UsniAdaptor::t('cms', 'privacy-policy')])); ?>
                    </li>
                    <li>
                        <?php echo Html::a(UsniAdaptor::t('cms', ($language=='en-US')?'Terms & Conditions':'Términos y condiciones'), UsniAdaptor::createUrl('cms/site/page', ['alias' => UsniAdaptor::t('cms', 'terms')])); ?>
                    </li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5> <?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Customer Service':'Servicio al cliente'); ?> </h5>
                <ul class="list-unstyled">
                    <li>
                        <?php echo Html::a(UsniAdaptor::t('cms', ($language=='en-US')?'Contact Us':'Contáctenos'), UsniAdaptor::createUrl('site/default/contact-us')); ?>
                    </li>
                    <li>
                        <?php
                        $label = UsniAdaptor::t('newsletter', ($language=='en-US')?'Join the newsletter':'Suscríbete al boletín');
                        echo Html::a($label, '#',
                                        ['class'       => 'send-newsletter',
                                         'type'        => 'button',
                                         'data-toggle' => 'modal',
                                         'data-target' => '#sendNewsletterModal']);
                        echo $this->render('@newsletter/views/front/sendnewsletter', ['model' => new NewsletterCustomers(['scenario' => 'send'])]);
                        ?>
                    </li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5> <?php echo UsniAdaptor::t('users', ($language=='en-US')?'My Account':'Mi Cuenta'); ?> </h5>
                <ul class="list-unstyled">
                    <li> <?php echo Html::a(UsniAdaptor::t('users', ($language=='en-US')?'My Account':'Mi Cuenta'), UsniAdaptor::createUrl('customer/site/my-account')); ?> </li>
                    <li> <?php echo Html::a(UsniAdaptor::t('order', ($language=='en-US')?'Order History':'Historial de pedidos'), UsniAdaptor::createUrl('customer/site/order-history')); ?> </li>
                    <li> <?php echo Html::a(UsniAdaptor::t('cart', ($language=='en-US')?'Shopping Cart':'Carrito de compras'), UsniAdaptor::createUrl('cart/default/view')); ?> </li>
                    <li> <?php echo Html::a(UsniAdaptor::t('cart', ($language=='en-US')?'Checkout':'Realizar Pedido'), UsniAdaptor::createUrl('cart/checkout/index')); ?> </li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5> <?php echo UsniAdaptor::t('application', ($language=='en-US')?'Social':'Social'); ?> </h5>
                <p>
                    <a href="#"><i class="fa fa-twitter"></i></a> &nbsp;
                    <a href="#"><i class="fa fa-facebook"></i></a> &nbsp;
                    <a href="#"><i class="fa fa-rss"></i></a>
                </p>
            </div>
        </div>
        <hr>
        <?php
            $currentStore   = UsniAdaptor::app()->storeManager->selectedStore;
            if($currentStore['id'] == Store::DEFAULT_STORE_ID)
            {
                $storeName  = UsniAdaptor::t('stores', ($language=='en-US')?'Default Store':'Almacén por defecto');
            }
            else
            {
                $storeName = $currentStore['name'];
            }
            echo '<p>Copyright &copy; ' . date("Y") . " " . $storeName . '. All Rights Reserved.' . UsniAdaptor::app()->powered() . '</p>';
        ?>
    </div>
</footer>
<!-- end:footer -->