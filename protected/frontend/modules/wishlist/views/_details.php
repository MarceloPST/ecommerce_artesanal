<?php
use usni\UsniAdaptor;
?>
<div id="wishlist-full">
<?php

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(!$isEmpty)
{
?>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <td class="text-center"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Image':'Imagen')?></td>
                <td class="text-left"><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre')?></td>
                <td class="text-left"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Model':'Modelo')?></td>
                <td class="text-right"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Unit Price':'Precio unitario')?></td>
                <td class="text-right"><?php echo UsniAdaptor::t('wishlist', ($language=='en-US')?'Action':'Aplicar')?></td>
            </tr>
        </thead>
        <tbody>
            <?php echo $items;?>
        </tbody>
    </table>
<?php
}
else
{
?>
    <table class="table">
        <thead>
            <tr>
                <td class="text-center"><?php echo $items;?></td>
            </tr>
        </thead>
    </table>
<?php
} 
?>
</div>