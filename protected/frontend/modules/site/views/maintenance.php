<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/lgpl.html
 */
use usni\UsniAdaptor;
?>
<?php

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(empty(strip_tags($customMessage)))
{
?>
<h1><?php echo UsniAdaptor::t('service', ($language=='en-US')?'Site is temporarily unavailable.':'El sitio no está disponible temporalmente.'); ?></h1>
<p><?php echo UsniAdaptor::t('service', ($language=='en-US')?'Scheduled maintenance is currently in progress. Please check back soon.':'El mantenimiento programado está actualmente en curso. Por favor, vuelva pronto.');?></p>
<p><?php echo UsniAdaptor::t('service', ($language=='en-US')?'We apologize for any inconvenience.':'Pedimos disculpas por cualquier inconveniente.')?></p>
<?php
}
else
{
?>
<?php echo $customMessage;?>
<?php 
}
