<?php
use usni\UsniAdaptor;
use frontend\widgets\ActiveForm;
use yii\captcha\Captcha;
use frontend\widgets\FormButtons;

/* @var $this \frontend\web\View */

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model          = $formDTO->getModel();
$title          = UsniAdaptor::t('application', ($language=='en-US')?'Contact Us':'Contáctenos');
$this->title    = $title;
?>
<?php
$this->params['breadcrumbs'] = [
                                    ['label' => $title]
                                ];
$form = ActiveForm::begin([
                            'id'          => 'contactformview', 
                            'layout'      => 'horizontal',
                            'caption'     => $title
                         ]);
?>
<?= $form->field($model, 'name')->textInput();?>
<?= $form->field($model, 'email')->textInput();?>
<?= $form->field($model, 'subject')->textInput();?>
<?= $form->field($model, 'message')->textarea();?>
<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), ['captchaAction' => '/site/default/captcha']);?>
<?= FormButtons::widget(['submitButtonLabel' => UsniAdaptor::t('application', ($language=='en-US')?'Submit':'Enviar'), 'showCancelButton' => false]);?>
<?php ActiveForm::end();