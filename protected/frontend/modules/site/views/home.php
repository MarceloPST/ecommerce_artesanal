<?php
use usni\UsniAdaptor;

/* @var $this \frontend\web\View */
/* @var $homePageDTO \frontend\dto\HomePageDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Home':'Inicio');

echo $this->render('/_carousel');
echo "<h3>" . UsniAdaptor::t('products', ($language=='en-US')?'Latest Products':'Últimos Productos') . "</h3>";
echo $this->render('/_latestproductslist', ['products' => $homePageDTO->getLatestProducts()]);

