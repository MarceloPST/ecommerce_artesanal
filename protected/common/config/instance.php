<?php
/**
 * @copyright Copyright (c) 2017 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://github.com/ushainformatique/yiichimp/blob/master/LICENSE.md
 */
$installed = true;
$siteName           = "Ecommerce Artesanal";
//$dsn                = 'mysql:host=142.93.248.176;port=3307;dbname=artesanal;';
$dsn                = 'mysql:host=0.0.0.0;port=3306;dbname=artesanal;';
$username           = 'artesanal';
$password           = 'artesanal_pass';
$debug              = true;
$environment        = 'dev';
$vendorPath         = '.';
$backendVendorPath  = '..';
