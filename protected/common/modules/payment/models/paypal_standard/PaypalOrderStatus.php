<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl.html
 */
namespace common\modules\payment\models\paypal_standard;

use usni\UsniAdaptor;
/**
 * PaypalOrderStatus class file
 *
 * @package common\modules\payment\models\paypal_standard
 */
class PaypalOrderStatus extends \yii\base\Model
{
    /**
     * @var string 
     */
    public $canceled_reversal_status;
    /**
     * @var string 
     */
    public $completed_status;
    /**
     * @var string 
     */
    public $denied_status;
    /**
     * @var string 
     */
    public $expired_status;
    /**
     * @var string 
     */
    public $failed_status;
    /**
     * @var string 
     */
    public $pending_status;
    /**
     * @var string 
     */
    public $processed_status;
    /**
     * @var string 
     */
    public $refunded_status;
    /**
     * @var string 
     */
    public $reversed_status;
    /**
     * @var string 
     */
    public $voided_status;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                    [['canceled_reversal_status', 'completed_status', 'denied_status', 'expired_status', 'failed_status', 'pending_status', 
                      'processed_status', 'refunded_status', 'reversed_status', 'voided_status'], 'safe'],
               ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'canceled_reversal_status'  => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Canceled Reversal Status':'Estado de reverción cancelada'),
                    'completed_status'          => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Completed Status':'Estado de finalización'),
                    'denied_status'             => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Denied Status':'Estado de denegación'),
                    'expired_status'            => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Expired Status':'Estado vencido'),
                    'failed_status'             => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Failed Status':'Estado Fallido'),
                    'pending_status'            => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Pending Status':'Estado Pendiente'),
                    'processed_status'          => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Processed Status':'Estado procesado'),
                    'refunded_status'           => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Refunded Status':'Estado del Reembolso'),
                    'reversed_status'           => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Reversed Status':'Estado anulado'),
                    'voided_status'             => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Voided Status':'Estado anulado'),
               ];
    }
}
