<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl.html
 */
namespace common\modules\payment\models\paypal_standard;

use usni\library\validators\EmailValidator;
use usni\UsniAdaptor;
/**
 * PaypalSetting class file.
 *
 * @package common\modules\payment\models\paypal_standard
 */
class PaypalSetting extends \yii\base\Model
{
    /**
     * @var string 
     */
    public $business_email;
    /**
     * @var string 
     */
    public $return_url;
    /**
     * @var string 
     */
    public $cancel_url;
    /**
     * @var string 
     */
    public $notify_url;
    /**
     * @var boolean 
     */
    public $sandbox;
    
    /**
     * Transaction type for paypal which could be sale or authorization
     * @var type 
     */
    public $transactionType;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['business_email', 'return_url', 'cancel_url', 'notify_url', 'transactionType'], 'required'],
            ['business_email',  EmailValidator::className()],
            [['business_email'], 'string', 'max' => 128],
            [['sandbox'], 'boolean'],
            [['return_url', 'cancel_url'], 'string', 'max' => 256],
            [['business_email', 'sandbox', 'return_url', 'cancel_url', 'notify_url', 'transactionType'], 'safe'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'business_email'    => UsniAdaptor::t('paypal', ($language=='en-US')?'Business Email':'Correo empresarial'),
                    'sandbox'           => UsniAdaptor::t('paypal', ($language=='en-US')?'Sandbox Environment':'Entorno del arenero'),
                    'return_url'        => UsniAdaptor::t('paypal', ($language=='en-US')?'Return Url':'Url de devolución'),
                    'cancel_url'        => UsniAdaptor::t('paypal', ($language=='en-US')?'Cancel Url':'Cancelar Url'),
                    'notify_url'        => UsniAdaptor::t('paypal', ($language=='en-US')?'Notify Url':'Notificar a Url'),
                    'transactionType'   => UsniAdaptor::t('paypal', ($language=='en-US')?'Transaction Type':'Tipo de transacción'),
               ];
    }
    
    /**
     * Gets attribute hints.
     * @return array
     */
    public function attributeHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'business_email'    => UsniAdaptor::t('paypalhint', ($language=='en-US')?'Business email for the paypal account':'Correo electrónico comercial para la cuenta de paypal'),
                    'sandbox'           => UsniAdaptor::t('paypalhint', ($language=='en-US')?'Enable sandbox':'Habilitar Sandbox'),
                    'return_url'        => UsniAdaptor::t('paypalhint', ($language=='en-US')?'Return Url after the order is successfully placed':'Url de devolución después de que el pedido se ha realizado con éxito'),
                    'cancel_url'        => UsniAdaptor::t('paypalhint', ($language=='en-US')?'Cancel Url after the order is cancelled':'Cancelar Url después de la cancelación del pedido'),
                    'notify_url'        => UsniAdaptor::t('paypalhint', ($language=='en-US')?'Notify Url where IPN response messages would be sent':'Notificar a la URL a la que se enviarán los mensajes de respuesta de IPN'),
                    'transactionType'   => UsniAdaptor::t('paypalhint', ($language=='en-US')?'Transaction type for paypal Sale or Authorization':'Clase de operación para la venta o autorización de paypal')
               ];
    }
}
