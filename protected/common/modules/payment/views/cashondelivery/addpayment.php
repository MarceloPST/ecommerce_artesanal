<?php
use usni\UsniAdaptor;

/* @var $this \usni\library\web\AdminView */
/* @var $formDTO \usni\library\dto\FormDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->title = UsniAdaptor::t('payment', ($language=='en-US')?'Cash Details':'Detalles de efectivo');

$file = UsniAdaptor::getAlias('@common/modules/payment/views/baseaddpayment.php');
echo $this->renderPhpFile($file, ['formDTO' => $formDTO]);
