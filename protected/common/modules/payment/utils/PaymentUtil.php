<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\payment\utils;

use usni\UsniAdaptor;
use common\modules\extension\models\Extension;
/**
 * PaymentUtil class file.
 * 
 * @package common\modules\payment\utils
 */
class PaymentUtil
{
    /**
     * Gets status dropdown.
     * @return array
     */
    public static function getStatusDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return array(
            Extension::STATUS_ACTIVE     => UsniAdaptor::t('application',($language=='en-US')?'Active':'Activo'),
            Extension::STATUS_INACTIVE   => UsniAdaptor::t('application',($language=='en-US')?'Inactive':'Inactivo')
        );
    }
    
    /**
     * Get transaction attribute labels
     * @return array
     */
    public static function getTransactionAttributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                        'transaction_id'    => UsniAdaptor::t('payment', ($language=='en-US')?'Transaction ID':'ID de transacción'),
                        'transaction_fee'   => UsniAdaptor::t('payment', ($language=='en-US')?'Transaction Fee':'Cargo por Transacción'),
                        'amount'            => UsniAdaptor::t('payment', ($language=='en-US')?'Amount':'Cantidad'),
                        'payment_status'    => UsniAdaptor::t('payment', ($language=='en-US')?'Payment Status':'Estado del pago'),
                        'order_id'          => UsniAdaptor::t('payment', ($language=='en-US')?'Order':'Pedido'),
                        'totalAmount'      => UsniAdaptor::t('payment', ($language=='en-US')?'Total Amount':'Cantidad Total'),
                        'alreadyPaidAmount'=> UsniAdaptor::t('payment', ($language=='en-US')?'Captured Amount':'Monto Captado'),
                        'pendingAmount'    => UsniAdaptor::t('payment', ($language=='en-US')?'Pending Amount':'Cantidad Pendiente'),
                        'received_date'    => UsniAdaptor::t('payment', ($language=='en-US')?'Received Date':'Fecha de recibo'),
                  ];
    }
}