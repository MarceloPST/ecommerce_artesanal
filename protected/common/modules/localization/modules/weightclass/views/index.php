<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\grid\GridView;
use usni\library\grid\ActionToolbar;
use yii\grid\CheckboxColumn;
use usni\library\grid\ActionColumn;
use common\modules\localization\modules\weightclass\models\WeightClass;

/* @var $gridViewDTO \usni\library\dto\GridViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$title          = UsniAdaptor::t('weightclass', ($language=='en-US')?'Manage Weight Classes':'Administrar clases de peso');
$this->title    = $this->params['breadcrumbs'][] = $title;

$toolbarParams  = [
    'createUrl'     => 'create',
    'bulkDeleteUrl' => 'bulk-delete',
    'showBulkEdit'  => false,
    'showBulkDelete'=> true,
    'gridId'        => 'weightclassgridview',
    'pjaxId'        => 'weightclassgridview-pjax',
    'permissionPrefix'  => 'weightclass'
];
$widgetParams   = [
                        'id'            => 'weightclassgridview',
                        'dataProvider'  => $gridViewDTO->getDataProvider(),
                        'filterModel'   => $gridViewDTO->getSearchModel(),
                        'caption'       => $title,
                        'modelClass'    => WeightClass::className(),
                        'columns' => [
                            ['class' => CheckboxColumn::className()],
                            'name',
                            'unit',
                            'value',
                            [
                                'class' => ActionColumn::className(),
                                'template' => '{view} {update} {delete}',
                                'modelClassName' => WeightClass::className()
                            ]
                        ],
                ];
echo ActionToolbar::widget($toolbarParams);
echo GridView::widget($widgetParams);