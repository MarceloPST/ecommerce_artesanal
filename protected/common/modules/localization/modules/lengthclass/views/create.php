<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

/* @var $this \usni\library\web\AdminView */
/* @var $formDTO \usni\library\dto\FormDTO */

use usni\UsniAdaptor;

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('lengthclass', ($language=='en-US')?'Length Class':'Clase de duración'),
        'url' => ['/localization/lengthclass/default/index']
    ],
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear')
    ]
];
$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('lengthclass', ($language=='en-US')?'Length Classes':'Clases de duración');
echo $this->render("/_form", ['formDTO' => $formDTO]);

