<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

/* @var $this \usni\library\web\AdminView */
/* @var $model \common\modules\localization\modules\state\models\State */

use usni\library\bootstrap\BulkEditActiveForm;
use usni\library\utils\StatusUtil;
use usni\library\bootstrap\BulkEditFormButton;
use usni\UsniAdaptor;
use common\modules\localization\modules\state\models\State;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model  = new State(['scenario' => 'bulkedit']);
$form = BulkEditActiveForm::begin([
            'id'        => 'statebulkeditview',
            'layout'    => 'horizontal',
            'caption' => UsniAdaptor::t('state', ($language=='en-US')?'State':'Estado') . ' ' . UsniAdaptor::t('application', ($language=='en-US')?'Bulk Edit':'Editar Seleccionado')
        ]);
?>
<?= $form->field($model, 'status')->select2Input(StatusUtil::getDropdown(), false);?>
<?= BulkEditFormButton::widget();?>
<?php
BulkEditActiveForm::end();