<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace taxes\models;

use usni\library\db\TranslatableActiveRecord;
use usni\UsniAdaptor;
use taxes\models\ProductTaxClassTranslated;
use products\dao\ProductDAO;
use taxes\dao\TaxRuleDAO;
use yii\db\Exception;
/**
 * ProductTaxClass active record.
 * 
 * @package taxes\models
 */
class ProductTaxClass extends TranslatableActiveRecord 
{
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    ['name',            'required'],
                    ['name',            'unique', 'targetClass' => ProductTaxClassTranslated::className(), 'targetAttribute' => ['name', 'language'], 'on' => 'create'],
                    ['name',            'unique', 'targetClass' => ProductTaxClassTranslated::className(), 'targetAttribute' => ['name', 'language'], 'filter' => ['!=', 'owner_id', $this->id], 'on' => 'update'],
                    ['description',     'safe'],
                    ['name',            'string', 'max' => 64],
                    [['id', 'name'],    'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario               = parent::scenarios();
        $scenario['create']     = $scenario['update'] = ['name', 'description'];
        return $scenario;
    }
    
    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                    'id'					 => UsniAdaptor::t('application', 'Id'),
                    'name'					 => UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre'),
                    'description'            => UsniAdaptor::t('application', ($language=='en-US')?'Description':'Descripción')
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('tax', ($language=='en-US')?'Product Tax Class':'Clase de impuesto del producto') : UsniAdaptor::t('tax', ($language=='en-US')?'Product Tax Classes':'Clases de impuestos sobre los productos');
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['name', 'description'];
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $isAllowedToDelete = $this->checkIfAllowedToDelete();
        if(!$isAllowedToDelete)
        {
            throw new Exception(($language=='en-US')?'This product tax class is associated with tax rule or product.':'Esta clase de impuesto de producto está asociada con la regla de impuestos o producto.');
        }
        return parent::beforeDelete();
    }
    
    /**
     * Check if product tax class is allowed to delete.
     * @return boolean
     */
    public function checkIfAllowedToDelete()
    {
        $taxRules   = TaxRuleDAO::getTaxRuleByAttribute('product_tax_class_id', $this->id, $this->language);
        $products   = ProductDAO::getProductsByAttribute('tax_class_id', $this->id, $this->language);
        if(empty($taxRules) && empty($products))
        {
            return true;
        }
        return false;
    }
}