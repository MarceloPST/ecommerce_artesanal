<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$subItems = [
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('language', ($language=='en-US')? 'Languages':'Idiomas ')),
                    'url'       => ['/language/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.language'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('city', ($language=='en-US')?'Cities':'Ciudades')),
                    'url'       => ['/localization/city/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('country', ($language=='en-US')?'Countries':'Países')),
                    'url'       => ['/localization/country/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('currency', ($language=='en-US')?'Currencies':'Monedas')),
                    'url'       => ['/localization/currency/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('state', ($language=='en-US')?'States':'Estados')),
                    'url'       => ['/localization/state/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('lengthclass', ($language=='en-US')?'Length Classes':'Clases de duración')),
                    'url'       => ['/localization/lengthclass/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('weightclass', ($language=='en-US')?'Weight Classes':'Clases de peso')),
                    'url'       => ['/localization/weightclass/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('stockstatus', ($language=='en-US')?'Stock Status':'Estado del stock')),
                    'url'       => ['/localization/stockstatus/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('orderstatus', ($language=='en-US')?'Order Status':'Estado del pedido')),
                    'url'       => ['/localization/orderstatus/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                ],
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('tax', ($language=='en-US')?'Taxes':'Impuestos')),
                    'url'       => '#',
                    'visible'   => UsniAdaptor::app()->user->can('access.localization'),
                    'items'     => [
                                        [
                                            'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('tax', ($language=='en-US')?'Product Tax Classes':'Clases de impuestos de producto')),
                                            'url'       => ['/localization/tax/product-tax-class/index']
                                        ],
                                        [
                                            'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('tax', ($language=='en-US')?'Tax Rules':'Normas Fiscales')),
                                            'url'       => ['/localization/tax/rule/index']
                                        ],
                                        [
                                            'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('tax', ($language=='en-US')?'Zones':'Zonas')),
                                            'url'       => ['/localization/tax/zone/index']
                                        ]
                                    ]
                ]
            ];
if(UsniAdaptor::app()->user->can('access.localization'))
{
    return [
                [
                    'label'       => MenuUtil::wrapLabel(UsniAdaptor::t('localization', ($language=='en-US')? 'Localization' : 'Localización')),
                    'url'         => '#',
                    'itemOptions' => ['class' => 'navblock-header'],
                    'items' => $subItems
                ]
            ];
}
return [];

