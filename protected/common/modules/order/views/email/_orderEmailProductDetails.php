<?php
use usni\UsniAdaptor;
use products\behaviors\PriceBehavior;

/* @var $this \usni\library\web\AdminView */
$this->attachBehavior('priceBehavior', PriceBehavior::className());
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$tdHeaderStyle = "font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;";
$tdColumnStyle = "font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; font-weight: bold; text-align: left; padding: 7px; color: #222222;";
?>
<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <thead>
        <tr>
            <td style="<?php echo $tdHeaderStyle;?>"><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre') ?></td>
            <td style="<?php echo $tdHeaderStyle;?>"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Model':'Modelo') ?></td>
            <td style="<?php echo $tdHeaderStyle;?>"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Options':'Opciones') ?></td>
            <td style="<?php echo $tdHeaderStyle;?>"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Quantity':'Cantidad') ?></td>
            <td style="<?php echo $tdHeaderStyle;?>"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Unit Price':'Precio unitario') ?></td>
            <td style="<?php echo $tdHeaderStyle;?>"><?php echo UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto') ?></td>
            <td style="<?php echo $tdHeaderStyle;?>"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Total Price':'Precio Total') ?></td>
        </tr>
    </thead>
    <tbody>
        <?php
        echo $items;
        ?>
    </tbody>
</table>
<br/>
<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
    <tbody>
        <tr>
            <td style="<?php echo $tdColumnStyle;?>"><strong><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Sub-Total':'Subtotal'); ?></strong></td>
            <td style="<?php echo $tdColumnStyle;?>"><?php echo $this->getPriceWithSymbol($totalUnitPrice, $currencySymbol); ?></td>
        </tr>
        <tr>
            <td style="<?php echo $tdColumnStyle;?>"><strong><?php echo UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto'); ?></strong></td>
            <td style="<?php echo $tdColumnStyle;?>"><?php echo $this->getPriceWithSymbol($totalTax, $currencySymbol); ?></td>
        </tr>
        <?php
        if ($shippingPrice > 0)
        {
            ?>
            <tr>
                <td style="<?php echo $tdColumnStyle;?>"><strong><?php echo UsniAdaptor::t('shipping', ($language=='en-US')?'Shipping Cost':'Costo de envío'); ?></strong></td>
                <td style="<?php echo $tdColumnStyle;?>"><?php echo $this->getPriceWithSymbol($shippingPrice, $currencySymbol); ?></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td style="<?php echo $tdColumnStyle;?>"><strong><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Total':'Total'); ?></strong></td>
            <td style="<?php echo $tdColumnStyle;?>"><?php echo $this->getPriceWithSymbol($totalPrice, $currencySymbol); ?></td>
        </tr>
    </tbody>
</table>