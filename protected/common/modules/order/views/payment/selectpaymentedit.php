<?php
use usni\UsniAdaptor;
use usni\library\bootstrap\ActiveForm;
use usni\library\bootstrap\FormButtons;
use usni\library\utils\Html;

/* @var $this \usni\library\web\AdminView */
/* @var $form \usni\library\bootstrap\ActiveForm */
/* @var $formDTO \common\modules\order\dto\PaymentFormDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'] = [
                                    [
                                        'label' => UsniAdaptor::t('order', ($language=='en-US')?'Manage Orders':'Administrar pedidos'),
                                        'url'   => UsniAdaptor::createUrl('order/default/index')
                                    ],
                                    [
                                        'label' => UsniAdaptor::t('payment', ($language=='en-US')?'Select Payment Method':'Seleccione la forma de pago')
                                    ]
                                ];
$this->title = UsniAdaptor::t('order', ($language=='en-US')?'Select Payment Method':'Seleccione la forma de pago');
$form = ActiveForm::begin([
        'id' => 'orderselectpaymentmethodview',
        'layout' => 'horizontal',
        'caption' => $this->title,
    ]);

$paymentMethods = $formDTO->getPaymentMethods();
$model          = $formDTO->getModel();
echo $form->field($model, 'payment_type')->dropDownList($paymentMethods);
echo Html::activeHiddenInput($model, 'orderId', ['value' => $formDTO->getOrder()->id]);
echo FormButtons::widget(['cancelUrl' => UsniAdaptor::createUrl('order/default/index'),
                          'submitButtonLabel' => UsniAdaptor::t('application', ($language=='en-US')?'Continue':'Continuar')]);?>
<?php ActiveForm::end();