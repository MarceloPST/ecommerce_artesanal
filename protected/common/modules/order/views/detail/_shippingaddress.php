<?php
use usni\library\widgets\DetailView;
use usni\UsniAdaptor;
use common\modules\order\utils\OrderUtil;

/* @var $detailViewDTO \common\modules\order\dto\DetailViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model          = $detailViewDTO->getModel();
$widgetParams   = [
                    'detailViewDTO' => $detailViewDTO,
                    'decoratorView' => false,
                    'model'         => $model,
                    'attributes'    => [
                                            [
                                                'label'      => UsniAdaptor::t('users', ($language=='en-US')?'Address':'Dirección'),
                                                'value'      => OrderUtil::getConcatenatedAddress($model['shippingAddress']),
                                                'format'     => 'raw'
                                            ]
                                        ]
                    ];
echo DetailView::widget($widgetParams);