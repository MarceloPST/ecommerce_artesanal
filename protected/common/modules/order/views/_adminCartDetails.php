<?php
use usni\UsniAdaptor;
use products\behaviors\PriceBehavior;

/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->attachBehavior('priceBehavior', PriceBehavior::className());

if($isEmpty)
{
    echo $items . "\n";
}
else
{
?>
<div class="table-responsive" id="shopping-cart-admin-full">
    <table class="table table-bordered">
        <thead>
            <tr>
                <td class="text-center"><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Image':'Imagen')?></td>
                <td><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre') ?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Model':'Modelo') ?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Options':'Opciones') ?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Quantity':'Cantidad') ?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Unit Price':'Precio unitario') ?></td>
                <td><?php echo UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto') ?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Total Price':'Precio Total') ?></td>
                <?php
                if (!$isConfirm)
                {
                    ?>
                    <td>&nbsp;</td>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            echo $items;
            ?>
        </tbody>
    </table>
</div>
<br/>
<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="text-right"><strong><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Sub-Total':'SubTotal'); ?></strong></td>
                    <td class="text-right"><?php echo $this->getFormattedPrice($totalUnitPrice, $currencyCode); ?></td>
                </tr>
                <tr>
                    <td class="text-right"><strong><?php echo UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto'); ?></strong></td>
                    <td class="text-right"><?php echo $this->getFormattedPrice($totalTax, $currencyCode); ?></td>
                </tr>
                <?php
                if ($shippingPrice > 0)
                {
                    ?>
                    <tr>
                        <td class="text-right"><strong><?php echo UsniAdaptor::t('shipping', ($language=='en-US')?'Shipping Cost':'Costo de envío'); ?></strong></td>
                        <td class="text-right"><?php echo $this->getFormattedPrice($shippingPrice, $currencyCode); ?></td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td class="text-right"><strong><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Total':'Total'); ?></strong></td>
                    <td class="text-right"><?php echo $this->getFormattedPrice($totalPrice, $currencyCode); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<br/>
<?php
}