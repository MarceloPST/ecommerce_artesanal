<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

use usni\UsniAdaptor;
use usni\library\widgets\DetailActionToolbar;
use usni\fontawesome\FA;
use usni\library\widgets\Tabs;
use usni\library\widgets\DetailBrowseDropdown;
use products\behaviors\PriceBehavior;

/* @var $detailViewDTO \common\modules\order\dto\DetailViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$id = UsniAdaptor::app()->user->getIdentity()->id;
$this->attachBehavior('priceBehavior', PriceBehavior::className());
$model = $detailViewDTO->getModel();
$this->title    = UsniAdaptor::t('application', ($language=='en-US')?'View':'Ver') . ' ' . UsniAdaptor::t('products', ($language=='en-US')?'Order':'Pedido') . '  '. ' #' . $model['id'];
$this->params['breadcrumbs'] = [
    [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('order', ($language=='en-US')?'Orders':'Pedidos'),
        'url' => ['/order/default/index']
    ],
    [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'View':'Ver') . ' #' . $model['id']
    ]
];


$browseParams = ['permission' => 'order.viewother',
    'model' => $model,
    'textAttribute' => 'id',
    'data' => $detailViewDTO->getBrowseModels(),
    'modalDisplay' => $detailViewDTO->getModalDisplay()];

//echo DetailBrowseDropdown::widget($browseParams);
$toolbarParams = ['editUrl' => UsniAdaptor::createUrl('order/default/update', ['id' => $model['id']]),
    'deleteUrl' => UsniAdaptor::createUrl('order/default/delete', ['id' => $model['id']])];
?>
<div class="panel panel-default detail-container">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo FA::icon('book') . UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido') . '  ' . '#' . $model['id']; ?></h6>
        <?php if ($id == 1) {
            echo DetailActionToolbar::widget($toolbarParams);
        }
        ?>
    </div>
    <?php
    $items[] = [
        'options' => ['id' => 'tabgeneral'],
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'General':'General'),
        'class' => 'active',
        'content' => $this->render('/detail/_general', ['detailViewDTO' => $detailViewDTO])
    ];
    if ($model['billingAddress'] !== false) {
        $items[] = [
            'options' => ['id' => 'tabbilling'],
            'label' => UsniAdaptor::t('customer', ($language=='en-US')?'Billing Address':'Dirección de facturación'),
            'content' => $this->render('/detail/_billingaddress', ['detailViewDTO' => $detailViewDTO])
        ];
    }
    if ($model['shippingAddress'] !== false) {
        $items[] = [
            'options' => ['id' => 'tabshipping'],
            'label' => UsniAdaptor::t('customer', ($language=='en-US')?'Shipping Address':'Dirección de envío'),
            'content' => $this->render('/detail/_shippingaddress', ['detailViewDTO' => $detailViewDTO])
        ];
    }
    $items[] = [
        'options' => ['id' => 'tabpayment'],
        'label' => UsniAdaptor::t('order', ($language=='en-US')?'Payment Details':'Detalles del pago'),
        'content' => $this->render('/detail/_paymentdetails', ['detailViewDTO' => $detailViewDTO])
    ];
    $items[] = [
        'options' => ['id' => 'tabproducts'],
        'label' => UsniAdaptor::t('order', ($language=='en-US')?'Product Details':'Detalles del producto'),
        'content' => $this->render('/detail/_productdetails', ['detailViewDTO' => $detailViewDTO])
    ];
    $items[] = [
        'options' => ['id' => 'tabhistory'],
        'label' => UsniAdaptor::t('order', ($language=='en-US')?'Order History':'Historial de pedidos'),
        'content' => $this->render('/detail/_orderhistory', ['detailViewDTO' => $detailViewDTO])
    ];
    echo Tabs::widget(['items' => $items]);
    ?>
</div>