<?php
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<td>
    <address>
        <strong><?php echo UsniAdaptor::t('users', ($language=='en-US')?'Email Address':'Dirección de correo') . ':' ?></strong><br>
        <?php echo $email ?><br>
        <strong><?php echo UsniAdaptor::t('users', ($language=='en-US')?'Phone Number':'Número de teléfono') . ':' ?></strong><br>
        <?php echo $mobilephone ?><br>
    </address>
</td>