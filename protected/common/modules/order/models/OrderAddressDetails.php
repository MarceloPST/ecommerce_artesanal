<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\order\models;

use usni\library\db\ActiveRecord;
use usni\UsniAdaptor;
use common\modules\order\utils\OrderUtil;
/**
 * OrderAddressDetails active record.
 * 
 * @package common\modules\Order\models
 */
class OrderAddressDetails extends ActiveRecord 
{
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['firstname', 'lastname', 'email', 'address1', 'city', 'postal_code', 'country'],                  'required'],
                    [['mobilephone', 'officephone'], 'number',              'integerOnly' => true],
                    [['email', 'firstname', 'lastname', 'mobilephone', 'officephone', 'address1', 'address2', 'city', 'country', 'postal_code', 'state', 
                      'type', 'order_id'],   'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios['update'] = ['email', 'firstname', 'lastname', 'mobilephone', 'officephone', 'address1', 'address2', 'city', 
                                                       'country', 'postal_code', 'state', 'type', 'order_id'];
        return $scenarios;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                    'email'        => UsniAdaptor::t('users', ($language=='en-US')?'Email':'E-mail'),
                    'firstname'    => UsniAdaptor::t('users',($language=='en-US')?'First Name':'Primer nombre'),
                    'lastname'     => UsniAdaptor::t('users',($language=='en-US')?'Last Name':'Apellido'),
                    'mobilephone'  => UsniAdaptor::t('users',($language=='en-US')?'Mobile':'Móvil'),
                    'officephone'  => UsniAdaptor::t('users',($language=='en-US')?'Office Phone':'Teléfono de oficina'),
                    'address1'     => UsniAdaptor::t('users', ($language=='en-US')?'Address1':'Dirección1'),
                    'address2'     => UsniAdaptor::t('users', ($language=='en-US')?'Address2':'Dirección2'),
                    'city'         => UsniAdaptor::t('city', ($language=='en-US')?'City':'Ciudad'),
                    'state'        => UsniAdaptor::t('state', ($language=='en-US')?'State':'Estado'),
                    'country'      => UsniAdaptor::t('country', ($language=='en-US')?'Country':'País'),
                    'postal_code'  => UsniAdaptor::t('users', ($language=='en-US')?'Postal Code':'Código Postal'),
                    'type'         => UsniAdaptor::t('application', ($language=='en-US')?'Type':'Tipo'),
                    'order_id'     => UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido'),
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('order', ($language=='en-US')?'Order Address':'Dirección de pedido');
    }
    
    /**
     * Get concatenated displayed address
     * @return string
     */
    public function getConcatenatedDisplayedAddress()
    {
        return OrderUtil::getConcatenatedAddress($this);
    }
}