<?php
namespace common\modules\order\models;

use usni\UsniAdaptor;
/**
 * AdminSelectPaymentMethodForm active record.
 * @package common\modules\payment\models
 */
class AdminSelectPaymentMethodForm extends \yii\base\Model 
{
    /**
     * Type of payment
     * @var string 
     */
    public $payment_type;
    
    /**
     * Order id
     * @var int 
     */
    public $orderId;
    
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['payment_type', 'orderId'], 'required'],
                    [['payment_type', 'orderId'], 'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios['update'] = ['payment_type', 'orderId'];
        return $scenarios;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                     'payment_type' => UsniAdaptor::t('order', ($language=='en-US')?'Payment Type':'Tipo de pago'),
                     'orderId'      => UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido'),
                  ];
        return $labels;
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('payment', ($language=='en-US')?'Select Payment Method':'Seleccione la forma de pago');
    }
}