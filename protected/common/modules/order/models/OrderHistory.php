<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\order\models;

use usni\library\db\TranslatableActiveRecord;
use usni\UsniAdaptor;
/**
 * OrderHistory class file.
 *
 * @package common\modules\Order\models
 */
class OrderHistory extends TranslatableActiveRecord 
{
    
    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['order_id', 'status'],   'required'],
                    [['order_id', 'status', 'notify_customer', 'comment'],   'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios['update'] = ['order_id', 'status', 'notify_customer', 'comment'];
        return $scenarios;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                     'order_id'         => UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido'),
                     'status'           => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Order Status':'Estado del pedido'),
                     'notify_customer'  => UsniAdaptor::t('customer', ($language=='en-US')?'Notify Customer':'Notificar al cliente'),
                     'comment'          => UsniAdaptor::t('application', ($language=='en-US')?'Comment':'Comentario'),
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}
    
    /**
     * inheritdoc
     */
    public function attributeHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'notify_customer'   => UsniAdaptor::t('orderhint', ($language=='en-US')?'Customer would be notified if status is completed.':'Se notificará al cliente si se ha completado el estado.'),
               ];
    }
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('order', ($language=='en-US')?'Order History':'Historial de pedidos');
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
       return ['comment'];
    }
}