<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\order\models;

use usni\UsniAdaptor;
/**
 * OrderPaymentDetails active record.
 *
 * @package common\modules\Order\models
 */
class OrderPaymentDetails extends \usni\library\db\TranslatableActiveRecord 
{
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['payment_method'],                            'required'],
                    [['order_id', 'payment_method', 'total_including_tax', 'tax', 'payment_type', 'comments', 'shipping_fee'],   'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios['update'] = ['order_id', 'payment_method', 'total_including_tax', 'tax', 'payment_type', 'comments', 'shipping_fee'];
        return $scenarios;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                     'order_id'         => UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido'),
                     'payment_method'   => UsniAdaptor::t('payment', ($language=='en-US')?'Payment Method':'Forma de pago'),
                     'total_including_tax'  => UsniAdaptor::t('order', ($language=='en-US')?'Total Including Tax':'Total Incluyendo Impuestos'),
                     'tax'              => UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto'),
                     'payment_type'     => UsniAdaptor::t('payment', ($language=='en-US')?'Payment Type':'Forma de pago'),
                     'comments'         => UsniAdaptor::t('order', ($language=='en-US')?'Comments':'Comentarios'),
                     'shipping_fee'     => UsniAdaptor::t('shipping', ($language=='en-US')?'Shipping Fee':'Costo de envío'),
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('order', ($language=='en-US')?'Order Payment':'Pago del pedido') : UsniAdaptor::t('order', ($language=='en-US')?'Order Payments':'Pagos de pedidos');
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['comments'];
    }
}