<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\stores\models;

use usni\UsniAdaptor;
use usni\library\db\ActiveRecord;
/**
 * StoreConfiguration active record.
 *
 * @package common\modules\stores\models
 */
class StoreConfiguration extends ActiveRecord
{   
    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [
                        [['store_id', 'code', 'key', 'value', 'category'],  'required'],
                        [['store_id', 'code', 'key', 'value', 'category'],  'safe'],
                    ]
               ];
	}

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['create'] = $scenario['update'] = ['store_id', 'code', 'key', 'value', 'category'];
        return $scenario;
    }
    
    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		return [
                        'id'        => UsniAdaptor::t('application','Id'),
                        'store_id'  => UsniAdaptor::t('stores', ($language=='en-US')?'Store':'Tienda'),
                        'code'      => UsniAdaptor::t('application', ($language=='en-US')?'Code':'Código'),
                        'key'       => UsniAdaptor::t('application', ($language=='en-US')?'Key':'Clave'),
                        'value'     => UsniAdaptor::t('application', ($language=='en-US')?'Value':'Valor'),
                        'category'  => UsniAdaptor::t('stores', ($language=='en-US')?'Category':'Categoría'),
                  ];
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('settings', ($language=='en-US')?'Settings':'Ajustes');
    }
    
    /**
     * @inheritdoc
     */
	public function attributeHints()
	{
		return [];
	}
}