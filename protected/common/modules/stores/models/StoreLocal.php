<?php

/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\stores\models;

use usni\UsniAdaptor;
/**
 * StoreLocal active record.
 * 
 * @package common\modules\stores\models
 */
class StoreLocal extends \yii\base\Model
{
    public $country;
    public $timezone;
    public $state;
    public $currency;
    public $length_class;
    public $weight_class;
    public $language;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                    [['country', 'timezone', 'state', 'currency', 'length_class', 'weight_class', 'language'], 'required'],
                    ['length_class', 'integer'],
                    ['weight_class', 'integer'],
                    [['country', 'timezone', 'state', 'currency', 'length_class', 'weight_class', 'language'], 'safe'],
               ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['create'] = $scenario['update'] = ['country', 'timezone', 'state', 'currency', 'length_class', 'weight_class', 'language'];
        return $scenario;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                        'id'            => UsniAdaptor::t('application', 'Id'),
                        'country'       => UsniAdaptor::t('country', ($language=='en-US')?'Country':'País'),
                        'state'         => UsniAdaptor::t('state', ($language=='en-US')?'State':'Estado'),
                        'timezone'      => UsniAdaptor::t('users', ($language=='en-US')?'Timezone':'Zona horaria'),
                        'weight_class'  => UsniAdaptor::t('weightclass', ($language=='en-US')?'Weight Class':'Clase de peso'),
                        'length_class'  => UsniAdaptor::t('lengthclass', ($language=='en-US')?'Length Class':'Clase de duración'),
                        'currency'      => UsniAdaptor::t('currency', ($language=='en-US')?'Currency':'Moneda'),
                        'language'      => UsniAdaptor::t('application', ($language=='en-US')?'Language':'Lenguaje'),
                  ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $hints = [
                    'country'       => UsniAdaptor::t('storehint', ($language=='en-US')?'Country for the store':'País de la tienda'),
                    'state'         => UsniAdaptor::t('storehint', ($language=='en-US')?'State for the store':'Estado de la tienda'),
                    'timezone'      => UsniAdaptor::t('storehint', ($language=='en-US')?'Timezone in which store operates':'Zona horaria en la que opera la tienda'),
                    'weight_class'  => UsniAdaptor::t('storehint', ($language=='en-US')?'Weight Class for the store':'Clase de peso para la tienda'),
                    'length_class'  => UsniAdaptor::t('storehint', ($language=='en-US')?'Length Class for the store':'Clase de duración para la tienda'),
                    'currency'      => UsniAdaptor::t('storehint', ($language=='en-US')?'Change the default currency. Clear your browser cache to see the change and reset your existing cookie.':'Cambie la moneda por defecto. Limpie la caché de su navegador para ver el cambio y restablecer su cookie existente.'),
                    'language'      => UsniAdaptor::t('storehint', ($language=='en-US')?'Language for the store':'Idioma de la tienda')
                 ];
        return $hints;
    }

    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('stores', ($language=='en-US')?'Local':'Local');
    }
}