<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace products\models;

use usni\library\db\TranslatableActiveRecord;
use usni\UsniAdaptor;
use products\dao\ProductOptionDAO;
/**
 * ProductOptionValue class file.
 *
 * @package products\models
 */
class ProductOptionValue extends TranslatableActiveRecord
{
    /**
     * @inheritdoc
     */
	public function rules()
	{
        return [
                    [['value','option_id'], 'required'],
                    ['value', 'validateValue'],
                    ['value', 'string', 'max' => 128],
                    [['value','option_id'], 'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario               = parent::scenarios();
        $scenario['update']     = $scenario['create']     = ['value', 'option_id'];
        return $scenario;
    }
    
	/**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $message_1 = ($language == 'es-EC' ? 'Valor de Opción' : UsniAdaptor::t('products', 'Option Value'));
        $message_2 = ($language == 'es-EC' ? 'Valores de Opción' : UsniAdaptor::t('products', 'Option Values'));
        return ($n == 1) ? $message_1 : $message_2;
    }
    
    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
		$labels = [
                    'value'      => self::getLabel(1),
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['value'];
    }
    
    /**
     * Get option for the value.
     * @return ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(ProductOption::className(), ['id' => 'option_id']);
    }
    
    /**
     * Validate name in rules
     * @param string $attribute Attribute
     * @param array  $params
     * @return void
     */
    public function validateValue($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $language       = $this->language;
            $record         = ProductOptionDAO::getOptionValueRecord($this->value, $this->option_id, $language);
            if ($record !== false)
            {
                $language_2 = UsniAdaptor::app()->languageManager->selectedLanguage;
                $message = ($language == 'es-EC' ? 'La combinación ' . $this->value . '"-"' . $language . ' de Valor de Opción e Idioma ya ha sido tomada para la opción' :
                    UsniAdaptor::t('products', 'The combination "' . $this->value . '"-"' . $language . '" of Option Value and Language has already been taken for the option.'));
                $this->addError($attribute, $message);
            }
        }
    }
}