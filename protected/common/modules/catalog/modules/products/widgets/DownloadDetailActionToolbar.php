<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace products\widgets;

use usni\fontawesome\FA;
use usni\UsniAdaptor;
/**
 * DownloadDetailActionToolbar class file.
 *
 * @package products\widgets
 */
class DownloadDetailActionToolbar extends \usni\library\widgets\DetailActionToolbar
{
    /**
     * Download url for the model
     * @var string 
     */
    public $downloadUrl;
    
    /**
     * inheritdoc
     */
    public function getListItems()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $items[]    = ['label' => FA::icon('pencil') . "\n" . UsniAdaptor::t('application', ($language=='en-US')?'Edit':'Editar'),
                       'url' => $this->editUrl];
        $items[]    = ['label' => FA::icon('trash-o') . "\n" . UsniAdaptor::t('application', ($language=='en-US')?'Delete':'Eliminar'),
                       'url' => $this->deleteUrl];
        $items[]    = ['label' => FA::icon('pencil') . "\n" . UsniAdaptor::t('products', ($language=='en-US')?'Download':'Descargar'),
                       'url' => $this->downloadUrl];
        return $items;
    }
}
