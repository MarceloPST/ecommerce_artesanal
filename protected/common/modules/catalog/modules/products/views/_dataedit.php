<?php

use products\utils\ProductUtil;
use usni\library\utils\StatusUtil;
use usni\library\utils\AdminUtil;
use dosamigos\datepicker\DatePicker;
use usni\library\utils\Html;
use usni\library\widgets\Thumbnail;

/* @var $formDTO \products\dto\FormDTO */
/* @var $form \usni\library\bootstrap\ActiveForm */

$model = $formDTO->getModel();
?>
<?= $form->field($model, 'status')->dropDownList(StatusUtil::getDropdown()); ?>
<?= Html::activeHiddenInput($model, 'taxclass_id', ['value' => '']); ?>
<?= Thumbnail::widget(['model' => $model,
    'attribute' => 'image',
    'showDeleteLink' => false]); ?>
<?= $form->field($model, 'image')->fileInput(); ?>
<?= Html::activeHiddenInput($model, 'minimum_quantity', ['value' => '']); ?>
<?= $form->field($model, 'stock_status')->dropDownList(ProductUtil::getOutOfStockSelectOptions()); ?>
<?= Html::activeHiddenInput($model, 'subtract_stock', ['value' => 1]); ?>
<?= $form->field($model, 'requires_shipping')->dropDownList(AdminUtil::getYesNoOptions()); ?>
<?= $form->field($model, 'location')->textInput(); ?>
<?= $form->field($model, 'date_available')->widget(DatePicker::className(), [
    'template' => '{addon}{input}',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-m-dd',
    ],
    'options' => ['class' => 'form-control']
]); ?>
<?= $form->field($model, 'length')->textInput(); ?>
<?= $form->field($model, 'width')->textInput(); ?>
<?= $form->field($model, 'height')->textInput(); ?>
<?= $form->field($model, 'weight')->textInput(); ?>
<?= $form->field($model, 'length_class')->dropDownList($formDTO->getLengthClasses()); ?>
<?= $form->field($model, 'weight_class')->dropDownList($formDTO->getWeightClasses()); ?>
<?= Html::activeHiddenInput($model, 'upc', ['value' => '']); ?>
<?= Html::activeHiddenInput($model, 'ean', ['value' => '']); ?>
<?= Html::activeHiddenInput($model, 'jan', ['value' => '']); ?>
<?= Html::activeHiddenInput($model, 'isbn', ['value' => '']); ?>
<?= Html::activeHiddenInput($model, 'mpn', ['value' => '']); ?>
<?= $form->field($model, 'is_featured', ['horizontalCssClasses' => ['wrapper' => 'col-sm-12'],
    'horizontalCheckboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}"])->checkbox(); ?>