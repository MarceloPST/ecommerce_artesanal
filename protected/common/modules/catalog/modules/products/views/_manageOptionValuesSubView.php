<?php
use usni\UsniAdaptor;
use usni\library\utils\Html;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<table id="option-values-list" class="table table-striped table-bordered table-hover">
    <tbody>
        <?php
        if(!empty($assignedOptions))
        {
            foreach($assignedOptions as $assignedOption)
            {
                $isRequired     = $assignedOption['required'] ? UsniAdaptor::t('application', ($language=='en-US')?'Yes':'Si') : UsniAdaptor::t('application', ($language=='en-US')?'No':'No');
                $optionLabel    = Html::tag('strong', UsniAdaptor::t('products', ($language=='en-US')?'Option':'Opción')) .  ': ' . $assignedOption['display_name'] . ' ' .
                                    Html::tag('strong', UsniAdaptor::t('products', ($language=='en-US')?'Required':'Requerido')) . ': ' . $isRequired;
            ?>
            <tr>
                <td colspan="5"><?php echo $optionLabel;?></td>
            </tr>
            <tr>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Option Value':'Valor de la opción');?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Quantity':'Cantidad');?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Subtract Stock':'Restar existencias');?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Price':'Precio');?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Weight':'Peso');?></td>
            </tr>
            
            <?php
                $optionValues   = $assignedOption['optionValues'];
                foreach($optionValues as $index => $optionValueRecord)
                {
            ?>
                    <tr class="option-value-row-<?php echo $optionValueRecord['option_value_id'];?>">
                        <td>
                            <?php echo $optionValueRecord['option_value_name']; ?>
                        </td>
                        <td>
                            <?php echo $optionValueRecord['quantity'];?>
                        </td>
                        <td>
                            <?php echo $optionValueRecord['subtract_stock'] == 1 ? UsniAdaptor::t('application', ($language=='en-US')?'Yes':'Si'): UsniAdaptor::t('application', ($language=='en-US')?'No':'No');?>
                        </td>
                        <td>
                            <?php echo $optionValueRecord['price_prefix'] . $optionValueRecord['price'];?> 
                        </td>
                        <td>
                            <?php echo $optionValueRecord['weight_prefix'] . $optionValueRecord['weight'];?> 
                        </td>
                    </tr>
            <?php
                }
            }
        }
        else
        {
        ?>
            <tr>
                <td colspan='6'> <?php echo UsniAdaptor::t('application', ($language=='en-US')?'No results found':'No se han encontrado resultados');?></td>
            </tr>
        <?php
            }
        ?>
    </tbody>
</table>