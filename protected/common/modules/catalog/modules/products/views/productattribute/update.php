<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;

use usni\library\widgets\BrowseDropdown;

/* @var $formDTO \products\dto\ProductAttributeFormDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model  = $formDTO->getModel();
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('products', ($language=='en-US')?'Attributes':'Atributos'),
        'url' => ['/catalog/products/attribute/index']
    ],
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' #' . $model->id
    ]
];

$browseParams   = ['permission' => 'product.updateother',
                   'data'   => $formDTO->getBrowseModels(),
                   'model'  => $model];
echo BrowseDropdown::widget($browseParams);

$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('products', ($language=='en-US')?'Attributes':'Atributos');
echo $this->render("/productattribute/_form", ['formDTO' => $formDTO]);
