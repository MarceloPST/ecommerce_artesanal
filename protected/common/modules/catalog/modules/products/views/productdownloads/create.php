<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;

/* @var $formDTO \products\dto\FormDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'] = [    
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' . UsniAdaptor::t('products', ($language=='en-US')?'Product Downloads':'Descargas de productos'),
                                        'url'   => ['/catalog/products/download/index']
                                    ],
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear')
                                    ]
                               ];

$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('products', ($language=='en-US')?'Product Download':'Descarga de productos');
echo $this->render('/productdownloads/_form', ['formDTO' => $formDTO]);