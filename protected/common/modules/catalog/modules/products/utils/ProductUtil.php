<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace products\utils;

use usni\UsniAdaptor;
use products\models\Product;
use common\modules\stores\utils\StoreUtil;
use usni\library\utils\FileUploadUtil;
/**
 * ProductUtil class file
 *
 * @package products\utils
 */
class ProductUtil
{
    /**
     * Get product pption type.
     * @return Array
     */
    public static function getProductOptionType()
    {
        return [
                   'select'             => 'Select',
                   'radio'              => 'Radio',
                   'checkbox'           => 'Checkbox'
               ];
    }
    
    /**
     * Get input quantity when user clicks on add to cart.
     * @param Product $product
     * @param integer $inputQty
     * @return int
     */
    public static function getAddToCartInputQuantity($product, $inputQty)
    {
        $minQuantity    = $product->minimum_quantity;
        if($inputQty != null && $inputQty >= $minQuantity)
        {
            $quantity = $inputQty;
        }
        else
        {
            $quantity = $minQuantity;
        }
        return $quantity;
    }
    
    /**
     * Get Stock Select options.
     * @return array
     */
    public static function getOutOfStockSelectOptions()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return array(
            Product::IN_STOCK      => ($language == 'es-EC' ? 'En Stock' : UsniAdaptor::t('products', 'In Stock')),
            Product::OUT_OF_STOCK  => ($language == 'es-EC' ? 'Fuera de Stock' : UsniAdaptor::t('products', 'Out of Stock'))
        );
    }
    
    /**
     * Get product labels
     * @return array
     */
    public static function getProductLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'name'              => ($language == 'es-EC' ? 'Nombre' : UsniAdaptor::t('application', 'Name')),
                    'alias'             => ($language == 'es-EC' ? 'Alias' : UsniAdaptor::t('application', 'Alias')),
                    'model'             => ($language == 'es-EC' ? 'Modelo' : UsniAdaptor::t('products', 'Model')),
                    'price'             => ($language == 'es-EC' ? 'Precio' : UsniAdaptor::t('products', 'Price')),
                    'quantity'          => ($language == 'es-EC' ? 'Cantidad' : UsniAdaptor::t('products', 'Quantity')),
                    'description'       => ($language == 'es-EC' ? 'Descripción' : UsniAdaptor::t('application', 'Description')),
                    'status'            => ($language == 'es-EC' ? 'Estado' : UsniAdaptor::t('application', 'Status')),
                    'metakeywords'      => UsniAdaptor::t('application', 'Meta Keywords'),
                    'metadescription'   => UsniAdaptor::t('application', 'Meta Description'),
                    'tagNames'          => ($language == 'es-EC' ? 'Etiquetas' : UsniAdaptor::t('products', 'Tags')),
                    'minimum_quantity'  => ($language == 'es-EC' ? 'Cantidad Mínima' : UsniAdaptor::t('products', 'Minimum Quantity')),
                    'subtract_stock'    => ($language == 'es-EC' ? 'Disminuir Stock' : UsniAdaptor::t('products', 'Subtract Stock')),
                    'stock_status'      => ($language == 'es-EC' ? 'Estado del Stock' : UsniAdaptor::t('products', 'Stock Status')),
                    'requires_shipping' => ($language == 'es-EC' ? 'Requiere Envío' : UsniAdaptor::t('products', 'Requires Shipping')),
                    'manufacturer'      => ($language == 'es-EC' ? 'Fabricante' : UsniAdaptor::t('manufacturer', 'Manufacturer')),
                    'relatedProducts'   => ($language == 'es-EC' ? 'Productos Relacionados' : UsniAdaptor::t('products', 'Related Products')),
                    'attribute'         => ($language == 'es-EC' ? 'Atributo' : UsniAdaptor::t('products', 'Attribute')),
                    'language_id'       => ($language == 'es-EC' ? 'Idioma' : UsniAdaptor::t('localization', 'Language')),
                    'is_featured'       => ($language == 'es-EC' ? 'Productos Destacados' : UsniAdaptor::t('products', 'Featured Product')),
                    'itemPerPage'       => ($language == 'es-EC' ? 'Ítems por Página' : UsniAdaptor::t('application', 'Items Per Page')),
                    'sort_by'           => ($language == 'es-EC' ? 'Ordenar Por' : UsniAdaptor::t('application', 'Sort By')),
                    'tax_class_id'      => ($language == 'es-EC' ? 'Clase de Impuesto' : UsniAdaptor::t('tax', 'Tax Class')),
                    'sku'               => UsniAdaptor::t('products', 'SKU'),
                    'categories'        => ($language == 'es-EC' ? 'Categorías' : UsniAdaptor::t('productCategories', 'Categories')),
                    'location'          => ($language == 'es-EC' ? 'Ubucación' : UsniAdaptor::t('products', 'Location')),
                    'length'            => ($language == 'es-EC' ? 'Longitud' : UsniAdaptor::t('products', 'Length')),
                    'width'             => ($language == 'es-EC' ? 'Ancho' : UsniAdaptor::t('products', 'Width')),
                    'height'             => ($language == 'es-EC' ? 'Altura' : UsniAdaptor::t('products', 'Height')),
                    'date_available'    => ($language == 'es-EC' ? 'Fecha Disponible' : UsniAdaptor::t('products', 'Date Available')),
                    'weight'            => ($language == 'es-EC' ? 'Peso' : UsniAdaptor::t('products', 'Weight')),
                    'length_class'      => ($language == 'es-EC' ? 'Clase de Longitud' : UsniAdaptor::t('lengthclass', 'Length Class')),
                    'weight_class'      => ($language == 'es-EC' ? 'Clase de Peso' : UsniAdaptor::t('weightclass', 'Weight Class')),
                    'buy_price'         => ($language == 'es-EC' ? 'Precio de Compra' : UsniAdaptor::t('products', 'Buy Price')),
                    'initial_quantity'  => ($language == 'es-EC' ? 'Stock Incial' : UsniAdaptor::t('products', 'Initial Stock')),
                    'hits'              => ($language == 'es-EC' ? 'Impacto' : UsniAdaptor::t('products','Hits')),
                    'upc'               => UsniAdaptor::t('products','UPC'),
                    'ean'               => UsniAdaptor::t('products','EAN'),
                    'jan'               => UsniAdaptor::t('products','JAN'),
                    'isbn'              => UsniAdaptor::t('products','ISBN'),
                    'mpn'               => UsniAdaptor::t('products','MPN'),
                    'download_option'   => ($language == 'es-EC' ? 'Opciones de Descarga' : UsniAdaptor::t('products','Download Option'))
                    
               ];
    }
    
    /**
     * Get product hints
     * @return array
     */
    public static function getProductHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'name'              => ($language == 'es-EC' ? 'Nombre para Producto' : UsniAdaptor::t('productshint', 'Name for Product')),
                    'alias'             => ($language == 'es-EC' ? 'Alias para Producto' : UsniAdaptor::t('productshint', 'Alias for Product')),
                    'model'             => ($language == 'es-EC' ? 'Modelo para Producto' : UsniAdaptor::t('productshint', 'Model for Product')),
                    'price'             => ($language == 'es-EC' ? 'Precio para Producto' : UsniAdaptor::t('productshint', 'Price for Product')),
                    'metakeywords'      => ($language == 'es-EC' ? 'Meta Keywords para Producto' : UsniAdaptor::t('productshint', 'Meta Keywords for Product')),
                    'metadescription'   => ($language == 'es-EC' ? 'Meta Description para Producto' : UsniAdaptor::t('productshint', 'Meta Description for Product')),
                    'tagNames'          => ($language == 'es-EC' ? 'Etiquetas asociadas con el producto p.ej. Prodcutos Útiles' : UsniAdaptor::t('productshint', 'Tags associated with the product. e.g - Useful Products')),
                    'minimum_quantity'  => ($language == 'es-EC' ? 'Cantidad Mínima requerida para añadir un producto al carrito.' : UsniAdaptor::t('productshint', 'Minimum Quantity required to add product to the cart.')),
                    'subtract_stock'    => ($language == 'es-EC' ? 'Disminuir Stock por cantidad de compra, p.ej. Si hay 100 computadoras portátiles y se compran 2, las existencias se reducirán a 98.' : UsniAdaptor::t('productshint', 'Subtract Stock by the purchase quantity for e.g. If there are 100 laptops, and 2 items are purchased, stock would be reduced to 98.')),
                    'stock_status'      => ($language == 'es-EC' ? 'Selecciones "Fuera de Stock", "En Stock" como mensaje mostrado en la página del producto cuando la cantidad del producto llegue a 0.' : UsniAdaptor::t('productshint', 'Select "Out of Stock", "In Stock" as the message shown on the product page when the product quantity reaches 0.')),
                    'is_featured'       => ($language == 'es-EC' ? 'Si desea enfatizar los productos más importantes, Productos Destacados es exáctamente lo que necesita.' : UsniAdaptor::t('productshint', 'If you want to emphasize the most important products, the Featured Products is exactly what you need.')),
                    'sku'               => ($language == 'es-EC' ? 'Código aleatorio para el producto' : UsniAdaptor::t('productshint', 'A random code for the product.')),
                    'categories'        => ($language == 'es-EC' ? 'Categorías para el Producto' : UsniAdaptor::t('productshint', 'Categories for the Product')),
                    'length'            => ($language == 'es-EC' ? 'Longitud del producto' : UsniAdaptor::t('productshint', 'Length for the product')),
                    'width'             => ($language == 'es-EC' ? 'Ancho del producto' : UsniAdaptor::t('productshint', 'Width for the product')),
                    'height'            => ($language == 'es-EC' ? 'Altura del producto' : UsniAdaptor::t('productshint', 'Height for the product')),
                    'initial_quantity'  => ($language == 'es-EC' ? 'Stock Inicial para el producto' : UsniAdaptor::t('productshint', 'Initial stock for the product')),
                    'upc'               => ($language == 'es-EC' ? 'Código Universal del Producto' : UsniAdaptor::t('productshint','Universal Product Code')),
                    'ean'               => ($language == 'es-EC' ? 'Número Europeo del Artículo' : UsniAdaptor::t('productshint','European Article Number')),
                    'jan'               => ($language == 'es-EC' ? 'Número Japones del Producto' : UsniAdaptor::t('productshint','Japanese Article Number')),
                    'isbn'              => ($language == 'es-EC' ? 'Número Estándar Internacional del Libro' : UsniAdaptor::t('productshint','International Standard Book Number')),
                    'mpn'               => ($language == 'es-EC' ? 'Número de Pieza del Fabricante' : UsniAdaptor::t('productshint','Manufacturer Part Number')),
               ];
    }
    
    /**
     * Get product type list
     * @return Array
     */
    public static function getProductTypeList()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                   Product::TYPE_DEFAULT    => UsniAdaptor::t('products', ($language=='en-US')?'Default':'Predeterminado'),
                   Product::TYPE_DOWNLOADABLE => ($language == 'es-EC' ? 'Descargable' : UsniAdaptor::t('products', 'Downloadable'))
               ];
    }
    
    /**
     * Get thumbnail image.
     * @param array $data
     * @return mixed
     */
    public static function getThumbnailImage($data)
    {
        return FileUploadUtil::getThumbnailImage($data, 'image', ['thumbWidth' => 50, 'thumbHeight' => 50]);
    }
}