<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl.html
 */
namespace products\utils;

use usni\UsniAdaptor;
use usni\library\bootstrap\Label;
use usni\library\utils\Html;
use products\models\ProductReview;
/**
 * ReviewUtil implements the static functions related to product reviews
 *
 * @package products\utils
 */
class ReviewUtil
{
    /**
     * Renders label for the status.
     * @param string $data ActiveRecord of the model.
     * @return string
     */
    public static function renderStatus($data)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $value = self::getLabel($data);
        if($value == ($language == 'es-EC' ? 'Pendiente' : UsniAdaptor::t('application', 'Pending')))
        {
            return Label::widget(['content' => $value, 'modifier' => Html::COLOR_WARNING]);
        }
        elseif ($value == ($language == 'es-EC' ? 'Aprobado' : UsniAdaptor::t('products', 'Approve')))
        {
            return Label::widget(['content' => $value, 'modifier' => Html::COLOR_SUCCESS]);
        }
        elseif ($value == UsniAdaptor::t('products', 'Spam'))
        {
            return Label::widget(['content' => $value, 'modifier' => Html::COLOR_DANGER]);
        }
        elseif ($value == ($language == 'es-EC' ? 'Borrar' : UsniAdaptor::t('application', 'Delete')))
        {
            return Label::widget(['content' => $value, 'modifier' => Html::COLOR_DANGER]);
        }
    }
    
    /**
     * Gets label for the status.
     * @param string $data ActiveRecord of the model.
     * @return string
     */
    public static function getLabel($data)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        if ($data['status'] == ProductReview::STATUS_PENDING)
        {
            return ($language == 'es-EC' ? 'Pendiente' : UsniAdaptor::t('application', 'Pending'));
        }
        else if ($data['status'] == ProductReview::STATUS_APPROVED)
        {
            return ($language == 'es-EC' ? 'Aprobar' : UsniAdaptor::t('products', 'Approve'));
        }
        else if ($data['status'] == ProductReview::STATUS_SPAM)
        {
            return UsniAdaptor::t('products', 'Spam');
        }
        else if ($data['status'] == ProductReview::STATUS_DELETED)
        {
            return ($language == 'es-EC' ? 'Borrar' : UsniAdaptor::t('application', 'Delete'));
        }
    }
    
    /**
     * Gets status dropdown.
     * @return array
     */
    public static function getStatusDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    ProductReview::STATUS_APPROVED    => ($language == 'es-EC' ? 'Aprobar' : UsniAdaptor::t('products', 'Approve')),
                    ProductReview::STATUS_PENDING     => ($language == 'es-EC' ? 'Pendiente' : UsniAdaptor::t('application', 'Pending')),
                    ProductReview::STATUS_DELETED     => ($language == 'es-EC' ? 'Borrar' : UsniAdaptor::t('application', 'Delete')),
               ];
    }
}
