<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(UsniAdaptor::app()->user->can('access.catalog'))
{
    return [    
                [
                    'label'     => MenuUtil::wrapLabel(UsniAdaptor::t('productCategories', ($language=='en-US')?'Product Category':'Categoría de productos')),
                    'url'       => ['/catalog/productCategories/default/index'],
                    'visible'   => UsniAdaptor::app()->user->can('productcategory.manage'),
                ],
            ];
}
return [];
