<?php
use usni\library\utils\CountryUtil;
use usni\UsniAdaptor;

/* @var $this \frontend\web\View */
/* @var $form \usni\library\bootstrap\ActiveForm */

$user = UsniAdaptor::app()->user->getIdentity();
$person = $user->person;
?>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'firstname')->textInput(['value' => $person->firstname]);?>
        <?= $form->field($model, 'lastname')->textInput(['value' => $person->lastname]);?>
        <?= $form->field($model, 'email')->textInput(['value' => $person->email]);?>
        <?= $form->field($model, 'mobilephone')->textInput(['value' => $person->mobilephone]);?>
        <?= $form->field($model, 'address1')->textInput(['value' => $user->address->address1]);?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'address2')->textInput(['value' => $user->address->address2]);?>
        <?= $form->field($model, 'country')->select2input(CountryUtil::getCountries());?>
        <?= $form->field($model, 'city')->textInput(['value' => $user->address->city]);?>
        <?= $form->field($model, 'state')->textInput(['value' => $user->address->state]);?>
        <?= $form->field($model, 'postal_code')->textInput();?>
    </div>
</div>

