<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace cart\models;

use yii\base\Model;
use usni\UsniAdaptor;
/**
 * DeliveryOptionsEditForm class file
 * 
 * @package cart\models
 */
class DeliveryOptionsEditForm extends Model
{
    /**
     * Shipping type
     * @var int 
     */
    public $shipping;
    
    /**
     * Comments related to delivery options.
     * @var string 
     */
    public $comments;
    
    /**
     * Calculate price for shipping.
     * @var string 
     */
    public $shipping_fee;
    
    /**
     * Validation rules for the model.
     * @return array Validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            [['shipping', 'comments', 'shipping_fee'],  'safe'],
        );
    }

    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $message = ($language == 'es-EC' ? 'Opciones de Entrega' : UsniAdaptor::t('cart', 'Delivery Options'));
        return $message;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                'shipping'    => ($language == 'es-EC' ? 'Envío' : UsniAdaptor::t('shipping', 'Shipping')),
                'comments'    => ($language == 'es-EC' ? 'Commentarios' : UsniAdaptor::t('application', 'Comments')),
                'shipping_fee'    => ($language == 'es-EC' ? 'Precio de Envío' : UsniAdaptor::t('shipping', 'Shipping Fee')),
               ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                'shipping'    => ($language == 'es-EC' ? 'Método de Envío' : UsniAdaptor::t('shippinghint', 'Shipping Method')),
                'comments'    => ($language == 'es-EC' ? 'Comentarios asociados al envío' : UsniAdaptor::t('shippinghint', 'Comments associated with the shipping')),
                'shipping_fee'    => ($language == 'es-EC' ? 'Precio de Envío' : UsniAdaptor::t('shipping', 'Shipping Fee')),
               ];
    }
}
