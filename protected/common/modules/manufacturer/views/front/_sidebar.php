<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

use usni\UsniAdaptor;

/* @var $this \frontend\web\View */
$manList = $this->params['manList'];
?>

<div class="col-sm-3 hidden-xs" id="column-left">
    <div class="list-group">
        <?php foreach ($manList as $manufacturer) {
            if ($manufacturer['id'] == $_GET['manufacturerId']) {
                $class = "list-group-item active";
            } else {
                $class = "list-group-item";
            }
            $manufacturer_aux = explode('_', $manufacturer['name'], 2);;
            $manufacturer_name = count($manufacturer_aux) == 2 ? $manufacturer_aux[1] : $manufacturer['name'];
            $url = UsniAdaptor::createUrl('manufacturer/site/list', ['manufacturerId' => $manufacturer['id']]);
            ?>
            <a class="<?php echo $class; ?>" href="<?php echo $url; ?>"><?php echo $manufacturer_name; ?></a>
        <?php } ?>
    </div>
</div>