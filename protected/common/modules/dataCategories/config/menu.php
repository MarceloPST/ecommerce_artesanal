<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(UsniAdaptor::app()->user->can('access.dataCategories'))
{
    return [
                [
                    'label'       => MenuUtil::wrapLabel(UsniAdaptor::t('dataCategories', ($language=='en-US')?'Data Categories':'Data Categories')),
                    'url'         => ['/dataCategories/default/index'],
                    'itemOptions' => ['class' => 'navblock-header']
                ]
            ];
}
return [];

