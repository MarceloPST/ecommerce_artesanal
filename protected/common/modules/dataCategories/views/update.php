<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;

use usni\library\widgets\BrowseDropdown;
use common\modules\dataCategories\models\DataCategory;

/* @var $formDTO \usni\library\dto\FormDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model  = $formDTO->getModel();
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('dataCategories', ($language=='en-US')?'Data Categories':'Categorías de datos'),
        'url' => ['/dataCategories/default/index']
    ],
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' #' . $model->id
    ]
];

$browseParams   = ['permission' => 'datacategory.updateother',
                   'data'   => $formDTO->getBrowseModels(),
                   'model'  => $model];
echo BrowseDropdown::widget($browseParams);
$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('dataCategories', ($language=='en-US')?'Data Category':'categorías de datos');
echo $this->render("/_form", ['model' => $model]);
