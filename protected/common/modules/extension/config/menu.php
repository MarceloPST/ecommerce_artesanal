<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(UsniAdaptor::app()->user->can('access.extension'))
{
    return [    
                'label'       => MenuUtil::getSidebarMenuIcon('puzzle-piece') .
                                     MenuUtil::wrapLabel(UsniAdaptor::t('extension', ($language=='en-US')?'Extensions':'Extensiones')),
                'url'         => '#',
                'itemOptions' => ['class' => 'navblock-header'],
                'items'       => [
                                    [
                                        'label'       => UsniAdaptor::t('extension', ($language=='en-US')?'Enhancements':'Mejoras'),
                                        'url'         => ['/enhancement/default/index'],
                                    ],
                                    [
                                        'label'       => UsniAdaptor::t('extension', ($language=='en-US')?'Payment Methods':'Formas de pago'),
                                        'url'         => ['/payment/default/index'],
                                    ],
                                    [
                                        'label'       => UsniAdaptor::t('extension', ($language=='en-US')?'Shipping Methods':'Formas de envío'),
                                        'url'         => ['/shipping/default/index'],
                                    ],
                                    [
                                        'label'       => UsniAdaptor::t('extension', ($language=='en-US')?'Modules':'Módulos'),
                                        'url'         => ['/extension/module/index'],
                                    ],
                                    [
                                        'label'       => UsniAdaptor::t('extension', ($language=='en-US')?'Modifications':'Modificaciones'),
                                        'url'         => ['/extension/modification/index'],
                                    ]
                                  ]
            ];
}
return [];

