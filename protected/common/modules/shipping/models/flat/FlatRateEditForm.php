<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */ 
namespace common\modules\shipping\models\flat;

use yii\base\Model;
use usni\UsniAdaptor;
use common\modules\shipping\utils\flat\FlatShippingUtil;
/**
 * FlatRateEditForm class file
 * @package common\modules\shipping\models\flat
 * @see http://merch.docs.magento.com/ce/user_guide-Jan-29/content/shipping/flat-rate.html
 */
class FlatRateEditForm extends Model
{
    /**
     * Method name for the flat rate displayed
     * @var string 
     */
    public $method_name;
    
    /**
     * Calculate handling fee type= percent or fixed
     * @var string 
     */
    public $calculateHandlingFee;
    
    /**
     * Handling fee
     * @var float 
     */
    public $handlingFee;
    
    /**
     * Price for flat rate shipping
     * @var Price 
     */
    public $price;
    
    /**
     * This would be a dropdown having per item, per order, none means free shipping
     * @var string 
     */
    public $type;
    
    /**
     * Ship to applicable zones(All Zones or Specific Zones)
     * @var int 
     */
    public $applicableZones;
    
    /**
     * Ship to specific zones
     * @var array 
     */
    public $specificZones;

    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('shipping', ($language=='en-US')?'Flat Rate':'Tarifa Fija');
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'method_name', 'type'], 'required'],
            [['price', 'method_name', 'type', 'calculateHandlingFee', 'handlingFee', 'applicableZones', 'specificZones'], 'safe'],
            ['price', 'match', 'pattern'=>'/^[0-9]{1,12}(\.[0-9]{0,4})?$/'],
            ['specificZones', 'required', 
                    'whenClient' => "function(attribute, value){
                        return $('#flatrateeditform-applicablezones').val() != '1';
                     }", 
                    'when' => [$this, 'validateSpecificZones']
                    ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario               = parent::scenarios();
        $scenario['create']     = $scenario['update'] = ['price', 'method_name', 'type', 'calculateHandlingFee', 'handlingFee', 'applicableZones', 'specificZones'];
        $scenario['bulkedit']   = ['status'];
        return $scenario;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
            'price'     => UsniAdaptor::t('products', ($language=='en-US')?'Price':'Precio'),
            'method_name' => UsniAdaptor::t('shipping', ($language=='en-US')?'Method Name':'Nombre del método'),
            'type'      => UsniAdaptor::t('application', ($language=='en-US')?'Type':'Tipo'),
            'calculateHandlingFee' => UsniAdaptor::t('shipping', ($language=='en-US')?'Calculate Handling Fee':'Calcular la tarifa de tramitación'),
            'handlingFee'  => UsniAdaptor::t('shipping', ($language=='en-US')?'Handling Fee':'Tarifa de manejo'),
            'applicableZones' => UsniAdaptor::t('shipping', ($language=='en-US')?'Applicable Zones':'Zonas aplicables'),
            'specificZones'   => UsniAdaptor::t('shipping', ($language=='en-US')?'Specific Zones':'Zonas específicas')
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
            'price'     => UsniAdaptor::t('productshint', ($language=='en-US')?'Price of the shipping':'Precio del envío'),
            'method_name' => UsniAdaptor::t('shippinghint', ($language=='en-US')?'Method name for shipping. It could be Fixed or Plus Handling as suitable':'Nombre del método de envío. Puede ser Fijo o Plus Manipulación según convenga'),
            'type'      => UsniAdaptor::t('shippinghint', ($language=='en-US')?'Type of flat rate i.e per order, per item or none':'Tipo de tarifa plana, es decir, por pedido, por artículo o por ninguno.'),
            'calculateHandlingFee' => UsniAdaptor::t('shippinghint',($language=='en-US')? 'Calculate handling fees type fixed or percent':'Calcular los honorarios de manejo tipo fijo o porcentaje'),
            'handlingFee'  => UsniAdaptor::t('shippinghint', ($language=='en-US')?'Handling Fee, fixed or percent. If percent for example 6% equals .06':'Cuota de gestión, fija o porcentual. Si por ejemplo 6% es igual a.06'),
            'applicableZones' => UsniAdaptor::t('shippinghint', ($language=='en-US')?'Zones to which the shipping would be applied. All zones or specific zones.':'Zonas a las que se aplicaría el envío. Todas las zonas o zonas específicas.'),
            'specificZones'   => UsniAdaptor::t('shippinghint', ($language=='en-US')?'Specific zones to which the shipping is applied':'Zonas específicas a las que se aplica el envío')
        ];
    }
    
    /**
     * Validate the specific zones.
     * @param string $attribute Attribute having user attribute related to login.
     * @param array  $params
     * @return void
     */
    public function validateSpecificZones($attribute, $params)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        if (!$this->hasErrors())
        {
            if($this->applicableZones != FlatShippingUtil::SHIP_TO_ALL_ZONES)
            {
                if($this->specificZones == null)
                {
                    $this->addError('specificZones', UsniAdaptor::t('tax', ($language=='en-US')?'Specific zones is required.':'Se requieren zonas específicas.'));
                }
            }            
        }
    }
}