<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\shipping\utils\flat;

use usni\UsniAdaptor;
/**
 * FlatShippingUtil class file.
 * 
 * @package common\modules\shipping\utils\flat
 */
class FlatShippingUtil
{
    const SHIP_TO_ALL_ZONES = 1;
    const SHIP_TO_SPECIFIC_ZONES = 2;
    
    /**
     * Gets type dropdown.
     * @return array
     */
    public static function getTypeDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return array(
            'none'     => UsniAdaptor::t('application',($language=='en-US')?'None':'Ninguno'),
            'perOrder' => UsniAdaptor::t('shipping',($language=='en-US')?'Per Order':'Por orden'),
            'perItem'  => UsniAdaptor::t('shipping',($language=='en-US')?'Per Item':'Por artículo')
        );
    }
    
    /**
     * Gets method name dropdown.
     * @return array
     */
    public static function getMethodNameDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return array(
            'fixed'             => UsniAdaptor::t('shipping',($language=='en-US')?'Fixed':'Fijo'),
            'fixedPlusHandling' => UsniAdaptor::t('shipping',($language=='en-US')?'Fixed plus handling':'Fijo más manipulación')
        );
    }
    
    /**
     * Gets handling fees type dropdown.
     * @return array
     */
    public static function getHandlingFeesTypeDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return array(
            'fixed'    => UsniAdaptor::t('shipping',($language=='en-US')?'Fixed':'Fijo'),
            'percent'  => UsniAdaptor::t('shipping',($language=='en-US')?'Percent':'Porcentaje')
        );
    }
    
    /**
     * Gets ship to applicable dropdown.
     * @return array
     */
    public static function getShipToApplicableDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return array(
            self::SHIP_TO_ALL_ZONES       => UsniAdaptor::t('shipping', ($language=='en-US')?'All Zones':'Todas las zonas'),
            self::SHIP_TO_SPECIFIC_ZONES  => UsniAdaptor::t('shipping',($language=='en-US')?'Specific Zones':'Zonas específicas')
        );
    }
}