<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(UsniAdaptor::app()->user->can('access.marketing'))
{
    return [    
                'label'       => MenuUtil::getSidebarMenuIcon('share') .
                                     MenuUtil::wrapLabel(UsniAdaptor::t('marketing', ($language=='en-US')?'Marketing':'Mercadeo')),
                'url'         => '#',
                'itemOptions' => ['class' => 'navblock-header'],
                'items'       => [
                                    [
                                        'label'     => UsniAdaptor::t('marketing', ($language=='en-US')?'Mails':'Correos'),
                                        'url'       => ['/marketing/send-mail/create'],
                                        'visible'   => UsniAdaptor::app()->user->can('marketing.mail'),
                                    ],
                                    [
                                        'label'     => UsniAdaptor::t('newsletter', ($language=='en-US')?'Newsletters':'Boletines'),
                                        'url'       => ['/marketing/newsletter/default/index'],
                                        'visible'   => UsniAdaptor::app()->user->can('newsletter.manage'),
                                    ]
                                  ]
            ];
}
return [];

