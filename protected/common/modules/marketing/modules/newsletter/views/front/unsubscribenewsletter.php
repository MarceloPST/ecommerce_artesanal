<?php
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'][] = UsniAdaptor::t('newsletter', ($language=='en-US')?'Unsubscribe Newsletter':'Anular suscripción del boletín');
?>
<h2><?php echo UsniAdaptor::t('newsletter', ($language=='en-US')?'You have been successfully removed from our subscriber list.':'Usted ha sido removido exitosamente de nuestra lista de suscriptores.'); ?></h2>