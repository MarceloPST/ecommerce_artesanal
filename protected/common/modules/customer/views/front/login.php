<?php
use usni\UsniAdaptor;

/* @var $this \frontend\web\View */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<div class="row">
    <div class="col-sm-6">
        <div class="well">
            <h2><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'New Customer':'Nuevo Cliente');?></h2>
            <p><strong><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Register Account':'Registrarse');?></strong></p>
            <p><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Creating an account will make your shopping a nice experience. It would make your order management and tracking much friendly.':'Crear una cuenta hará que sus compras sean una experiencia agradable. Haría que la gestión de sus pedidos y el seguimiento fuera mucho más fácil.')?></p>
            <a href="<?php echo UsniAdaptor::createUrl('/customer/site/register');?>" class="btn btn-success"><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Continue':'Continuar');?></a>
        </div>
    </div>	 
    <div class="col-sm-6">
        <div class="well">
            <?php echo $this->render('/front/_loginform', ['formDTO' => $formDTO]);?>
        </div>
    </div>
</div>
        
