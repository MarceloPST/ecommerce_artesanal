<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->registerJsFile(\yii\helpers\Url::toRoute('/js/jquery-3.3.1.min.js'), [
    'position' => \frontend\web\View::POS_HEAD
]);
$title          = UsniAdaptor::t('users', ($language=='en-US')?'Register':'Registrarse');
$this->title    = $title;
$this->params['breadcrumbs'] = [    
                                    [
                                        'label' => UsniAdaptor::t('customer', ($language=='en-US')?'My Account':'Mi cuenta'),
                                        'url'   => ['/customer/site/my-account']
                                    ],
                                    [
                                        'label' => UsniAdaptor::t('users', ($language=='en-US')?'Register':'Registrarse')
                                    ]
                               ];
echo $this->render('/front/_tabform', ['formDTO' => $formDTO]);