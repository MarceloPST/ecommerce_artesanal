<?php
use usni\UsniAdaptor;
use usni\library\bootstrap\TabbedActiveForm;
use usni\library\bootstrap\FormButtons;
use usni\library\widgets\Tabs;
use usni\library\widgets\TabbedActiveFormAlert;
use usni\library\utils\ArrayUtil;

/* @var $formDTO \customer\dto\FormDTO */
/* @var $form \usni\library\bootstrap\TabbedActiveForm */
/* @var $this \usni\library\web\AdminView */

?>
<?php
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if($formDTO->getScenario() == 'create')
{
    $caption = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('customer', ($language=='en-US')?'Customer':'Cliente');
}
else
{
    $caption = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('customer', ($language=='en-US')?'Customer':'Cliente');
}
$errors = ArrayUtil::merge($formDTO->getModel()->errors, $formDTO->getPerson()->errors, $formDTO->getAddress()->errors);
echo TabbedActiveFormAlert::widget(['model' => $formDTO->getModel(), 'errors' => $errors]);
$form = TabbedActiveForm::begin([
                                    'id'          => 'customereditview', 
                                    'layout'      => 'horizontal',
                                    'options'     => ['enctype' => 'multipart/form-data'],
                                    'caption'     => $caption
                               ]); 
            $items[] = [
                'options' => ['id' => 'tabuser'],
                'label' => UsniAdaptor::t('application', ($language=='en-US')?'General':'General'),
                'class' => 'active',
                'content' => $this->render('/_customeredit', ['form' => $form, 'formDTO' => $formDTO])
            ];
            $items[] = [
                'options' => ['id' => 'tabperson'],
                'label' => UsniAdaptor::t('users', ($language=='en-US')?'Person':'Persona'),
                'content' => $this->render('@usni/library/modules/users/views/_personedit', 
                                            ['form' => $form, 'formDTO' => $formDTO, 'deleteUrl' => UsniAdaptor::createUrl('customer/default/delete-image')])
            ];
            $items[] = [
                'options' => ['id' => 'tabaddress'],
                'label' => UsniAdaptor::t('users', ($language=='en-US')?'Address':'Dirección'),
                'content' => $this->render('@usni/library/modules/users/views/_addressedit', ['formDTO' => $formDTO, 'form' => $form])
            ];
            echo Tabs::widget(['items' => $items]);
    ?>
<?= FormButtons::widget(['cancelUrl' => UsniAdaptor::createUrl('customer/default/index')]);?>
<?php TabbedActiveForm::end();