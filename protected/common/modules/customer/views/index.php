<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use customer\widgets\CustomerNameDataColumn;
use usni\library\utils\TimezoneUtil;
use yii\grid\CheckboxColumn;
use usni\library\grid\GridView;
use usni\library\modules\users\utils\UserUtil;
use usni\library\modules\users\widgets\ActionToolbar;
use usni\library\grid\StatusDataColumn;
use customer\grid\CustomerActionColumn;
use customer\models\Customer;

/* @var $gridViewDTO \usni\library\modules\users\dto\UserGridViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$title          = UsniAdaptor::t('customer', ($language=='en-US')?'Manage Customers':'Administrar clientes');
$this->title    = $this->params['breadcrumbs'][] = $title;

$toolbarParams  = [
                        'createUrl'         => 'create',
                        'bulkEditFormView'  => '/_bulkedit.php',
                        'bulkDeleteUrl'     => 'bulk-delete',
                        'showBulkEdit'      => true,
                        'showBulkDelete'    => false,
                        'gridId'            => 'customergridview',
                        'pjaxId'            => 'customergridview-pjax',
                        'bulkEditFormTitle' => UsniAdaptor::t('customer', ($language=='en-US')?'Customer':'cliente') . ' ' . UsniAdaptor::t('application', ($language=='en-US')?'Bulk Edit':'Editar seleccionado'),
                        'bulkEditActionUrl' => 'bulk-edit',
                        'bulkEditFormId'    => 'customerbulkeditview',
                        'groupList'         => $gridViewDTO->getGroupList(),
                        'permissionPrefix'  => 'customer'
                  ];
$widgetParams  = [
                        'id'            => 'customergridview',
                        'dataProvider'  => $gridViewDTO->getDataProvider(),
                        'filterModel'   => $gridViewDTO->getSearchModel(),
                        'caption'       => $title,
                        'columns' => [
                                        ['class' => CheckboxColumn::className()],   
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'Username':'Usuario'),
                                            'attribute' => 'username',
                                            'class'     => CustomerNameDataColumn::className()
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'Email':'E-mail'),
                                            'attribute' => 'email'
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'First Name':'Nombre'),
                                            'attribute' => 'firstname'
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'Last Name':'Apellido'),
                                            'attribute' => 'lastname',
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'Timezone':'Zona horaria'),
                                            'attribute' => 'timezone',
                                            'filter'    => TimezoneUtil::getTimezoneSelectOptions()
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'Timezone':'Zona horaria'),
                                            'attribute' => 'address1'
                                        ],
                                        [
                                            'label'     => UsniAdaptor::t('users', ($language=='en-US')?'Status':'Estado'),
                                            'attribute' => 'status',
                                            'class'     => StatusDataColumn::className(),
                                            'filter'    => UserUtil::getStatusDropdown()
                                        ],
                                        [
                                            'class'             => CustomerActionColumn::className(),
                                            'template'          => '{view} {update} {delete} {changepassword} {changestatus}',
                                            'modelClassName'    => Customer::className()
                                        ]
                                     ],
                 ];
echo ActionToolbar::widget($toolbarParams);
echo GridView::widget($widgetParams);