<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\cms\utils;

use usni\UsniAdaptor;
use common\modules\cms\Module;
/**
 * DropdownUtil class file
 * 
 * @package common\modules\cms\utils
 */
class DropdownUtil
{
    /**
     * Gets status dropdown for cms.
     * @return array
     */
    public static function getStatusSelectOptions()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    Module::STATUS_PUBLISHED      => ($language == 'es-EC' ? 'Publicado' : UsniAdaptor::t('cms', 'Published')),
                    Module::STATUS_UNPUBLISHED    => ($language == 'es-EC' ? 'Despublicado' : UsniAdaptor::t('cms', 'Unpublished')),
                    Module::STATUS_ARCHIVED       => ($language == 'es-EC' ? 'Archivado' : UsniAdaptor::t('cms', 'Archived')),
                    Module::STATUS_TRASHED        => ($language == 'es-EC' ? 'Eliminado' : UsniAdaptor::t('cms', 'Trashed')),
               ];
    }
}