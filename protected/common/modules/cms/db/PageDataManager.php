<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\cms\db;

use usni\library\db\DataManager;
use common\modules\cms\models\Page;
use common\modules\cms\Module;
use usni\UsniAdaptor;
/**
 * Loads default data related to page.
 *
 * @package common\modules\cms\db
 */
class PageDataManager extends DataManager
{
    /**
     * @inheritdoc
     */
    public static function getModelClassName()
    {
        return Page::className();
    }
    
    /**
     * @inheritdoc
     */
    public function getDefaultDataSet()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    [
                        'name'              => UsniAdaptor::t('cms', ($language=='en-US')?'About Us':'Acerca de nosotros'),
                        'alias'             => UsniAdaptor::t('cms', ($language=='en-US')?'about-us':'acerca de nosotros'),
                        'content'           => $this->getPageContent('_aboutus'),
                        'status'            => Module::STATUS_PUBLISHED,
                        'menuitem'          => UsniAdaptor::t('cms', ($language=='en-US')?'About Us':'Acerca de nosotros')   ,
                        'theme'             => null,
                        'parent_id'         => 0,
                        'summary'           => UsniAdaptor::t('cms', ($language=='en-US')?'About us summary':'Resumen acerca nosotros'),
                        'metakeywords'      => UsniAdaptor::t('cms', ($language=='en-US')?'about us':'acerca de nosotros'),
                        'metadescription'   => UsniAdaptor::t('cms', ($language=='en-US')?'about us description':'Descripción acerca de nosotros'),
                    ],
                    [
                        'name'              => UsniAdaptor::t('cms', ($language=='en-US')?'Delivery Information':'Información de entrega'),
                        'alias'             => UsniAdaptor::t('cms', ($language=='en-US')?'delivery-info':'información de entrega'),
                        'content'           => $this->getPageContent('_delivery'),
                        'status'            => Module::STATUS_PUBLISHED,
                        'menuitem'          => UsniAdaptor::t('cms', ($language=='en-US')?'Delivery Information':'Información de entrega'),
                        'theme'             => null,
                        'parent_id'         => 0,
                        'summary'           => UsniAdaptor::t('cms', ($language=='en-US')?'Delivery information summary':'Resumen de la información de entrega'),
                        'metakeywords'      => UsniAdaptor::t('cms', ($language=='en-US')?'delivery information':'información de entrega'),
                        'metadescription'   => UsniAdaptor::t('cms', ($language=='en-US')?'deliverr information description':'descripción de la información de entrega'),
                    ],
                    [
                        'name'              => UsniAdaptor::t('cms', ($language=='en-US')?'Privacy Policy':'Política de privacidad'),
                        'alias'             => UsniAdaptor::t('cms', ($language=='en-US')?'privacy-policy':'política de privacidad'),
                        'content'           => $this->getPageContent('_privacypolicy'),
                        'status'            => Module::STATUS_PUBLISHED,
                        'menuitem'          => UsniAdaptor::t('cms', ($language=='en-US')?'Privacy Policy':'Política de privacidad'),
                        'theme'             => null,
                        'parent_id'         => 0,
                        'summary'           => UsniAdaptor::t('cms', ($language=='en-US')?'Privacy policy summary':'Resumen de la política de privacidad'),
                        'metakeywords'      => UsniAdaptor::t('cms', ($language=='en-US')?'privacy policy':'política de privacidad'),
                        'metadescription'   => UsniAdaptor::t('cms', ($language=='en-US')?'privacy policy description':'descripción de la política de privacidad'),
                    ],
                    [
                        'name'              => UsniAdaptor::t('cms', ($language=='en-US')?'Terms & Conditions':'Términos y condiciones'),
                        'alias'             => UsniAdaptor::t('cms', ($language=='en-US')?'terms':'términos'),
                        'content'           => $this->getPageContent('_terms'),
                        'status'            => Module::STATUS_PUBLISHED,
                        'menuitem'          => UsniAdaptor::t('cms', ($language=='en-US')?'Terms & Conditions':'Términos y condiciones'),
                        'theme'             => null,
                        'parent_id'         => 0,
                        'summary'           => UsniAdaptor::t('cms', ($language=='en-US')?'Terms & condition summary':'Resumen de términos y condiciones'),
                        'metakeywords'      => UsniAdaptor::t('cms', ($language=='en-US')?'terms & condition':'términos y condiciones'),
                        'metadescription'   => UsniAdaptor::t('cms', ($language=='en-US')?'terms & condition description':'descripción de términos y condiciones'),
                    ]
                ];
    }
    
	/**
     * Get content
     * @param string $template
     * @return string
     */
    public function getPageContent($template)
    {
        $filePath       = UsniAdaptor::app()->getModule('cms')->viewPath . "/templates/$template.php";
        if(file_exists($filePath))
        {
            return file_get_contents($filePath);
        }
        return null;
    }
}