<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;

if(UsniAdaptor::app()->user->can('access.cms'))
{
    $language = UsniAdaptor::app()->languageManager->selectedLanguage;
    return [
                'label'       => MenuUtil::getSidebarMenuIcon('pencil') .
                                     MenuUtil::wrapLabel(($language == 'es-EC' ? 'Contenido' : UsniAdaptor::t('cms', 'Content'))),
                'url'         => '#',
                'itemOptions' => ['class' => 'navblock-header'],
                'items'       => [
                                    [
                                        'label' => ($language == 'es-EC' ? 'Páginas' : UsniAdaptor::t('cms', 'Pages')),
                                        'url'   => ["/cms/page/index"],
                                        'visible'=> UsniAdaptor::app()->user->can('page.manage'),
                                    ]
                                  ]
            ];
}
return [];

