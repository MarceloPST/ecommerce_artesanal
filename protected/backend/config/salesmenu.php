<?php

use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$subItems = [
    [
        'label' => MenuUtil::wrapLabel(($language == 'es-EC' ? 'Clientes' :
            UsniAdaptor::t('customer', 'Customers'))),
        'url' => ['/customer/default/index'],
        'visible' => UsniAdaptor::app()->user->can('access.customer'),
    ],
    [
        'label' => MenuUtil::wrapLabel(($language == 'es-EC' ? 'Grupos de Clientes' :
            UsniAdaptor::t('customer', 'Customer Groups'))),
        'url' => ['/customer/group/index'],
        'visible' => UsniAdaptor::app()->user->can('access.auth'),
    ],
    [
        'label' => MenuUtil::wrapLabel(($language == 'es-EC' ? 'Pedidos' :
            UsniAdaptor::t('order', 'Orders'))),
        'url' => ['/order/default/index'],
        'visible' => UsniAdaptor::app()->user->can('access.order'),
    ]
];
return [
    'label' => MenuUtil::getSidebarMenuIcon('shopping-cart') .
        MenuUtil::wrapLabel(($language == 'es-EC' ? 'Ventas' :
            UsniAdaptor::t('application', 'Sales'))),
    'url' => '#',
    'itemOptions' => ['class' => 'navblock-header'],
    'items' => $subItems
];

