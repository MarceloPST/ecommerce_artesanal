<?php
use usni\UsniAdaptor;

/* @var $this \usni\library\web\AdminView */
/* @var $dashboardDTO \backend\dto\DashboardDTO */
$this->title = $this->params['breadcrumbs'][] = UsniAdaptor::t('application', 'Dashboard');
$id = UsniAdaptor::app()->user->getIdentity()->id;
?>
<div class="row">
    <?php if ($id == 1) { ?>
        <div class="col-sm-6 col-xs-12">
            <?php echo $dashboardDTO->getLatestUsers();?>
        </div>
    <?php } ?>
    <div class="col-sm-6 col-xs-12">
        <?php echo $dashboardDTO->getLatestProducts();?>
    </div>
    <?php if ($id == 1) { ?>
        <div class="col-sm-6 col-xs-12">
            <?php echo $dashboardDTO->getLatestCustomers();?>
        </div>
    <?php } ?>
    <div class="col-sm-6 col-xs-12">
        <?php echo $dashboardDTO->getLatestOrders();?>
    </div>
</div>


