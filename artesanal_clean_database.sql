CREATE TABLE artesanal.tbl_address
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    address1 varchar(128),
    address2 varchar(128),
    city varchar(20),
    state varchar(20),
    country varchar(10),
    postal_code varchar(16),
    relatedmodel varchar(32),
    relatedmodel_id int,
    type smallint,
    status smallint,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_address (id, address1, address2, city, state, country, postal_code, relatedmodel, relatedmodel_id, type, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'PUCE Sede Esmeraldas', '', 'Esmeraldas', 'Esmeraldas', 'EC', '080101', 'Person', 1, 1, 1, 1, 1, '2018-08-13 02:29:33', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_address (id, address1, address2, city, state, country, postal_code, relatedmodel, relatedmodel_id, type, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 'PUCE Sede Esmeraldas', '', 'Esmeraldas', 'Esmeraldas', 'EC', '080101', 'Store', 1, 2, 1, 1, 1, '2018-08-13 02:29:33', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_address (id, address1, address2, city, state, country, postal_code, relatedmodel, relatedmodel_id, type, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 'PUCE Sede Esmeraldas', '', 'Esmeraldas', 'Esmeraldas', 'EC', '080101', 'Store', 1, 3, 1, 1, 1, '2018-08-13 03:18:35', '2018-08-13 03:18:35');
CREATE TABLE artesanal.tbl_auth_assignment
(
    identity_name varchar(32) NOT NULL,
    identity_type varchar(16) NOT NULL,
    permission varchar(64) NOT NULL,
    resource varchar(32) NOT NULL,
    module varchar(32) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_auth_permission
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL,
    alias varchar(64) NOT NULL,
    resource varchar(32) NOT NULL,
    module varchar(32) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'access.auth', 'Access Tab', 'AuthModule', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 'auth.managepermissions', 'Manage Permissions', 'AuthModule', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 'group.create', 'Create Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 'group.view', 'View Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (5, 'group.viewother', 'View Others Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (6, 'group.update', 'Update Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (7, 'group.updateother', 'Update Others Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (8, 'group.delete', 'Delete Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (9, 'group.deleteother', 'Delete Others Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (10, 'group.manage', 'Manage Groups', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (11, 'group.bulk-edit', 'Bulk Edit Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (12, 'group.bulk-delete', 'Bulk Delete Group', 'Group', 'auth', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (13, 'access.home', 'Access Tab', 'HomeModule', 'home', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (14, 'access.language', 'Access Tab', 'LanguageModule', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (15, 'language.create', 'Create Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (16, 'language.view', 'View Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (17, 'language.viewother', 'View Others Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (18, 'language.update', 'Update Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (19, 'language.updateother', 'Update Others Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (20, 'language.delete', 'Delete Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (21, 'language.deleteother', 'Delete Others Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (22, 'language.manage', 'Manage Languages', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (23, 'language.bulk-edit', 'Bulk Edit Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (24, 'language.bulk-delete', 'Bulk Delete Language', 'Language', 'language', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (25, 'access.notification', 'Access Tab', 'NotificationModule', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (26, 'notification.delete', 'Delete Notification', 'Notification', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (27, 'notification.manage', 'Manage Notifications', 'Notification', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (28, 'notificationtemplate.create', 'Create Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (29, 'notificationtemplate.view', 'View Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (30, 'notificationtemplate.viewother', 'View Others Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (31, 'notificationtemplate.update', 'Update Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (32, 'notificationtemplate.updateother', 'Update Others Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (33, 'notificationtemplate.delete', 'Delete Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (34, 'notificationtemplate.deleteother', 'Delete Others Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (35, 'notificationtemplate.manage', 'Manage Templates', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (36, 'notificationtemplate.bulk-edit', 'Bulk Edit Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (37, 'notificationtemplate.bulk-delete', 'Bulk Delete Template', 'NotificationTemplate', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (38, 'notificationlayout.create', 'Create Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (39, 'notificationlayout.view', 'View Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (40, 'notificationlayout.viewother', 'View Others Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (41, 'notificationlayout.update', 'Update Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (42, 'notificationlayout.updateother', 'Update Others Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (43, 'notificationlayout.delete', 'Delete Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (44, 'notificationlayout.deleteother', 'Delete Others Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (45, 'notificationlayout.manage', 'Manage Layouts', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (46, 'notificationlayout.bulk-edit', 'Bulk Edit Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (47, 'notificationlayout.bulk-delete', 'Bulk Delete Layout', 'NotificationLayout', 'notification', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (48, 'access.service', 'Access Tab', 'ServiceModule', 'service', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (49, 'service.checksystem', 'System Configuration', 'ServiceModule', 'service', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (50, 'service.rebuildpermissions', 'Rebuild Permissions', 'ServiceModule', 'service', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (51, 'service.rebuildmodulemetadata', 'Rebuild module metadata', 'ServiceModule', 'service', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (52, 'access.settings', 'Access Tab', 'SettingsModule', 'settings', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (53, 'settings.email', 'Email Settings', 'SettingsModule', 'settings', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (54, 'settings.site', 'Site Settings', 'SettingsModule', 'settings', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (55, 'settings.database', 'Database Settings', 'SettingsModule', 'settings', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (56, 'access.users', 'Access Tab', 'UsersModule', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (57, 'user.create', 'Create User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (58, 'user.view', 'View User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (59, 'user.viewother', 'View Others User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (60, 'user.update', 'Update User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (61, 'user.updateother', 'Update Others User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (62, 'user.delete', 'Delete User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (63, 'user.deleteother', 'Delete Others User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (64, 'user.manage', 'Manage Users', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (65, 'user.bulk-edit', 'Bulk Edit User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (66, 'user.bulk-delete', 'Bulk Delete User', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (67, 'user.change-password', 'Change Password', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (68, 'user.change-status', 'Change Status', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (69, 'user.settings', 'Settings', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (70, 'user.change-passwordother', 'Change Others Password', 'User', 'users', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (71, 'access.catalog', 'Access Tab', 'CatalogModule', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (72, 'productcategory.create', 'Create Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (73, 'productcategory.view', 'View Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (74, 'productcategory.viewother', 'View Others Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (75, 'productcategory.update', 'Update Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (76, 'productcategory.updateother', 'Update Others Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (77, 'productcategory.delete', 'Delete Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (78, 'productcategory.deleteother', 'Delete Others Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (79, 'productcategory.manage', 'Manage Product Categories', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (80, 'productcategory.bulk-edit', 'Bulk Edit Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (81, 'productcategory.bulk-delete', 'Bulk Delete Product Category', 'ProductCategory', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (82, 'product.create', 'Create Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (83, 'product.view', 'View Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (84, 'product.viewother', 'View Others Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (85, 'product.update', 'Update Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (86, 'product.updateother', 'Update Others Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (87, 'product.delete', 'Delete Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (88, 'product.deleteother', 'Delete Others Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (89, 'product.manage', 'Manage Products', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (90, 'product.bulk-edit', 'Bulk Edit Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (91, 'product.bulk-delete', 'Bulk Delete Product', 'Product', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (92, 'productreview.delete', 'Delete Review', 'ProductReview', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (93, 'productreview.manage', 'Manage Reviews', 'ProductReview', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (94, 'productreview.approve', 'Approve', 'ProductReview', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (95, 'productreview.spam', 'Spam', 'ProductReview', 'catalog', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (96, 'access.cms', 'Access Tab', 'CmsModule', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (97, 'page.create', 'Create Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (98, 'page.view', 'View Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (99, 'page.viewother', 'View Others Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (100, 'page.update', 'Update Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (101, 'page.updateother', 'Update Others Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (102, 'page.delete', 'Delete Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (103, 'page.deleteother', 'Delete Others Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (104, 'page.manage', 'Manage Pages', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (105, 'page.bulk-edit', 'Bulk Edit Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (106, 'page.bulk-delete', 'Bulk Delete Page', 'Page', 'cms', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (107, 'access.customer', 'Access Tab', 'CustomerModule', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (108, 'customer.create', 'Create Customer', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (109, 'customer.view', 'View Customer', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (110, 'customer.viewother', 'View Others Customer', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (111, 'customer.update', 'Update Customer', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (112, 'customer.delete', 'Delete Customer', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (113, 'customer.deleteother', 'Delete Others Customer', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (114, 'customer.manage', 'Manage Customers', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (115, 'customer.bulk-edit', 'Bulk Edit Customer', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (116, 'customer.bulk-delete', 'Bulk Delete Customer', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (117, 'customer.change-password', 'Change Password', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (118, 'customer.change-status', 'Change Status', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (119, 'customer.change-passwordother', 'Change Others Password', 'Customer', 'customer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (120, 'access.dataCategories', 'Access Tab', 'DataCategoriesModule', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (121, 'datacategory.create', 'Create Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (122, 'datacategory.view', 'View Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (123, 'datacategory.viewother', 'View Others Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (124, 'datacategory.update', 'Update Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (125, 'datacategory.updateother', 'Update Others Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (126, 'datacategory.delete', 'Delete Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (127, 'datacategory.deleteother', 'Delete Others Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (128, 'datacategory.manage', 'Manage Data Categories', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (129, 'datacategory.bulk-edit', 'Bulk Edit Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (130, 'datacategory.bulk-delete', 'Bulk Delete Data Category', 'DataCategory', 'dataCategories', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (131, 'access.enhancement', 'Access Tab', 'EnhancementModule', 'enhancement', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (132, 'access.extension', 'Access Tab', 'ExtensionModule', 'extension', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (133, 'extension.update', 'Update Extension', 'Extension', 'extension', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (134, 'extension.updateother', 'Update Others Extension', 'Extension', 'extension', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (135, 'extension.delete', 'Delete Extension', 'Extension', 'extension', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (136, 'extension.deleteother', 'Delete Others Extension', 'Extension', 'extension', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (137, 'extension.manage', 'Manage Extensions', 'Extension', 'extension', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (138, 'extension.manageother', 'Manager Others Extension', 'Extension', 'extension', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (139, 'access.localization', 'Access Tab', 'LocalizationModule', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (140, 'city.create', 'Create City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (141, 'city.view', 'View City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (142, 'city.viewother', 'View Others City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (143, 'city.update', 'Update City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (144, 'city.updateother', 'Update Others City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (145, 'city.delete', 'Delete City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (146, 'city.deleteother', 'Delete Others City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (147, 'city.manage', 'Manage Cities', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (148, 'city.bulk-edit', 'Bulk Edit City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (149, 'city.bulk-delete', 'Bulk Delete City', 'City', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (150, 'country.create', 'Create Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (151, 'country.view', 'View Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (152, 'country.viewother', 'View Others Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (153, 'country.update', 'Update Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (154, 'country.updateother', 'Update Others Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (155, 'country.delete', 'Delete Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (156, 'country.deleteother', 'Delete Others Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (157, 'country.manage', 'Manage Countries', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (158, 'country.bulk-edit', 'Bulk Edit Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (159, 'country.bulk-delete', 'Bulk Delete Country', 'Country', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (160, 'currency.create', 'Create Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (161, 'currency.view', 'View Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (162, 'currency.viewother', 'View Others Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (163, 'currency.update', 'Update Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (164, 'currency.updateother', 'Update Others Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (165, 'currency.delete', 'Delete Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (166, 'currency.deleteother', 'Delete Others Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (167, 'currency.manage', 'Manage Currencies', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (168, 'currency.bulk-edit', 'Bulk Edit Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (169, 'currency.bulk-delete', 'Bulk Delete Currency', 'Currency', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (170, 'state.create', 'Create State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (171, 'state.view', 'View State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (172, 'state.viewother', 'View Others State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (173, 'state.update', 'Update State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (174, 'state.updateother', 'Update Others State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (175, 'state.delete', 'Delete State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (176, 'state.deleteother', 'Delete Others State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (177, 'state.manage', 'Manage States', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (178, 'state.bulk-edit', 'Bulk Edit State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (179, 'state.bulk-delete', 'Bulk Delete State', 'State', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (180, 'lengthclass.create', 'Create Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (181, 'lengthclass.view', 'View Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (182, 'lengthclass.viewother', 'View Others Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (183, 'lengthclass.update', 'Update Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (184, 'lengthclass.updateother', 'Update Others Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (185, 'lengthclass.delete', 'Delete Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (186, 'lengthclass.deleteother', 'Delete Others Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (187, 'lengthclass.manage', 'Manage Length Classes', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (188, 'lengthclass.bulk-edit', 'Bulk Edit Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (189, 'lengthclass.bulk-delete', 'Bulk Delete Length Class', 'LengthClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (190, 'weightclass.create', 'Create Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (191, 'weightclass.view', 'View Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (192, 'weightclass.viewother', 'View Others Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (193, 'weightclass.update', 'Update Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (194, 'weightclass.updateother', 'Update Others Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (195, 'weightclass.delete', 'Delete Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (196, 'weightclass.deleteother', 'Delete Others Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (197, 'weightclass.manage', 'Manage Weight Classes', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (198, 'weightclass.bulk-edit', 'Bulk Edit Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (199, 'weightclass.bulk-delete', 'Bulk Delete Weight Class', 'WeightClass', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (200, 'stockstatus.create', 'Create Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (201, 'stockstatus.view', 'View Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (202, 'stockstatus.viewother', 'View Others Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (203, 'stockstatus.update', 'Update Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (204, 'stockstatus.updateother', 'Update Others Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (205, 'stockstatus.delete', 'Delete Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (206, 'stockstatus.deleteother', 'Delete Others Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (207, 'stockstatus.manage', 'Manage Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (208, 'stockstatus.bulk-edit', 'Bulk Edit Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (209, 'stockstatus.bulk-delete', 'Bulk Delete Stock Status', 'StockStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (210, 'orderstatus.create', 'Create Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (211, 'orderstatus.view', 'View Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (212, 'orderstatus.viewother', 'View Others Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (213, 'orderstatus.update', 'Update Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (214, 'orderstatus.updateother', 'Update Others Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (215, 'orderstatus.delete', 'Delete Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (216, 'orderstatus.deleteother', 'Delete Others Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (217, 'orderstatus.manage', 'Manage Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (218, 'orderstatus.bulk-edit', 'Bulk Edit Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (219, 'orderstatus.bulk-delete', 'Bulk Delete Order Status', 'OrderStatus', 'localization', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (220, 'producttaxclass.create', 'Create Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (221, 'producttaxclass.view', 'View Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (222, 'producttaxclass.viewother', 'View Others Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (223, 'producttaxclass.update', 'Update Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (224, 'producttaxclass.updateother', 'Update Others Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (225, 'producttaxclass.delete', 'Delete Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (226, 'producttaxclass.deleteother', 'Delete Others Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (227, 'producttaxclass.manage', 'Manage Product Tax Classes', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (228, 'producttaxclass.bulk-edit', 'Bulk Edit Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (229, 'producttaxclass.bulk-delete', 'Bulk Delete Product Tax Class', 'ProductTaxClass', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (230, 'taxrule.create', 'Create Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (231, 'taxrule.view', 'View Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (232, 'taxrule.viewother', 'View Others Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (233, 'taxrule.update', 'Update Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (234, 'taxrule.updateother', 'Update Others Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (235, 'taxrule.delete', 'Delete Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (236, 'taxrule.deleteother', 'Delete Others Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (237, 'taxrule.manage', 'Manage Tax Rules', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (238, 'taxrule.bulk-edit', 'Bulk Edit Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (239, 'taxrule.bulk-delete', 'Bulk Delete Tax Rule', 'TaxRule', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (240, 'zone.create', 'Create Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (241, 'zone.view', 'View Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (242, 'zone.viewother', 'View Others Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (243, 'zone.update', 'Update Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (244, 'zone.updateother', 'Update Others Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (245, 'zone.delete', 'Delete Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (246, 'zone.deleteother', 'Delete Others Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (247, 'zone.manage', 'Manage Zones', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (248, 'zone.bulk-edit', 'Bulk Edit Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (249, 'zone.bulk-delete', 'Bulk Delete Zone', 'Zone', 'localization/tax', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (250, 'access.manufacturer', 'Access Tab', 'ManufacturerModule', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (251, 'manufacturer.create', 'Create Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (252, 'manufacturer.view', 'View Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (253, 'manufacturer.viewother', 'View Others Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (254, 'manufacturer.update', 'Update Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (255, 'manufacturer.updateother', 'Update Others Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (256, 'manufacturer.delete', 'Delete Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (257, 'manufacturer.deleteother', 'Delete Others Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (258, 'manufacturer.manage', 'Manage Manufacturers', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (259, 'manufacturer.bulk-edit', 'Bulk Edit Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (260, 'manufacturer.bulk-delete', 'Bulk Delete Manufacturer', 'Manufacturer', 'manufacturer', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (261, 'access.marketing', 'Access Tab', 'MarketingModule', 'marketing', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (262, 'marketing.mail', 'Marketing Mails', 'MarketingModule', 'marketing', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (263, 'newsletter.create', 'Create Newsletter', 'Newsletter', 'marketing/newsletter', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (264, 'newsletter.view', 'View Newsletter', 'Newsletter', 'marketing/newsletter', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (265, 'newsletter.viewother', 'View Others Newsletter', 'Newsletter', 'marketing/newsletter', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (266, 'newsletter.update', 'Update Newsletter', 'Newsletter', 'marketing/newsletter', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (267, 'newsletter.updateother', 'Update Others Newsletter', 'Newsletter', 'marketing/newsletter', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (268, 'newsletter.delete', 'Delete Newsletter', 'Newsletter', 'marketing/newsletter', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (269, 'newsletter.deleteother', 'Delete Others Newsletter', 'Newsletter', 'marketing/newsletter', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (270, 'newsletter.manage', 'Manage Newsletters', 'Newsletter', 'marketing/newsletter', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (271, 'access.order', 'Access Tab', 'OrderModule', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (272, 'order.create', 'Create Order', 'Order', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (273, 'order.view', 'View Order', 'Order', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (274, 'order.viewother', 'View Others Order', 'Order', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (275, 'order.update', 'Update Order', 'Order', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (276, 'order.updateother', 'Update Others Order', 'Order', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (277, 'order.delete', 'Delete Order', 'Order', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (278, 'order.deleteother', 'Delete Others Order', 'Order', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (279, 'order.manage', 'Manage Orders', 'Order', 'order', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (280, 'access.payment', 'Access Tab', 'PaymentModule', 'payment', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (281, 'access.shipping', 'Access Tab', 'ShippingModule', 'shipping', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (282, 'access.stores', 'Access Tab', 'StoresModule', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (283, 'store.create', 'Create Store', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (284, 'store.view', 'View Store', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (285, 'store.viewother', 'View Others Store', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (286, 'store.update', 'Update Store', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (287, 'store.updateother', 'Update Others Store', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (288, 'store.delete', 'Delete Store', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (289, 'store.deleteother', 'Delete Others Store', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (290, 'store.manage', 'Manage Stores', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
INSERT INTO artesanal.tbl_auth_permission (id, name, alias, resource, module, created_by, modified_by, created_datetime, modified_datetime) VALUES (291, 'store.bulk-edit', 'Bulk Edit Store', 'Store', 'stores', 1, 0, '2018-08-13 03:37:55', null);
CREATE TABLE artesanal.tbl_order
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    customer_id int,
    shipping varchar(64),
    status smallint,
    store_id int,
    shipping_fee decimal(10,2) DEFAULT 0.00,
    unique_id int NOT NULL,
    currency_code varchar(10) NOT NULL,
    currency_conversion_value double DEFAULT 1 NOT NULL,
    interface varchar(6) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_cash_on_delivery_transaction
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id int NOT NULL,
    payment_status varchar(16) NOT NULL,
    received_date date NOT NULL,
    transaction_id varchar(32) NOT NULL,
    transaction_fee decimal(10,2) DEFAULT 0.00 NOT NULL,
    amount decimal(10,2) DEFAULT 0.00 NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_cash_on_delivery_transaction_order_id FOREIGN KEY (order_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_cash_on_delivery_transaction_order_id ON artesanal.tbl_cash_on_delivery_transaction (order_id);
CREATE TABLE artesanal.tbl_city
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    country_id int NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_city_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_city_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_city (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_city_translated_owner_id ON artesanal.tbl_city_translated (owner_id);
CREATE TABLE artesanal.tbl_configuration
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    module varchar(32) NOT NULL,
    `key` varchar(32) NOT NULL,
    value longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'application', 'dbAdminUsername', '', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 'application', 'dbAdminPassword', '', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 'application', 'siteName', 'Ecommerce Artesanal', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 'application', 'siteDescription', 'Ecommerce Artesanal', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (5, 'application', 'superUsername', 'superadmin', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (6, 'application', 'superEmail', 'm.sancheztarira@gmail.com', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (7, 'application', 'superPassword', 'Soloyoentro2018', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (8, 'application', 'dbHost', 'localhost', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (9, 'application', 'dbPort', '3306', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (10, 'application', 'dbName', 'artesanal', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (11, 'application', 'dbUsername', 'artesanal', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (12, 'application', 'dbPassword', 'artesanal_pass', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (13, 'application', 'environment', 'dev', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (14, 'application', 'demoData', '1', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (15, 'application', 'timezone', 'America/Guayaquil', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (16, 'application', 'logo', '', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (17, 'application', 'uploadInstance', null, 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (18, 'application', 'enableSchemaCache', '1', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (19, 'application', 'schemaCachingDuration', '3600', 1, 1, '2018-06-29 03:47:14', '2018-06-29 03:47:14');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (20, 'application', 'appRebuild', '', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (21, 'users', 'passwordTokenExpiry', '3600', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (22, 'application', 'installTime', '2018-06-29 03:49:20', 1, 1, '2018-06-29 03:49:20', '2018-06-29 03:49:20');
INSERT INTO artesanal.tbl_configuration (id, module, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (23, 'settings', 'emailSettings', 'a:10:{s:8:"fromName";s:13:"Administrador";s:11:"fromAddress";s:22:"pucese.comex@gmail.com";s:14:"replyToAddress";s:17:"noreply@email.com";s:13:"sendingMethod";s:4:"smtp";s:8:"smtpHost";s:14:"smtp.gmail.com";s:8:"smtpPort";s:3:"587";s:12:"smtpUsername";s:22:"pucese.comex@gmail.com";s:12:"smtpPassword";s:20:"ComercioExterior2018";s:8:"smtpAuth";i:0;s:8:"testMode";s:1:"0";}', 1, 1, '2018-08-05 11:59:04', '2018-08-05 12:02:42');
CREATE TABLE artesanal.tbl_country
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    postcode_required smallint,
    status smallint,
    iso_code_2 varchar(2),
    iso_code_3 varchar(3),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_country_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(64) NOT NULL,
    address_format longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_country_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_country (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_country_translated_owner_id ON artesanal.tbl_country_translated (owner_id);
CREATE TABLE artesanal.tbl_currency
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    status smallint,
    value decimal(10,2) NOT NULL,
    code varchar(10) NOT NULL,
    symbol_left varchar(10),
    symbol_right varchar(10),
    decimal_place varchar(3),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_currency (id, status, value, code, symbol_left, symbol_right, decimal_place, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 1.00, 'USD', '$', '', '2', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
CREATE TABLE artesanal.tbl_currency_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(64),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_currency_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_currency (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_currency_translated_owner_id ON artesanal.tbl_currency_translated (owner_id);
INSERT INTO artesanal.tbl_currency_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'US Dollars', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
CREATE TABLE artesanal.tbl_customer
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username varchar(64) NOT NULL,
    unique_id int NOT NULL,
    password_reset_token varchar(128),
    password_hash varchar(128),
    auth_key varchar(128),
    status smallint,
    person_id int,
    login_ip varchar(20),
    last_login datetime,
    timezone varchar(32),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_customer_activity
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    customer_id int NOT NULL,
    `key` varchar(128) NOT NULL,
    data longtext NOT NULL,
    ip varchar(164) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_customer_download_mapping
(
    customer_id int,
    download_id int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_customer_metadata
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    customer_id int,
    cart longtext,
    wishlist longtext,
    compareproducts longtext,
    currency varchar(128),
    language varchar(128),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_customer_online
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    ip varchar(64),
    customer_id int,
    url varchar(164),
    referer varchar(164),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_data_category
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    status smallint NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_data_category (id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
CREATE TABLE artesanal.tbl_data_category_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    description longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_data_category_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_data_category (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_data_category_translated_owner_id ON artesanal.tbl_data_category_translated (owner_id);
INSERT INTO artesanal.tbl_data_category_translated (id, owner_id, language, name, description, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'Root Category', 'This is root data category for the application under which all the data would reside', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
CREATE TABLE artesanal.tbl_extension
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    category varchar(16) NOT NULL,
    author varchar(128),
    version varchar(10),
    product_version longtext,
    status smallint NOT NULL,
    code varchar(32),
    data longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_extension (id, category, author, version, product_version, status, code, data, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'payment', 'WhatACart', '1.0', '2.0.0', 1, 'cashondelivery', null, 1, 1, '2018-06-29 03:49:17', '2018-06-29 15:57:57');
INSERT INTO artesanal.tbl_extension (id, category, author, version, product_version, status, code, data, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 'payment', 'WhatACart', '1.0', '2.0.0', 1, 'paypal_standard', null, 1, 1, '2018-06-29 03:49:17', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_extension (id, category, author, version, product_version, status, code, data, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 'shipping', 'WhatACart', '1.0', '2.0.0', 1, 'flat', null, 1, 1, '2018-06-29 03:49:17', '2018-06-29 15:58:03');
INSERT INTO artesanal.tbl_extension (id, category, author, version, product_version, status, code, data, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 'shipping', 'WhatACart', '1.0', '2.0.0', 1, 'free', null, 1, 1, '2018-06-29 03:49:17', '2018-06-29 15:58:04');
CREATE TABLE artesanal.tbl_extension_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(32),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_extension_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_extension (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_extension_translated_owner_id ON artesanal.tbl_extension_translated (owner_id);
INSERT INTO artesanal.tbl_extension_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'Cash On Delivery', 1, 1, '2018-06-29 03:49:17', '2018-06-29 03:49:17');
INSERT INTO artesanal.tbl_extension_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 2, 'en-US', 'Paypal Standard', 1, 1, '2018-06-29 03:49:17', '2018-06-29 03:49:17');
INSERT INTO artesanal.tbl_extension_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 3, 'en-US', 'Flat Rate', 1, 1, '2018-06-29 03:49:17', '2018-06-29 03:49:17');
INSERT INTO artesanal.tbl_extension_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 4, 'en-US', 'Free Shipping', 1, 1, '2018-06-29 03:49:17', '2018-06-29 03:49:17');
CREATE TABLE artesanal.tbl_group
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL,
    parent_id int NOT NULL,
    level int NOT NULL,
    status int NOT NULL,
    category varchar(16) DEFAULT 'system' NOT NULL,
    path longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_group (id, name, parent_id, level, status, category, path, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'General', 0, 0, 1, 'customer', '7', 1, 1, '2018-08-13 02:24:25', '2018-08-13 02:24:25');
CREATE TABLE artesanal.tbl_group_member
(
    group_id int NOT NULL,
    member_id int NOT NULL,
    member_type varchar(16) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_invoice
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    unique_id int NOT NULL,
    order_id int NOT NULL,
    price_excluding_tax decimal(10,2) NOT NULL,
    tax decimal(10,2) NOT NULL,
    shipping_fee decimal(10,2) NOT NULL,
    total_items int NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_invoice_order_id FOREIGN KEY (order_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_invoice_order_id ON artesanal.tbl_invoice (order_id);
CREATE TABLE artesanal.tbl_invoice_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    terms longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_invoice_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_invoice (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_invoice_translated_owner_id ON artesanal.tbl_invoice_translated (owner_id);
CREATE TABLE artesanal.tbl_language
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL,
    locale varchar(10) NOT NULL,
    image varchar(64),
    sort_order int,
    status smallint NOT NULL,
    code varchar(10) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_language (id, name, locale, image, sort_order, status, code, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'English', 'en-US', '', 1, 1, 'en-US', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_language (id, name, locale, image, sort_order, status, code, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 'Español', 'es-EC', '', 1, 1, 'es-EC', 1, 1, '2018-07-30 13:36:08', '2018-08-13 02:08:56');
CREATE TABLE artesanal.tbl_length_class
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    unit varchar(10) NOT NULL,
    value decimal(10,2) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_length_class (id, unit, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'm', 1.00, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_length_class (id, unit, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 'cm', 100.00, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_length_class (id, unit, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 'in', 39.37, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_length_class (id, unit, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 'mm', 1000.00, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
CREATE TABLE artesanal.tbl_length_class_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_length_class_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_length_class (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_length_class_translated_owner_id ON artesanal.tbl_length_class_translated (owner_id);
INSERT INTO artesanal.tbl_length_class_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'Meter', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_length_class_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 2, 'en-US', 'Centimeter', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_length_class_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 3, 'en-US', 'Inch', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_length_class_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 4, 'en-US', 'Millimeter', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
CREATE TABLE artesanal.tbl_manufacturer
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(64) NOT NULL,
    image varchar(64),
    status smallint,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_newsletter
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    store_id int NOT NULL,
    `to` int NOT NULL,
    subject varchar(164) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_newsletter_customers
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    customer_id int NOT NULL,
    email varchar(164) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_newsletter_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    content longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_newsletter_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_newsletter (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_newsletter_translated_owner_id ON artesanal.tbl_newsletter_translated (owner_id);
CREATE TABLE artesanal.tbl_notification
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    modulename varchar(16) NOT NULL,
    type varchar(16) NOT NULL,
    data longblob NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    priority smallint DEFAULT 1 NOT NULL,
    senddatetime datetime,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_notification_layout
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    status smallint DEFAULT 1 NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_notification_layout_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(64) NOT NULL,
    content longblob NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_notification_layout_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_notification_layout (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_notification_layout_translated_owner_id ON artesanal.tbl_notification_layout_translated (owner_id);
CREATE TABLE artesanal.tbl_notification_template
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    type varchar(10) NOT NULL,
    notifykey varchar(32) NOT NULL,
    layout_id int,
    status smallint DEFAULT 1 NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'email', 'createUser', null, 1, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 'email', 'changepassword', null, 1, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 'email', 'forgotpassword', null, 1, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 'email', 'productReview', null, 1, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (5, 'email', 'orderCompletion', null, 1, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (6, 'email', 'orderReceived', null, 1, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (7, 'email', 'orderUpdate', null, 1, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (8, 'email', 'sendMail', null, 1, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (9, 'email', 'sendNewsletter', null, 1, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template (id, type, notifykey, layout_id, status, created_by, modified_by, created_datetime, modified_datetime) VALUES (10, 'email', 'createCustomer', null, 1, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
CREATE TABLE artesanal.tbl_notification_template_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    subject varchar(128) NOT NULL,
    content longblob NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_notification_template_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_notification_template (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_notification_template_translated_owner_id ON artesanal.tbl_notification_template_translated (owner_id);
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'New User Registration', 0x3C703E57656C636F6D65207B7B66756C6C4E616D657D7D2E20596F7572206163636F756E7420686173206265656E2063726561746564207375636365737366756C6C79206174207B7B6170706E616D657D7D3C2F703E0A0A3C703E596F7572206C6F67696E2064657461696C73206172652061732062656C6F773C6272202F3E3C62722F3E0A202020203C7374726F6E673E557365726E616D653A3C2F7374726F6E673E207B7B757365726E616D657D7D3C6272202F3E0A202020203C7374726F6E673E50617373776F72643C2F7374726F6E673E3A207B7B70617373776F72647D7D3C2F703E0A0A7B7B636F6E6669726D656D61696C4C6162656C7D7D0A7B7B636F6E6669726D656D61696C7D7D0A0A5468616E6B732C3C6272202F3E0A53797374656D2041646D696E, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 2, 'en-US', 'You have changed your password', 0x3C703E44656172207B7B66756C6C4E616D657D7D2C203C62722F3E3C62722F3E596F75722070617373776F726420686173206265656E206368616E67656420746F207B7B70617373776F72647D7D2E0A3C62722F3E3C62722F3E0A5468616E6B733C62722F3E0A53797374656D2041646D696E3C2F703E, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 3, 'en-US', 'Forgot Password Request', 0x3C703E44656172207B7B66756C6C4E616D657D7D2C3C62722F3E0A596F7572206C6F67696E2064657461696C73206172652061732062656C6F773C62723E0A3C7374726F6E673E557365726E616D653A3C2F7374726F6E673E207B7B757365726E616D657D7D3C62723E0A3C7374726F6E673E50617373776F72643C2F7374726F6E673E3A207B7B70617373776F72647D7D0A3C62722F3E3C62722F3E0A5468616E6B733C62723E0A53797374656D2041646D696E0A3C2F703E, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 4, 'en-US', 'Product Review | {{productName}}', 0x3C703E0A2020202048656C6C6F2C3C62722F3E0A202020207B7B637573746F6D65726E616D657D7D2068617320706F737465642061206E657720726576696577206F6E207B7B70726F647563746E616D657D7D2E0A3C2F703E0A3C703E0A20202020546865207265766965772069733A3C62722F3E0A202020207B7B7265766965777D7D3C62722F3E3C62722F3E0A202020205468616E6B732C3C6272202F3E0A2020202053797374656D2041646D696E0A3C2F703E, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (5, 5, 'en-US', 'Order Completion', 0x3C703E446561722C207B7B637573746F6D65726E616D657D7D3C2F703E0A3C703E0A20202020596F7572206F7264657220237B7B6F726465726E756D6265727D7D2070726F63657373696E6720697320636F6D706C65746564206F6E207B7B6F72646572646174657D7D2E0A3C2F703E0A7B7B6F726465724C696E6B7D7D0A3C703E0A202020205468616E6B20596F752C203C62722F3E200A2020202053797374656D2041646D696E0A3C2F703E, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (6, 6, 'en-US', 'Received Order', 0x3C646976207374796C653D2277696474683A2036383070783B223E0A20203C70207374796C653D226D617267696E2D746F703A203070783B206D617267696E2D626F74746F6D3A20323070783B223E5468616E6B20796F7520666F7220796F757220696E74657265737420696E207B7B73746F72654E616D657D7D2070726F64756374732E20596F7572206F7264657220686173206265656E20726563656976656420616E642077696C6C2062652070726F636573736564206F6E6365207061796D656E7420686173206265656E20636F6E6669726D65642E3C2F703E0A20207B7B6F726465724C696E6B7D7D0A20203C7461626C65207374796C653D22626F726465722D636F6C6C617073653A20636F6C6C617073653B2077696474683A20313030253B20626F726465722D746F703A2031707820736F6C696420234444444444443B20626F726465722D6C6566743A2031707820736F6C696420234444444444443B206D617267696E2D626F74746F6D3A20323070783B223E0A202020203C74686561643E0A2020202020203C74723E0A20202020202020203C7464207374796C653D22666F6E742D73697A653A20313270783B20626F726465722D72696768743A2031707820736F6C696420234444444444443B20626F726465722D626F74746F6D3A2031707820736F6C696420234444444444443B206261636B67726F756E642D636F6C6F723A20234546454645463B20666F6E742D7765696768743A20626F6C643B20746578742D616C69676E3A206C6566743B2070616464696E673A203770783B20636F6C6F723A20233232323232323B2220636F6C7370616E3D2232223E0A2020202020202020202020204F726465722044657461696C730A20202020202020203C2F74643E0A2020202020203C2F74723E0A202020203C2F74686561643E0A202020203C74626F64793E0A2020202020203C74723E0A20202020202020203C7464207374796C653D22666F6E742D73697A653A20313270783B09626F726465722D72696768743A2031707820736F6C696420234444444444443B20626F726465722D626F74746F6D3A2031707820736F6C696420234444444444443B20746578742D616C69676E3A206C6566743B2070616464696E673A203770783B223E0A202020202020202020203C623E4F726465722049443A3C2F623E207B7B6F7264657249647D7D3C6272202F3E0A202020202020202020203C623E44617465206F66204F726465723A3C2F623E207B7B6461746541646465647D7D3C6272202F3E0A202020202020202020203C623E5061796D656E74204D6574686F643A3C2F623E207B7B7061796D656E744D6574686F647D7D3C6272202F3E0A202020202020202020207B7B7368697070696E674D6574686F647D7D0A202020202020202020203C2F74643E0A20202020202020203C7464207374796C653D22666F6E742D73697A653A20313270783B09626F726465722D72696768743A2031707820736F6C696420234444444444443B20626F726465722D626F74746F6D3A2031707820736F6C696420234444444444443B20746578742D616C69676E3A206C6566743B2070616464696E673A203770783B223E0A202020202020202020203C623E456D61696C3A3C2F623E207B7B656D61696C7D7D3C6272202F3E0A202020202020202020203C623E54656C6570686F6E653A3C2F623E207B7B74656C6570686F6E657D7D3C6272202F3E0A202020202020202020203C623E5374617475733A3C2F623E207B7B6F726465725374617475737D7D3C6272202F3E0A20202020202020203C2F74643E0A2020202020203C2F74723E0A202020203C2F74626F64793E0A20203C2F7461626C653E0A20203C7461626C65207374796C653D22626F726465722D636F6C6C617073653A20636F6C6C617073653B2077696474683A20313030253B20626F726465722D746F703A2031707820736F6C696420234444444444443B20626F726465722D6C6566743A2031707820736F6C696420234444444444443B206D617267696E2D626F74746F6D3A20323070783B223E0A202020203C74686561643E0A2020202020203C74723E0A20202020202020203C7464207374796C653D22666F6E742D73697A653A20313270783B20626F726465722D72696768743A2031707820736F6C696420234444444444443B20626F726465722D626F74746F6D3A2031707820736F6C696420234444444444443B206261636B67726F756E642D636F6C6F723A20234546454645463B20666F6E742D7765696768743A20626F6C643B20746578742D616C69676E3A206C6566743B2070616464696E673A203770783B20636F6C6F723A20233232323232323B223E0A20202020202020202020202042696C6C696E6720416464726573730A20202020202020203C2F74643E0A20202020202020207B7B7368697070696E67416464726573735469746C657D7D0A2020202020203C2F74723E0A202020203C2F74686561643E0A202020203C74626F64793E0A2020202020203C74723E0A20202020202020203C7464207374796C653D22666F6E742D73697A653A20313270783B09626F726465722D72696768743A2031707820736F6C696420234444444444443B20626F726465722D626F74746F6D3A2031707820736F6C696420234444444444443B20746578742D616C69676E3A206C6566743B2070616464696E673A203770783B223E0A2020202020202020202020207B7B7061796D656E74416464726573737D7D0A20202020202020203C2F74643E0A20202020202020207B7B7368697070696E67416464726573737D7D0A2020202020203C2F74723E0A202020203C2F74626F64793E0A20203C2F7461626C653E0A20207B7B6F7264657250726F64756374737D7D0A20203C70207374796C653D226D617267696E2D746F703A203070783B206D617267696E2D626F74746F6D3A20323070783B223E506C65617365207265706C7920746F20737570706F7274407768617461636172742E636F6D20696620796F75206861766520616E79207175657374696F6E732E3C2F703E0A20203C703E0A2020202020205468616E6B732C3C62722F3E0A20202020202053797374656D2041646D696E0A20203C2F703E0A3C2F6469763E, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (7, 7, 'en-US', 'Update Order | {{ordernumber}}', 0x3C703E44656172207B7B637573746F6D65726E616D657D7D2C3C2F703E0A3C703E0A20202020596F7572206F7264657220237B7B6F726465726E756D6265727D7D20737461747573206F726465726564206F6E207B7B6F72646572646174657D7D20686173206265656E207570646174656420746F207B7B6F726465727374617475737D7D2E0A3C2F703E0A7B7B6F726465724C696E6B7D7D0A3C703E0A2020202054686520636F6D6D656E747320666F7220746865206F72646572206172653A3C62722F3E0A202020207B7B6F72646572636F6D6D656E74737D7D0A3C2F703E0A3C703E0A202020205468616E6B20596F752C203C62722F3E200A2020202053797374656D2041646D696E0A3C2F703E, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (8, 8, 'en-US', 'Send Mail', 0x3C68313E7B7B6170706E616D657D7D3C2F68313E0A3C703E0A202020203C7374726F6E673E46726F6D3A3C2F7374726F6E673E207B7B73746F72656E616D657D7D3C6272202F3E0A202020203C7374726F6E673E5375626A6563743A3C2F7374726F6E673E3A207B7B7375626A6563747D7D3C6272202F3E0A202020203C7374726F6E673E4D6573736167653A3C2F7374726F6E673E3A207B7B6D6573736167657D7D0A3C2F703E, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (9, 9, 'en-US', 'Newsletter', 0x3C68313E7B7B6170706E616D657D7D3C2F68313E0A3C703E0A202020203C7374726F6E673E46726F6D3A3C2F7374726F6E673E207B7B73746F72656E616D657D7D3C6272202F3E0A202020203C7374726F6E673E5375626A6563743A3C2F7374726F6E673E3A207B7B7375626A6563747D7D3C6272202F3E0A202020203C7374726F6E673E4D6573736167653A3C2F7374726F6E673E3A207B7B6D6573736167657D7D0A3C2F703E0A7B7B756E7375627363726962657D7D, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
INSERT INTO artesanal.tbl_notification_template_translated (id, owner_id, language, subject, content, created_by, modified_by, created_datetime, modified_datetime) VALUES (10, 10, 'en-US', 'New Customer Registration', 0x3C703E57656C636F6D65207B7B66756C6C4E616D657D7D2E20596F7572206163636F756E7420686173206265656E2063726561746564207375636365737366756C6C79206174207B7B6170706E616D657D7D3C2F703E0A0A3C703E596F7572206C6F67696E2064657461696C73206172652061732062656C6F773C6272202F3E3C62722F3E0A202020203C7374726F6E673E557365726E616D653A3C2F7374726F6E673E207B7B757365726E616D657D7D3C6272202F3E0A202020203C7374726F6E673E50617373776F72643C2F7374726F6E673E3A207B7B70617373776F72647D7D3C2F703E0A0A7B7B636F6E6669726D656D61696C4C6162656C7D7D0A7B7B636F6E6669726D656D61696C7D7D0A0A5468616E6B732C3C6272202F3E0A53797374656D2041646D696E0A0A, 1, 1, '2018-06-29 03:49:18', '2018-06-29 03:49:18');
CREATE TABLE artesanal.tbl_order_address_details
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id int NOT NULL,
    email varchar(128) NOT NULL,
    firstname varchar(32),
    lastname varchar(32),
    mobilephone varchar(16),
    officephone varchar(16),
    address1 varchar(128),
    address2 varchar(128),
    city varchar(20),
    country varchar(10),
    postal_code varchar(16),
    state varchar(20),
    type int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_address_details_order_id FOREIGN KEY (order_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_address_details_order_id ON artesanal.tbl_order_address_details (order_id);
CREATE TABLE artesanal.tbl_order_history
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id int,
    status smallint,
    notify_customer smallint,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_history_order_id FOREIGN KEY (order_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_history_order_id ON artesanal.tbl_order_history (order_id);
CREATE TABLE artesanal.tbl_order_history_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    comment varchar(255),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_history_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_order_history (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_history_translated_owner_id ON artesanal.tbl_order_history_translated (owner_id);
CREATE TABLE artesanal.tbl_order_payment_details
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id int NOT NULL,
    payment_method varchar(164) NOT NULL,
    payment_type varchar(64),
    total_including_tax decimal(10,2) NOT NULL,
    tax decimal(10,2) DEFAULT 0.00 NOT NULL,
    shipping_fee decimal(10,2) DEFAULT 0.00 NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_payment_details_order_id FOREIGN KEY (order_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_payment_details_order_id ON artesanal.tbl_order_payment_details (order_id);
CREATE TABLE artesanal.tbl_order_payment_details_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    comments varchar(255),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_payment_details_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_order_payment_details (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_payment_details_translated_owner_id ON artesanal.tbl_order_payment_details_translated (owner_id);
CREATE TABLE artesanal.tbl_order_payment_transaction_map
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id int NOT NULL,
    amount decimal(10,2) DEFAULT 0.00 NOT NULL,
    payment_method varchar(20) NOT NULL,
    transaction_record_id int NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_payment_transaction_map_order_id FOREIGN KEY (order_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_payment_transaction_map_order_id ON artesanal.tbl_order_payment_transaction_map (order_id);
CREATE TABLE artesanal.tbl_order_product
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id int NOT NULL,
    product_id int NOT NULL,
    options longtext,
    displayed_options longtext,
    item_code varchar(128),
    name varchar(128),
    model varchar(128),
    quantity int,
    price decimal(10,2) DEFAULT 0.00,
    options_price decimal(10,2) DEFAULT 0.00,
    total decimal(10,2) DEFAULT 0.00,
    tax decimal(10,2) DEFAULT 0.00,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_product_order_id FOREIGN KEY (order_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_product_order_id ON artesanal.tbl_order_product (order_id);
CREATE TABLE artesanal.tbl_order_status
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (5, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (6, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (7, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (8, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (9, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (10, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (11, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (12, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (13, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (14, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
CREATE TABLE artesanal.tbl_order_status_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(64) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_status_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_order_status (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_status_translated_owner_id ON artesanal.tbl_order_status_translated (owner_id);
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'Cancelled', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 2, 'en-US', 'Canceled_Reversal', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 3, 'en-US', 'Chargeback', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 4, 'en-US', 'Completed', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (5, 5, 'en-US', 'Denied', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (6, 6, 'en-US', 'Expired', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (7, 7, 'en-US', 'Failed', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (8, 8, 'en-US', 'Pending', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (9, 9, 'en-US', 'Processed', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (10, 10, 'en-US', 'Processing', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (11, 11, 'en-US', 'Refunded', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (12, 12, 'en-US', 'Reversed', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (13, 13, 'en-US', 'Shipped', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_order_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (14, 14, 'en-US', 'Voided', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
CREATE TABLE artesanal.tbl_order_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    shipping_comments longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_order_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_order_translated_owner_id ON artesanal.tbl_order_translated (owner_id);
CREATE TABLE artesanal.tbl_page
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    status smallint NOT NULL,
    parent_id int,
    custom_url varchar(64),
    level smallint NOT NULL,
    path longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_page (id, status, parent_id, custom_url, level, path, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 0, null, 0, '1', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_page (id, status, parent_id, custom_url, level, path, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 1, 0, null, 0, '2', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_page (id, status, parent_id, custom_url, level, path, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 1, 0, null, 0, '3', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_page (id, status, parent_id, custom_url, level, path, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 1, 0, null, 0, '4', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
CREATE TABLE artesanal.tbl_page_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    alias varchar(128) NOT NULL,
    menuitem varchar(128) NOT NULL,
    content longtext,
    summary longtext,
    metakeywords longtext,
    metadescription longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_page_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_page (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_page_translated_owner_id ON artesanal.tbl_page_translated (owner_id);
INSERT INTO artesanal.tbl_page_translated (id, owner_id, language, name, alias, menuitem, content, summary, metakeywords, metadescription, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'About Us', 'about-us', 'About Us', '<p>
    <strong class="first-paragraph">A</strong>t Usha Informatique, Web Development Company in India, we are driven by SPEED and EFFICIENCY to achieve superior quality and cost-competitiveness so as to enable our customer&rsquo;s stay at the forefront of their industry.</p><p>At Usha Informatique, you can find a right combination of Technical excellence, outstanding design, effective strategy and the results are pretty impressive, to serve clients acroos the globe. We utilizes both continued technical and intellectual education to enhance each project that is brought to Usha Informatique that stands our clients into the world of technology with class.</p><p>Our knowledge and experience in Software and Web solutions have greatly boosted our clients in business augmentation. We specialize in delivering cost-effective software/web solutions by implementing an offshore development model. We have a dedicated team of software professionals to bring quality products to the clients.
</p>

', 'About us summary', 'about us', 'about us description', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_page_translated (id, owner_id, language, name, alias, menuitem, content, summary, metakeywords, metadescription, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 2, 'en-US', 'Delivery Information', 'delivery-info', 'Delivery Information', '<p>This is delivery information</p>', 'Delivery information summary', 'delivery information', 'deliverr information description', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_page_translated (id, owner_id, language, name, alias, menuitem, content, summary, metakeywords, metadescription, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 3, 'en-US', 'Privacy Policy', 'privacy-policy', 'Privacy Policy', '<p>This is privacy policy</p>', 'Privacy policy summary', 'privacy policy', 'privacy policy description', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_page_translated (id, owner_id, language, name, alias, menuitem, content, summary, metakeywords, metadescription, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 4, 'en-US', 'Terms & Conditions', 'terms', 'Terms & Conditions', '<p>These are terms and conditions</p>', 'Terms & condition summary', 'terms & condition', 'terms & condition description', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
CREATE TABLE artesanal.tbl_paypal_standard_transaction
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    order_id int NOT NULL,
    payment_status varchar(32) NOT NULL,
    received_date date NOT NULL,
    transaction_id varchar(32) NOT NULL,
    transaction_fee decimal(10,2) DEFAULT 0.00 NOT NULL,
    amount decimal(10,2) DEFAULT 0.00 NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_paypal_standard_transaction_order_id FOREIGN KEY (order_id) REFERENCES artesanal.tbl_order (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_paypal_standard_transaction_order_id ON artesanal.tbl_paypal_standard_transaction (order_id);
CREATE TABLE artesanal.tbl_person
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    firstname varchar(32),
    lastname varchar(32),
    mobilephone varchar(16),
    email varchar(64),
    avatar varchar(128),
    profile_image varchar(255),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_person (id, firstname, lastname, mobilephone, email, avatar, profile_image, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'Super', 'Admin', '', 'pucese.comex@gmail.com', null, 'NGRiNDdlODcomex.png', 1, 1, '2018-06-29 03:47:15', '2018-07-29 20:51:01');
CREATE TABLE artesanal.tbl_product
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    type smallint DEFAULT 1,
    model varchar(64),
    price decimal(10,2) DEFAULT 0.00,
    buy_price decimal(10,2) DEFAULT 0.00,
    image varchar(64),
    status smallint NOT NULL,
    sku varchar(16),
    quantity int,
    initial_quantity int,
    tax_class_id int,
    minimum_quantity int,
    subtract_stock varchar(5),
    stock_status smallint,
    requires_shipping smallint,
    available_date date,
    manufacturer int,
    is_featured smallint,
    location varchar(64),
    length int,
    width int,
    height int,
    date_available date,
    weight decimal(10,2),
    length_class int,
    weight_class int,
    hits int DEFAULT 0 NOT NULL,
    upc varchar(12),
    ean varchar(14),
    jan varchar(13),
    isbn varchar(17),
    mpn varchar(64),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_attribute
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    sort_order int,
    attribute_group int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_attribute_group
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    sort_order int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_attribute_group_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_attribute_group_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_attribute_group (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_attribute_group_translated_owner_id ON artesanal.tbl_product_attribute_group_translated (owner_id);
CREATE TABLE artesanal.tbl_product_attribute_mapping
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_id int,
    attribute_id int,
    attribute_value varchar(32),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_attribute_mapping_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_attribute_mapping_product_id ON artesanal.tbl_product_attribute_mapping (product_id);
CREATE TABLE artesanal.tbl_product_attribute_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_attribute_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_attribute (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_attribute_translated_owner_id ON artesanal.tbl_product_attribute_translated (owner_id);
CREATE TABLE artesanal.tbl_product_category
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    image varchar(64),
    parent_id int,
    level int,
    status smallint,
    displayintopmenu smallint,
    data_category_id int NOT NULL,
    code varchar(164) NOT NULL,
    path longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_category_mapping
(
    product_id int,
    category_id int,
    data_category_id int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_category_mapping_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_category_mapping_product_id ON artesanal.tbl_product_category_mapping (product_id);
CREATE TABLE artesanal.tbl_product_category_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    alias varchar(128) NOT NULL,
    metakeywords varchar(128),
    metadescription varchar(128),
    description longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_category_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_category (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_category_translated_owner_id ON artesanal.tbl_product_category_translated (owner_id);
CREATE TABLE artesanal.tbl_product_discount
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    group_id int NOT NULL,
    quantity int NOT NULL,
    priority int,
    price decimal(10,2) NOT NULL,
    start_datetime datetime,
    end_datetime datetime,
    product_id int NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_discount_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_discount_product_id ON artesanal.tbl_product_discount (product_id);
CREATE TABLE artesanal.tbl_product_download
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    file varchar(128),
    type varchar(10),
    allowed_downloads int DEFAULT 0,
    number_of_days int DEFAULT 0,
    size double,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_download_mapping
(
    product_id int,
    download_id int,
    download_option varchar(28),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_download_mapping_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_download_mapping_product_id ON artesanal.tbl_product_download_mapping (product_id);
CREATE TABLE artesanal.tbl_product_download_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_download_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_download (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_download_translated_owner_id ON artesanal.tbl_product_download_translated (owner_id);
CREATE TABLE artesanal.tbl_product_image
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_id int,
    image varchar(64),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_id ON artesanal.tbl_product_image (product_id);
CREATE TABLE artesanal.tbl_product_image_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    caption varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_image_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_image (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_image_translated_owner_id ON artesanal.tbl_product_image_translated (owner_id);
CREATE TABLE artesanal.tbl_product_option
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    type varchar(64),
    url varchar(128),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_option_mapping
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_id int,
    option_id int,
    required smallint,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_option_mapping_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_option_mapping_product_id ON artesanal.tbl_product_option_mapping (product_id);
CREATE TABLE artesanal.tbl_product_option_mapping_details
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    mapping_id int NOT NULL,
    option_value_id varchar(32),
    quantity int NOT NULL,
    subtract_stock smallint NOT NULL,
    price_prefix varchar(1),
    price decimal(10,2),
    weight_prefix varchar(1),
    weight decimal(10,2),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_option_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    display_name varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_option_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_option (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_option_translated_owner_id ON artesanal.tbl_product_option_translated (owner_id);
CREATE TABLE artesanal.tbl_product_option_value
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    option_id int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_option_value_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    value varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_option_value_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_option_value (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_option_value_translated_owner_id ON artesanal.tbl_product_option_value_translated (owner_id);
CREATE TABLE artesanal.tbl_product_rating
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    rating decimal(10,2) NOT NULL,
    product_id int NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_rating_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_rating_product_id ON artesanal.tbl_product_rating (product_id);
CREATE TABLE artesanal.tbl_product_related_product_mapping
(
    product_id int,
    related_product_id int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_related_product_mapping_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_related_product_mapping_product_id ON artesanal.tbl_product_related_product_mapping (product_id);
CREATE TABLE artesanal.tbl_product_review
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(128) NOT NULL,
    status smallint NOT NULL,
    product_id int NOT NULL,
    email varchar(64) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_review_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_review_product_id ON artesanal.tbl_product_review (product_id);
CREATE TABLE artesanal.tbl_product_review_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    review longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_review_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_review (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_review_translated_owner_id ON artesanal.tbl_product_review_translated (owner_id);
CREATE TABLE artesanal.tbl_product_special
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    group_id int NOT NULL,
    priority int,
    price decimal(10,2) NOT NULL,
    start_datetime datetime,
    end_datetime datetime,
    product_id int NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_special_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_special_product_id ON artesanal.tbl_product_special (product_id);
CREATE TABLE artesanal.tbl_product_tag_mapping
(
    product_id int,
    tag_id int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_tag_mapping_product_id FOREIGN KEY (product_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_tag_mapping_product_id ON artesanal.tbl_product_tag_mapping (product_id);
CREATE TABLE artesanal.tbl_product_tax_class
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_product_tax_class_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(164) NOT NULL,
    description longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_tax_class_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product_tax_class (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_tax_class_translated_owner_id ON artesanal.tbl_product_tax_class_translated (owner_id);
CREATE TABLE artesanal.tbl_product_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    alias varchar(128) NOT NULL,
    metakeywords varchar(128),
    metadescription varchar(128),
    description longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_product_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_product (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_product_translated_owner_id ON artesanal.tbl_product_translated (owner_id);
CREATE TABLE artesanal.tbl_sequence
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    invoice_sequence_no varchar(11) NOT NULL,
    customer_sequence_no varchar(11) NOT NULL,
    order_sequence_no varchar(11) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_session
(
    id varchar(40) PRIMARY KEY NOT NULL,
    expire int,
    data longblob,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_state
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    country_id int NOT NULL,
    status smallint,
    code varchar(10),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_state_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(64) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_state_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_state (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_state_translated_owner_id ON artesanal.tbl_state_translated (owner_id);
CREATE TABLE artesanal.tbl_stock_status
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_stock_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_stock_status (id, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
CREATE TABLE artesanal.tbl_stock_status_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_stock_status_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_stock_status (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_stock_status_translated_owner_id ON artesanal.tbl_stock_status_translated (owner_id);
INSERT INTO artesanal.tbl_stock_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'In Stock', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
INSERT INTO artesanal.tbl_stock_status_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 2, 'en-US', 'Out Of Stock', 1, 1, '2018-06-29 03:47:16', '2018-06-29 03:47:16');
CREATE TABLE artesanal.tbl_store
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    url varchar(64),
    status smallint NOT NULL,
    data_category_id int NOT NULL,
    is_default smallint DEFAULT 0 NOT NULL,
    owner_id int NOT NULL,
    theme varchar(16),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_store (id, url, status, data_category_id, is_default, owner_id, theme, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'http://teststore.org', 1, 1, 0, 1, '', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
CREATE TABLE artesanal.tbl_store_configuration
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    store_id int NOT NULL,
    category varchar(32) NOT NULL,
    code varchar(128) NOT NULL,
    `key` varchar(128) NOT NULL,
    value longtext NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'storeconfig', 'storesettings', 'invoice_prefix', '#', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 1, 'storeconfig', 'storesettings', 'catalog_items_per_page', '8', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 1, 'storeconfig', 'storesettings', 'list_description_limit', '100', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 1, 'storeconfig', 'storesettings', 'display_price_with_tax', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (5, 1, 'storeconfig', 'storesettings', 'tax_calculation_based_on', 'billing', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (6, 1, 'storeconfig', 'storesettings', 'guest_checkout', '0', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (7, 1, 'storeconfig', 'storesettings', 'order_status', '8', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (8, 1, 'storeconfig', 'storesettings', 'display_stock', '0', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (9, 1, 'storeconfig', 'storesettings', 'customer_online', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (10, 1, 'storeconfig', 'storesettings', 'default_customer_group', '', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (11, 1, 'storeconfig', 'storesettings', 'allow_reviews', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (12, 1, 'storeconfig', 'storesettings', 'allow_guest_reviews', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (13, 1, 'storeconfig', 'storesettings', 'show_out_of_stock_warning', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (14, 1, 'storeconfig', 'storesettings', 'allow_out_of_stock_checkout', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (15, 1, 'storeconfig', 'storesettings', 'allow_wishlist', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (16, 1, 'storeconfig', 'storesettings', 'allow_compare_products', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (17, 1, 'storeconfig', 'storesettings', 'customer_prefix', '#', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (18, 1, 'storeconfig', 'storesettings', 'order_prefix', '#', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (19, 1, 'storeconfig', 'storesettings', 'display_weight', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (20, 1, 'storeconfig', 'storesettings', 'display_dimensions', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (21, 1, 'storeconfig', 'storelocal', 'country', 'EC', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (22, 1, 'storeconfig', 'storelocal', 'timezone', 'America/Guyana', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (23, 1, 'storeconfig', 'storelocal', 'state', 'Esmeraldas', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (24, 1, 'storeconfig', 'storelocal', 'currency', 'USD', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (25, 1, 'storeconfig', 'storelocal', 'length_class', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (26, 1, 'storeconfig', 'storelocal', 'weight_class', '1', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (27, 1, 'storeconfig', 'storelocal', 'language', 'en-US', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (28, 1, 'storeconfig', 'storeimage', 'store_logo', '', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (29, 1, 'storeconfig', 'storeimage', 'icon', '', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (30, 1, 'storeconfig', 'storeimage', 'category_image_width', '90', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (31, 1, 'storeconfig', 'storeimage', 'category_image_height', '90', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (32, 1, 'storeconfig', 'storeimage', 'product_list_image_width', '150', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (33, 1, 'storeconfig', 'storeimage', 'product_list_image_height', '150', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (34, 1, 'storeconfig', 'storeimage', 'related_product_image_width', '80', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (35, 1, 'storeconfig', 'storeimage', 'related_product_image_height', '80', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (36, 1, 'storeconfig', 'storeimage', 'compare_image_width', '90', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (37, 1, 'storeconfig', 'storeimage', 'compare_image_height', '90', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (38, 1, 'storeconfig', 'storeimage', 'wishlist_image_width', '47', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (39, 1, 'storeconfig', 'storeimage', 'wishlist_image_height', '47', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (40, 1, 'storeconfig', 'storeimage', 'cart_image_width', '47', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (41, 1, 'storeconfig', 'storeimage', 'cart_image_height', '47', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (42, 1, 'storeconfig', 'storeimage', 'store_image_width', '47', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (43, 1, 'storeconfig', 'storeimage', 'store_image_height', '47', 1, 1, '2018-06-29 03:49:17', '2018-08-13 02:29:33');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (44, 1, 'payment', 'cashondelivery', 'order_status', '8', 1, 1, '2018-06-29 15:57:57', '2018-06-29 15:57:57');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (45, 1, 'payment', 'paypal_standard_orderstatus_map', 'canceled_reversal_status', '2', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (46, 1, 'payment', 'paypal_standard_orderstatus_map', 'completed_status', '4', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (47, 1, 'payment', 'paypal_standard_orderstatus_map', 'denied_status', '5', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (48, 1, 'payment', 'paypal_standard_orderstatus_map', 'expired_status', '6', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (49, 1, 'payment', 'paypal_standard_orderstatus_map', 'failed_status', '7', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (50, 1, 'payment', 'paypal_standard_orderstatus_map', 'pending_status', '8', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (51, 1, 'payment', 'paypal_standard_orderstatus_map', 'processed_status', '9', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (52, 1, 'payment', 'paypal_standard_orderstatus_map', 'refunded_status', '11', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (53, 1, 'payment', 'paypal_standard_orderstatus_map', 'reversed_status', '12', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (54, 1, 'payment', 'paypal_standard_orderstatus_map', 'voided_status', '14', 1, 1, '2018-06-29 15:57:59', '2018-06-29 15:57:59');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (55, 1, 'shipping', 'flat', 'method_name', 'fixed', 1, 1, '2018-06-29 15:58:03', '2018-06-29 15:58:03');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (56, 1, 'shipping', 'flat', 'calculateHandlingFee', 'fixed', 1, 1, '2018-06-29 15:58:03', '2018-06-29 15:58:03');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (57, 1, 'shipping', 'flat', 'handlingFee', '0', 1, 1, '2018-06-29 15:58:03', '2018-06-29 15:58:03');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (58, 1, 'shipping', 'flat', 'type', 'perItem', 1, 1, '2018-06-29 15:58:03', '2018-06-29 15:58:03');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (59, 1, 'shipping', 'flat', 'applicableZones', '1', 1, 1, '2018-06-29 15:58:03', '2018-06-29 15:58:03');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (60, 1, 'shipping', 'flat', 'specificZones', 'a:0:{}', 1, 1, '2018-06-29 15:58:03', '2018-06-29 15:58:03');
INSERT INTO artesanal.tbl_store_configuration (id, store_id, category, code, `key`, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (61, 1, 'shipping', 'flat', 'price', '5', 1, 1, '2018-06-29 15:58:03', '2018-06-29 15:58:03');
CREATE TABLE artesanal.tbl_store_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    description longtext,
    metakeywords longtext,
    metadescription longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_store_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_store (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_store_translated_owner_id ON artesanal.tbl_store_translated (owner_id);
INSERT INTO artesanal.tbl_store_translated (id, owner_id, language, name, description, metakeywords, metadescription, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'Default', 'This is test store set up with the application', '', '', 1, 1, '2018-06-29 03:49:17', '2018-08-13 01:57:28');
CREATE TABLE artesanal.tbl_tag
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    frequency int,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_tag_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(64) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_tag_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_tag (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_tag_translated_owner_id ON artesanal.tbl_tag_translated (owner_id);
CREATE TABLE artesanal.tbl_tax_rule
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    based_on varchar(16) NOT NULL,
    type varchar(64) NOT NULL,
    value varchar(64) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_tax_rule_details
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    tax_rule_id int NOT NULL,
    product_tax_class_id int NOT NULL,
    customer_group_id int NOT NULL,
    tax_zone_id int NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_tax_rule_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(64) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_tax_rule_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_tax_rule (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_tax_rule_translated_owner_id ON artesanal.tbl_tax_rule_translated (owner_id);
CREATE TABLE artesanal.tbl_user
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username varchar(64) NOT NULL,
    password_reset_token varchar(128),
    password_hash varchar(128) NOT NULL,
    auth_key varchar(128),
    status smallint,
    person_id int,
    login_ip varchar(20),
    last_login datetime,
    timezone varchar(32),
    type varchar(16),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_user (id, username, password_reset_token, password_hash, auth_key, status, person_id, login_ip, last_login, timezone, type, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'superadmin', null, '$2y$13$7doXqm4dmHrUfGc4sTmq/u6aE9Myo6T1yTvM50ZVBqGGMtehJVHbS', 'vU5bx--w-yE8CcYRtpkajPbmZSCY6-X1', 1, 1, '127.0.0.1', '2018-08-13 02:59:32', 'America/Guayaquil', 'system', 1, 1, '2018-06-29 03:47:15', '2018-07-29 20:51:01');
CREATE TABLE artesanal.tbl_weight_class
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    unit varchar(10) NOT NULL,
    value decimal(10,2) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
INSERT INTO artesanal.tbl_weight_class (id, unit, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 'kg', 1.00, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_weight_class (id, unit, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 'g', 1000.00, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_weight_class (id, unit, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 'oz', 35.27, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_weight_class (id, unit, value, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 'lb', 2.20, 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
CREATE TABLE artesanal.tbl_weight_class_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(128) NOT NULL,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_weight_class_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_weight_class (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_weight_class_translated_owner_id ON artesanal.tbl_weight_class_translated (owner_id);
INSERT INTO artesanal.tbl_weight_class_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (1, 1, 'en-US', 'Kilogram', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_weight_class_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (2, 2, 'en-US', 'Gram', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_weight_class_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (3, 3, 'en-US', 'Ounce', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
INSERT INTO artesanal.tbl_weight_class_translated (id, owner_id, language, name, created_by, modified_by, created_datetime, modified_datetime) VALUES (4, 4, 'en-US', 'Pound', 1, 1, '2018-06-29 03:47:15', '2018-06-29 03:47:15');
CREATE TABLE artesanal.tbl_zone
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    country_id int NOT NULL,
    state_id int NOT NULL,
    zip varchar(16),
    is_zip_range smallint,
    from_zip varchar(16),
    to_zip varchar(16),
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime
);
CREATE TABLE artesanal.tbl_zone_translated
(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    owner_id int NOT NULL,
    language varchar(10) NOT NULL,
    name varchar(64) NOT NULL,
    description longtext,
    created_by int DEFAULT 0,
    modified_by int DEFAULT 0,
    created_datetime datetime,
    modified_datetime datetime,
    CONSTRAINT fk_tbl_zone_translated_owner_id FOREIGN KEY (owner_id) REFERENCES artesanal.tbl_zone (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE UNIQUE INDEX fk_tbl_zone_translated_owner_id ON artesanal.tbl_zone_translated (owner_id);